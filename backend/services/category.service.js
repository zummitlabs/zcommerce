import { PrismaClientKnownRequestError } from "@prisma/client/runtime/library";
import prisma from "../prisma/client.js";
import { PrismaClientKnownRequestErrorException } from "../exceptions/prisma.exception.js";

export const createCategory = (data) => {
    return new Promise( async ( resolve, reject ) => {
        let newCategory = null;

        try {
            newCategory = await prisma.category.create({
                data: {
                    name: data.name,
                    description: data.description
                }
            });

        } catch (err) {
            if (err instanceof PrismaClientKnownRequestError) {
                if (err.code === "P2002") {
                    console.error(err.message);
                    reject(new PrismaClientKnownRequestErrorException(err.message, err.code, err.meta));
                }
            }
        }
        
        newCategory ? resolve(newCategory) : reject ("Could not create category");
    });
}

export const createSubcategory = (data) => {
    return new Promise( async( resolve, reject ) => {
        let newSubcategory = null;
            try {
                newSubcategory = await prisma.category.create({
                    data: {
                        name: data.name,
                        description: data.description,
                        parentId: data.parentId
                    }
                })
            } catch (err) {
                if (err instanceof PrismaClientKnownRequestError) {
                    console.error(err.message);
                    reject(new PrismaClientKnownRequestErrorException(err.message, err.code, err.meta));
                }
            }
        newSubcategory ? resolve(newSubcategory) : reject (new Error("Could not create subcategory"));
    });
}

export const findAllCategories = () => {
    return new Promise ( async (resolve, reject) => {
        let allCategories = [];

        try {
            allCategories = await prisma.category.findMany();

        } catch (err) {
            reject(err)
        }

        resolve(allCategories);
    });
}

export const findCategoryById = (id) => {
    return new Promise ( async (resolve, reject) => {
        let category = null;

        try {
            category = await prisma.category.findFirst({
                where: {
                    id: id
                }
            });

        } catch (err) {
            reject(err);
        } 

        resolve(category);
    })
}

export const findCategoryByName = (categoryName) => {
    return new Promise( async (resolve, reject) => {
        let category = null;
        try {
            category = await prisma.category.findFirst({
                where: {
                    name: categoryName
                }
            })
        } catch (err) {
            reject(err)
        }

        resolve(category);
    })
} 

export const findAllSubcategoriesForCategoryId = (categoryId) => {
    return new Promise( async ( resolve, reject ) => {
        let allSubcategories = [];

        try {
            allSubcategories = await prisma.category.findMany({
                where: {
                    parentId: categoryId
                }
            })
        } catch (err) {
            
        }

        resolve(allSubcategories);
    })
}