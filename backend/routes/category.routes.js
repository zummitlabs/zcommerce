import express from 'express';
import { addCategory, addSubcategory, categories, categoryById, categoryName, subcategoriesForCategory } from '../controllers/category.controller.js';

const router = express.Router();

router.get("/", categories),
router.get("/:id", categoryById),
router.get("/:id/subcategories", subcategoriesForCategory),
router.get("/category-name", categoryName)

router.put("/add", addCategory), 
router.put("/add/subcategory", addSubcategory)

export default router;