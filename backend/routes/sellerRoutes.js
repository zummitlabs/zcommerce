import express from 'express'

import { createSeller,getSellers } from '../controllers/sellerController.js'

const router = express.Router()

router.get("/", getSellers)
router.post("/", createSeller)

export default router