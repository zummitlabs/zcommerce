import express from "express";
import { createStore, getStores } from "../controllers/storeController.js";

const router = express.Router();

router.get("/", getStores);
router.post("/", createStore);

export default router;
