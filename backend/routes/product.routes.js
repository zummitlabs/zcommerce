import express from 'express';
const router = express.Router();
import { createProduct, getProducts, getProduct, updateProduct, deleteProduct } from "../controllers/product.controller.js";

router.post("/createproduct", createProduct);
router.get("/getproduct", getProducts);
router.get("/getproductbyid/:id", getProduct);
router.put("/updateproduct/:id", updateProduct);
router.delete("/deleteproduct/:id", deleteProduct);

export default router;