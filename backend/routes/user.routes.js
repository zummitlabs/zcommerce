import express from 'express';
const router = express.Router();
import {createUser , forgotPassword, login, passwordReset, verifyEmail} from "../controllers/users.controller.js";

router.post('/signup', createUser);
router.post('/login', login);
router.post('/verify' , verifyEmail);
router.post('/forgetpassword' , forgotPassword)
router.put('/resetpassword' , passwordReset)



export default router;
