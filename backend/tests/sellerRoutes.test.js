import request from 'supertest';
import express from 'express';
import sellerRoutes from '../routes/sellerRoutes';

const app = express();
app.use(express.json());
app.use('/api/sellers', sellerRoutes);

describe('Seller Routes', () => {
  it('should create a new seller', async () => {
    const newSeller = {
      firstname: 'John',
      lastname: 'Doe',
      email: 'john.doe@example.com',
      password: 'password123',
      phone: '1234567890'
    };

    const response = await request(app).post('/api/sellers').send(newSeller);
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.firstname).toBe('John');
    expect(response.body.lastname).toBe('Doe');
    expect(response.body.email).toBe('john.doe@example.com');
  });

  it('should retrieve all sellers', async () => {
    const response = await request(app).get('/api/sellers');
    expect(response.status).toBe(200);
    expect(response.body).toBeInstanceOf(Array);
  });
});
