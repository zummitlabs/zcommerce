import { BadRequestException, NotFoundException } from "../exceptions/http.exception.js";
import {  createCategory, createSubcategory, findAllCategories, findAllSubcategoriesForCategoryId, findCategoryById, findCategoryByName } from "../services/category.service.js";
import { categoryNameValidator, categoryValidator } from "../validations/category.validator.js";
import * as Zod from 'zod';

export const addCategory = (req, res) => {
    try {
        const categoryData = categoryValidator.parse(req.body);
        createCategory(categoryData)
        .then( (category) => {
            res.status(201).json(category)
        })
        .catch((err) => {
            res.status(500).send(err)
        })
    } 
    catch(err) {
        if (err instanceof Zod.ZodError) {
            res.status(500).json(err.issues);
        }
    }
}

export const addSubcategory = (req, res) => {
    try {
        const subcategoryData = categoryValidator.parse(req.body);
        createSubcategory(subcategoryData)
        .then(( subcategory ) => {
            res.status(201).send(subcategory)
        })
        .catch(( err ) => {
            res.status(500).send(err)
        })

    } catch (err) {
        if (err instanceof Zod.ZodError) {
            res.status(500).json(err.issues);
        } 
    }
}

export const categories = (req, res) => {
    findAllCategories()
    .then( (allCategories) => {
        res.status(200).send(allCategories)
    })
    .catch( (err) => {
        res.status(500).send(err)
    })

}

export const subcategoriesForCategory = (req, res) => {
    const id = +req.params.id;
    
    if (isNaN(id)) {
        return res.status(400).send(new BadRequestException ("Bad request parameter!") );
    }

    findAllSubcategoriesForCategoryId(id)
    .then( (subcategories) => {
        res.status(200).send(subcategories)
    })
    .catch( (err) => {
        res.status(500).send(err)
    })
}

export const categoryById = (req, res) => {
    const id = +req.params.id;

    if (isNaN(id)) {
        return res.status(400).send(new BadRequestException ("Bad request parameter!") );
    }

    findCategoryById(id)
    .then( (category) => {
        if (!category) {
            return res.status(404).send(new NotFoundException("Resource not found!"));
            
        }

        res.status(200).send(category)
    })
    .catch( (err) => {
        res.status(500).send(err)
    })
}

export const categoryName = (req, res) => {
    try {
        let categoryName = categoryNameValidator(req.body);
        findCategoryByName(categoryName)
        .then((category) => {
            if (!category) {
                return res.status(404).send(new NotFoundException("Resource not found!"))
            }
        })
        .catch((err) => {
            res.status(500).send(err)
        })
    } catch (err) {
        if (err instanceof Zod.ZodError) {
            res.status(500).send(err.issues)
        } 
    }
}