import { ZodError } from "zod";
import { prisma } from "../prismaClient.js";
import { storeValidations } from '../validations/storeValidation.js'

export const getStores = async (req, res) => {
  try {
    const stores = await prisma.store.findMany();
    res.json(stores);
  } catch (error) {
    res.status(500).json({ error: "Error fetching stores" });
  }
};

export const createStore = async (req, res) => {
  try {
    const validation = storeValidations.parse(req.body);

    const { name, category, image } = req.body;
    console.log(name)
    const newStore = await prisma.store.create({
      data: {
        name,
        category,
        image,
      },
    });
    res.status(201).json(newStore);
  } catch (error) {
    if (error instanceof ZodError) {
      const formatedErrors = error.errors.map((error) => ({
        field: error.path[0],
        message: error.message,
      }));
      return res.status(400).json({ errors: formatedErrors });
    }

    res.status(500).json({ error: "Error creating store" });
  }
};
