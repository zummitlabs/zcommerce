import { PrismaClient } from "@prisma/client";
import sellerValidations from "../validations/sellerValidation.js"

import {z} from "zod";

const prisma = new PrismaClient()

export const getSellers = async(req,res)=> {
    try {
       const allSeller = await prisma.seller.findMany()
       res.json(allSeller)

    }catch(error){
       return res.status(500).json({message:"Internal server error"})
    }
}

export const createSeller = async (req,res) => {

 try{
    const validateSeller = sellerValidations.parse(req.body)
    const newSeller = await prisma.seller.create({data:validateSeller})
    res.status(201).json(newSeller)
 
 }catch(error){
    if(error instanceof z.ZodError){
        return res.status(500).json(e.error)
    }
    else{
        res.status(500).json({message:"Internal Server error"})
    }
 }

}