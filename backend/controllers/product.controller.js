import { prisma } from "../prismaClient.js";

export const createProduct = async (req, res) => {  
    try {
        const { name, price, description } = req.body;
        if(!name || !price || !description) {
            return res.status(400).json({ error: "Name, price, description and category are required" });
        }
        const newProduct = await prisma.product.create({
            data: {
                name : name,
                price : price,
                description : description
            },
        });
        res.status(201).json(newProduct);
    } catch (error) {
        return res.status(500).json({ error: "Error creating product",
            errorMessage: error.message
         });
    }
}

export const getProducts = async (req, res) => {
    try {
        const products = await prisma.product.findMany();
        res.status(200).json(products);
    } catch (error) {
        return res.status(500).json({ error: "Error getting products",
            errorMessage: error.message
         });
    }
}

export const getProduct = async (req, res) => {
    try {
        const { id } = req.params;
        const product = await prisma.product.findUnique({
            where: {
                id: parseInt(id),
            },
        });
        if(!product) {
            return res.status(404).json({ error: "Product not found" });
        }
        res.status(200).json(product);
    } catch (error) {
        return res.status(500).json({ error: "Error getting product",
            errorMessage: error.message
         });
    }
}

export const updateProduct = async (req, res) => {
    try {
        const { id } = req.params;
        const { name, price, description } = req.body;
        if(!name || !price || !description) {
            return res.status(400).json({ error: "Name, price and description are required" });
        }
        const product = await prisma.product.findUnique({
            where: {
                id: parseInt(id),
            },
        });
        if(!product) {
            return res.status(404).json({ error: "Product not found" });
        }
        const updatedProduct = await prisma.product.update({
            where: {
                id: parseInt(id),
            },
            data: {
                name : name,
                price : price,
                description : description
            },
        });
        res.status(200).json(updatedProduct);
    } catch (error) {
        return res.status(500).json({ error: "Error updating product",
            errorMessage: error.message
         });
    }
}

export const deleteProduct = async (req, res) => {
    try {
        const { id } = req.params;
        const product = await prisma.product.findUnique({
            where: {
                id: parseInt(id),
            },
        });
        if(!product) {
            return res.status(404).json({ error: "Product not found" });
        }
        await prisma.product.delete({
            where: {
                id: parseInt(id),
            },
        });
        res.status(200).json({ message: "Product deleted" });
    } catch (error) {
        return res.status(500).json({ error: "Error deleting product",
            errorMessage: error.message
         });
    }
}

