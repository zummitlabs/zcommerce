import { prisma } from "../prismaClient.js";
import  emailTransporter  from '../utils/emailTransporter.js';
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

function generateSixDigitNumber() {
    return Math.floor(100000 + Math.random() * 900000);
}

export const createUser = async (req, res) => {
    try {
        const { name, email, password , username , phone , role} = req.body;
        if(!name || !email || !password) {
            return res.status(400).json({ error: "Name, email and password are required" });
        }
        const user = await prisma.user.findUnique({
            where: {
                email,
            },
        });

        if(user) {
            return res.status(400).json({ error: "User already exists" });
        }
        
        const hashpassword = await bcrypt.hash(password, 10);
        // create user
        const newUser = await prisma.user.create({
        data: {
            fullName : name,
            username : username,
            email : email,
            password : hashpassword,
            phone : phone,
            role : role
        },
        });
        //token generation 
        const token = await prisma.token.create({
            data: {
                token: generateSixDigitNumber().toString(),
                userId: newUser.id,
                tokenType:"EMAIL_VERIFICATION",
                expiry: new Date(new Date().getTime() + 60 * 1000 * 5), // 5 minutes
            },
        });
        console.log(token.token);
        // send email
        try {
            await emailTransporter(email, "Email verification", `Use this code to verify your email: ${token.token}`);
        } catch (emailError) {
            // Handle email-specific errors
            return res.status(500).json({
                error: "Invalid Email",
            });
        }    
        res.status(201).json({ message: "Code is send to your email"  , newUser : newUser.id });
    } catch (error) {
        return res.status(500).json({ error: "Error creating user try again after 5 minutes",
            errorMessage: error.message
         });
    }
};

export const login = async (req, res) => {
    try {
        const { email, password } = req.body;
        if(!email || !password) {
            return res.status(400).json({ error: "Email and password are required" });
        }
        const user = await prisma.user.findUnique({
            where: {
                email,
            },
        });
        if(!user) {
            return res.status(400).json({ error: "User not found" });
        }
        const isMatch = await bcrypt.compare(password, user.password);
        if(!isMatch) {
            return res.status(400).json({ error: "Invalid credentials" });
        }
        if(!user.isVerified) {
            const token = await prisma.token.create({
                data: {
                    token: generateSixDigitNumber().toString(),
                    userId: user.id,
                    tokenType:"EMAIL_VERIFICATION",
                    expiry: new Date(new Date().getTime() + 60 * 1000 * 5), // 5 minutes
                },
            });           
            await emailTransporter(email, "Email verification", `Use this code to verify your email: ${token.token}`);
            return res.status(400).json({ error: "Email not verified. Verification email sent again" });

        }
        const token = jwt.sign({ user : user }, 'secret_key', { expiresIn: '1h' })
        return res.json({ mssg: "user logged in succesfully", user: user, token: token })
    } catch (error) {
        return res.status(500).json({ error: "Error logging in",
            errorMessage: error.message
         });
    }
}

export const verifyEmail = async (req, res) => {
    try {
        const { token } = req.body;
        if(!token) {
            return res.status(400).json({ error: "Token is required" });
        }
        const tokenData = await prisma.token.findFirst({
            where: {
                token,
                tokenType: { in: ["EMAIL_VERIFICATION", "PASSWORD_RESET"] },
                expiry: {
                    gte: new Date(),
                },
            },
        });
        if(!tokenData) {
            return res.status(400).json({ error: "Invalid or expired token" });
        }
        await prisma.user.update({
            where: {
                id: tokenData.userId,
            },
            data: {
                isVerified: true,
            },
        });
        res.status(200).json({ message: "Email verified" ,
            tokenType: tokenData.tokenType
         });
    } catch (error) {
        return res.status(500).json({ error: "Error verifying email",
            errorMessage: error.message
         });
    }
}

const generateResetPasswordToken = async (id) => {
    const token = await prisma.token.create({
        data: {
            token: generateSixDigitNumber().toString(),
            userId: id,
            tokenType:"PASSWORD_RESET",
            expiry: new Date(new Date().getTime() + 60 * 1000 * 5), // 5 minutes
        },
    });
    return token.token;
}

export const forgotPassword = async (req, res) => {
    try {
        const { email } = req.body;
        if(!email) {
            return res.status(400).json({ error: "Email is required" });
        }
        const user = await prisma.user.findUnique({
            where: {
                email,
            },
        });
        if(!user) {
            return res.status(400).json({ error: "User not found" });
        }
        const token = await generateResetPasswordToken(user.id);
        await emailTransporter(email, "Reset password", `Use this code to reset your password: ${token}`);
        res.status(200).json({ message: "Reset password token sent" });
    } catch (error) {
        return res.status(500).json({ error: "Error sending reset password token",
            errorMessage: error.message
         });
    }
}

export const passwordReset = async(req, res) => {
    try {
        const { email, password } = req.body;
        if(!email || !password) {
            return res.status(400).json({ error: "password are required" });
        }
        
        const hashpassword = await bcrypt.hash(password, 10);
        await prisma.user.update({
            where: {
                email : email,
            },
            data: {
                password: hashpassword,
            },
        });
        res.status(200).json({ message: "Password reset" });
    } catch (error) {
        return res.status(500).json({ error: "Error resetting password",
            errorMessage: error.message
         });
    }
}


