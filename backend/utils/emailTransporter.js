import nodemailer from 'nodemailer';
import dotenv from 'dotenv';
dotenv.config()



const emailTransporter = async(email,subject,text)=> {
    try {
        const transporter = nodemailer.createTransport({
            host: process.env.HOST,
            service: process.env.SERVICE,
            port:Number(process.env.EMAIL_PORT),
            secure: Boolean(process.env.SECURE),
            auth:{
                user: process.env.USERK,
                pass: process.env.PASS
            }
    });
   
    await transporter.sendMail({
        from: process.env.USERK,
        to: email,
        subject: subject,
        text: text
    });
    } catch(error) {
        throw new Error("Invalid email");
    }
}  

export default emailTransporter;
