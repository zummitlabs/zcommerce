export default class PrismaException { 
    constructor(message, status, meta) {
        this.meta = meta;
        this.message = message;
        this.status = status;
    }
}

export class PrismaClientKnownRequestErrorException extends PrismaException {
    constructor(message, status, meta) {
        super(message, status, meta);
        this.name = "PrismaClientKnownRequestError";
    }
}
