export default class HttpException {}

export class BadRequestException extends HttpException {
    constructor(message, name = "BAD REQUEST", code = 400) {
        super();
        
        this.message = message;
        this.name = name; 
        this.code = 400;
    }
}

export class NotFoundException extends HttpException {
    constructor(message, name = "NOT FOUND", code = 404) {
        super();
        
        this.message = message;
        this.name = name; 
        this.code = 404;
    }
}