/*
  Warnings:

  - You are about to drop the column `token_type` on the `token` table. All the data in the column will be lost.
  - Added the required column `type` to the `token` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `token` DROP COLUMN `token_type`,
    ADD COLUMN `type` ENUM('EMAIL_VERIFICATION', 'PASSWORD_RESET') NOT NULL;

-- AlterTable
ALTER TABLE `user` ADD COLUMN `isVerified` BOOLEAN NOT NULL DEFAULT false;
