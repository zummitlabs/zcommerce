import zod from "zod";

export const categoryValidator = zod.object({
    name:          zod.string().min(3).max(32),
    description:   zod.string().min(8).max(320),
    parentId:      zod.number().optional()
});

export const categoryNameValidator = zod.object({
    name: zod.string().min(3).max(32)
});