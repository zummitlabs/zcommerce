
import {z} from "zod"

const sellerValidations = z.object({
    firstname: z.string().min(1).max(20),
    lastname: z.string().min(1).max(20),
    email: z.string().email({message:"Invalid email address"}),
    password:z.string().min(6,{message:"Password must be 6 characters long"}),
    phone: z.string().optional()
})

export default sellerValidations