import z from 'zod'

export const storeValidations = z.object({
  name: z
    .string()
    .min(5, { message: "Name must be at least 5 characters long" })
    .max(20, { message: "Name must be at most 20 characters long" }),
  category: z.string().min(1, { message: "Category is required" }),
});

