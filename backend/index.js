import express from 'express'
import dotenv from "dotenv"
import sellerRoutes from "./routes/sellerRoutes.js"
import categoryRouter from "./routes/category.routes.js";
import storeRouter from "./routes/storeRoutes.js";
import userRouter from "./routes/user.routes.js";
import productRouter from "./routes/product.routes.js";
import cors  from "cors";

dotenv.config();

const app = express();

const PORT = 4000

app.use(express.json())
app.use(cors({
  origin: "http://localhost:5173", // frontend url
  methods: ["POST", "GET", "PUT", "DELETE"],
  allowedHeaders: ["Content-Type", "Authorization"],
}));
app.use("/api/categories", categoryRouter)
app.use("/api/stores", storeRouter)
app.use("/api/sellers", sellerRoutes)
app.use("/api/users", userRouter)

app.use("/api/product", productRouter);

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});