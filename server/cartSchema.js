const db = require("./dbConfig");

function createCartTable() {
    const stm = `CREATE TABLE IF NOT EXISTS Shopping_cart (
        cart_id INT AUTO_INCREMENT PRIMARY KEY,
        customer_id INT,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (customer_id) REFERENCES Customer(customer_id)
    )`;


    db.query(stm, (err) => {
        if (err) {
            console.error('error while creating cart table', err);
        }
        else {
            console.log('Cart table ready.');
        }
    });
}
module.exports = createCartTable;