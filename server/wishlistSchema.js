const db = require("./dbConfig");

function createWishlistTable() {
    const stm = `CREATE TABLE IF NOT EXISTS Wishlist (
    wishlist_id INT PRIMARY KEY AUTO_INCREMENT,
    customer_id INT NOT NULL,
    product_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (customer_id) REFERENCES Customer(customer_id),
    FOREIGN KEY (product_id) REFERENCES Products(product_id),
    UNIQUE (customer_id, product_id)
);
`;


    db.query(stm, (err) => {
        if (err) {
            console.error('error while creating Wishlist Table', err);
        }
        else {
            console.log('Wishlist items table ready.');
        }
    });
}
module.exports = createWishlistTable;