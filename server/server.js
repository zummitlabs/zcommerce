const express = require('express');
const cors = require('cors');
const db = require('./dbConfig');
const createSellerTable = require('./SellerSchema');
const createCartTable = require('./cartSchema');
const bodyParser = require('body-parser');
const createCartItemsTable = require('./cartItemsSchema');
const createWishlistTable = require('./wishlistSchema');

const app = express();
app.use(express.json());
app.use(bodyParser.json());
app.use(cors());

// Ensure table is created on server startup
createSellerTable(); 
createCartTable(); 
createCartItemsTable(); 
createWishlistTable();

app.get("/sellers", (req, res) => {
    const sql = "SELECT * FROM seller";
    db.query(sql, (err, data) => {
        if (err) {
            res.status(500).json({ error: "Database query error" });
        } else {
            res.json(data);
        }
    });
});


//when clicking the cart button
app.get('/cart', (req, res) => {
  const query = `SELECT * FROM Shopping_cart`;
  db.query(query, (err, results) => {
      if (err) throw err;
      res.send(results);
  });
});

//When clicking goto wishlist button
app.get('/wishlist', (req, res) => {
  const customer_id = req.body.customer_id;
  const query = `SELECT p.* FROM Wishlist w JOIN Products p ON w.product_id = p.product_id WHERE w.customer_id = ?`;
  db.query(query, [customer_id], (err, results) => {
      if (err) return res.status(500).send(err);
      res.status(200).json(results);
  });
});

//when clicking the wishlist button
app.post('/wishlist', (req, res) => {
  const { customer_id, product_id } = req.body;
  const query = `INSERT INTO Wishlist (customer_id, product_id) VALUES (?, ?) ON DUPLICATE KEY UPDATE created_at = CURRENT_TIMESTAMP;`;
  db.query(query, [customer_id, product_id], (err, results) => {
      if (err) return res.status(500).send(err);
      res.status(201).send({ message: 'Product added to wishlist!' });
  });
});


//when clicking the add to cart
app.post('/addToCart', (req, res) => {
  const { customer_id, product_id, quantity } = req.body;

  // Find or create the cart for the customer
  db.query(
      `SELECT cart_id FROM Shopping_Cart WHERE customer_id = ?`,
      [customer_id],
      (err, results) => {
          if (err) throw err;

          let cart_id;
          if (results.length > 0) {
              cart_id = results[0].cart_id;
              addProductToCart(cart_id, product_id, quantity, res);
          } else {
              // Create a new cart
              db.query(
                  `INSERT INTO Shopping_Cart (customer_id) VALUES (?)`,
                  [customer_id],
                  (err, result) => {
                      if (err) throw err;
                      cart_id = result.insertId;
                      addProductToCart(cart_id, product_id, quantity, res);
                  }
              );
          }
      }
  );
});

// Function to add a product to the cart
function addProductToCart(cart_id, product_id, quantity, res) {
  // Check if the product is already in the cart
  db.query(
      `SELECT cart_item_id, quantity FROM Cart_Items WHERE cart_id = ? AND product_id = ?`,
      [cart_id, product_id],
      (err, results) => {
          if (err) throw err;

          if (results.length > 0) {
              // Update the quantity if the product is already in the cart
              const newQuantity = results[0].quantity + quantity;
              db.query(
                  `UPDATE Cart_Items SET quantity = ? WHERE cart_item_id = ?`,
                  [newQuantity, results[0].cart_item_id],
                  (err) => {
                      if (err) throw err;
                      res.send({ message: 'Cart updated successfully' });
                  }
              );
          } else {
              // Insert the product into the cart if it's not already there
              db.query(
                  `INSERT INTO Cart_Items (cart_id, product_id, quantity) VALUES (?, ?, ?)`,
                  [cart_id, product_id, quantity],
                  (err) => {
                      if (err) throw err;
                      res.send({ message: 'Product added to cart successfully' });
                  }
              );
          }
      }
  );
}
app.post('/sellers', (req, res) => {
    const {
        firstName, lastName, email, password,
        phoneNumber, businessName, addressLine1, addressLine2,
        state, city, postalZipcode, country, businessRegNumber
    } = req.body;

    if (
        !firstName ||
        !lastName ||
        !email ||
        !password ||
        !phoneNumber ||
        !businessName ||
        !addressLine1 ||
        !addressLine2 ||
        !state ||
        !city ||
        !postalZipcode ||
        !country ||
        !businessRegNumber
    ) {
        return res.status(400).json({ error: "Please provide all the details" });
    }

    const sql = `
    INSERT INTO seller (firstName, lastName, email, password,
      phoneNumber, businessName, addressLine1, addressLine2,
      state, city, postalZipcode, country, businessRegNumber, createdAt, updatedAt
    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())
  `;

    db.query(
        sql,
        [
            firstName,
            lastName,
            email,
            password,
            phoneNumber,
            businessName,
            addressLine1,
            addressLine2,
            state,
            city,
            postalZipcode,
            country,
            businessRegNumber,
        ],
        (err, result) => {
            if (err) {
                console.error("Error inserting seller:", err);
                res.status(500).json({ error: "Error while inserting data" });
            } else {
                console.log("Seller added:", result);
                res.status(201).json({
                    id: result.insertId,
                    firstName,
                    lastName,
                    email,
                    password,
                    phoneNumber,
                    businessName,
                    addressLine1,
                    addressLine2,
                    state,
                    city,
                    postalZipcode,
                    country,
                    businessRegNumber,
                });
            }
        }
    );
});

app.get("/stores", getStores);
app.post("/stores", createStore);

const PORT = 4000;
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
