const db = require("./dbConfig");

function createStoreTable() {
  const sql = `
        CREATE TABLE IF NOT EXISTS store (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            image VARCHAR(255) NOT NULL,
            category VARCHAR(255) NOT NULL,
            createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        )
    `;

  db.query(sql, (err, result) => {
    if (err) {
      console.error("Error while creating store table", err);
    } else {
      console.log("store table successfuly created");
    }
  });
}

module.exports = createStoreTable;
