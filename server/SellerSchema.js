const db = require('./dbConfig');

function createSellerTable() {
    const sql = `
        CREATE TABLE IF NOT EXISTS seller (
            id INT AUTO_INCREMENT PRIMARY KEY,
            firstName VARCHAR(255) NOT NULL,
            lastName VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL UNIQUE,
            password VARCHAR(255) NOT NULL,
        
            phoneNumber VARCHAR(20),
            businessName VARCHAR(255),
            addressLine1 VARCHAR(255),
            addressLine2 VARCHAR(255),
            state VARCHAR(255),
            city VARCHAR(255),
            postalZipcode VARCHAR(20),
            country VARCHAR(255),
            businessRegNumber VARCHAR(255),
            createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        )
    `;

    db.query(sql, (err, result) => {
        if (err) {
            console.error('Error while creating database', err);
        } else {
            console.log("SELLER TABLE SUCCESSFULLY CREATED !!");
        }
    });
}

module.exports = createSellerTable;
