const sql = require('mysql2')

const db = sql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "zcommerse"
})

db.connect( (err) => {
    if(err){
        console.error('Database Connection Error : ', err)
    }
    else{
        console.log('Database connected successfully')
    }
} )
module.exports=db