const db = require("./dbConfig");

function createCartItemsTable() {
    const stm = `CREATE TABLE IF NOT EXISTS Cart_Items (
    cart_item_id INT AUTO_INCREMENT PRIMARY KEY,
    cart_id INT,
    product_id INT,
    quantity INT,
    FOREIGN KEY (cart_id) REFERENCES Shopping_Cart(cart_id),
    FOREIGN KEY (product_id) REFERENCES Products(product_id)
);`;


    db.query(stm, (err) => {
        if (err) {
            console.error('error while creating cart table', err);
        }
        else {
            console.log('Cart items table ready.');
        }
    });
}
module.exports = createCartItemsTable;