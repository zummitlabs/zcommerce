import TestimonalCard from "./TestimonalsCard";
import { useRef, useEffect } from 'react';

function Testimonal() {
  const scrollContainerRef = useRef(null);

  useEffect(() => {
    const handleWheel = (e) => {
      if (e.deltaY !== 0 && scrollContainerRef.current) {
        e.preventDefault();
        scrollContainerRef.current.scrollLeft += e.deltaY;
      }
    };

    const container = scrollContainerRef.current;
    if (container) {
      container.addEventListener('wheel', handleWheel, { passive: false });
    }

    return () => {
      if (container) {
        container.removeEventListener('wheel', handleWheel);
      }
    };
  }, []);

  const data_for_testimonal = [
    {
      title: "“Seamless Shopping and Selling Experience.”",
      review:
        "I recently started using the Zcommerce application and I must say, it has completely changed the way I shop online. This app not only provides a seamless shopping experience, but also offers a selling feature that has been incredibly beneficial for me.",
      name: "Chris Hemming",
      job: "Marketing Director",
      img: "https://qph.cf2.quoracdn.net/main-thumb-270870595-200-egtycjbuiltykyeigutohwoogpzjmzxq.jpeg",
    },
    {
      title: "“Smooth Navigation with a satisfying user journey.”",
      review:
        "As a regular online shopper, I appreciate the user-friendly interface and intuitive design of the app. It is extremely easy to navigate through different categories, search for products, and make purchases.",
      name: "Natalie James",
      job: "Business Analyst",
      img: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.n5CeR93916slWXGyV13PuAHaHa%26pid%3DApi&f=1&ipt=29b679d34c26b4b61c48f6b170f5f807c2b7ddaf3d38bc16b712f39739b52e76&ipo=images",
    },
    {
      title: "“Seamless Shopping and Selling Experience.”",
      review:
        "I recently started using the Zcommerce application and I must say, it has completely changed the way I shop online. This app not only provides a seamless shopping experience, but also offers a selling feature that has been incredibly beneficial for me.",
      name: "Chris Hemming",
      job: "Marketing Director",
      img: "https://qph.cf2.quoracdn.net/main-thumb-270870595-200-egtycjbuiltykyeigutohwoogpzjmzxq.jpeg",
    },
    {
      title: "“Seamless Shopping and Selling Experience.”",
      review:
        "I recently started using the Zcommerce application and I must say, it has completely changed the way I shop online. This app not only provides a seamless shopping experience, but also offers a selling feature that has been incredibly beneficial for me.",
      name: "Chris Hemming",
      job: "Marketing Director",
      img: "https://qph.cf2.quoracdn.net/main-thumb-270870595-200-egtycjbuiltykyeigutohwoogpzjmzxq.jpeg",
    },
    {
      title: "“Seamless Shopping and Selling Experience.”",
      review:
        "I recently started using the Zcommerce application and I must say, it has completely changed the way I shop online. This app not only provides a seamless shopping experience, but also offers a selling feature that has been incredibly beneficial for me.",
      name: "Chris Hemming",
      job: "Marketing Director",
      img: "https://qph.cf2.quoracdn.net/main-thumb-270870595-200-egtycjbuiltykyeigutohwoogpzjmzxq.jpeg",
    },
    {
      title: "“Seamless Shopping and Selling Experience.”",
      review:
        "I recently started using the Zcommerce application and I must say, it has completely changed the way I shop online. This app not only provides a seamless shopping experience, but also offers a selling feature that has been incredibly beneficial for me.",
      name: "Mark Hemming",
      job: "Marketing Director",
      img: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.Xo89Fu2pDKhSHLMLO9-OcAHaHa%26pid%3DApi&f=1&ipt=93be95a2611d217748512e38931e78b2f89be9624734e1498c5e313acf860f96&ipo=images",
    },
    {
      title: "“Smooth Navigation with a satisfying user journey.”",
      review:
        "I recently started using the Zcommerce application and I must say, it has completely changed the way I shop online. This app not only provides a seamless shopping experience, but also offers a selling feature that has been incredibly beneficial for me.",
      name: "Chris Hemming",
      job: "Marketing Director",
      img: "https://qph.cf2.quoracdn.net/main-thumb-270870595-200-egtycjbuiltykyeigutohwoogpzjmzxq.jpeg",
    },
  ];

  return (
    <div className=" mt-[24px] mb-[97px]">
      <div>
        <div className="m-auto  flex justify-center max-md:w-[130px]  ">
          <svg
            width="164"
            height="120"
            viewBox="0 0 164 120"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M14.7854 109.656C5.34375 100.057 0.274582 89.2915 0.274582 71.8403C0.274582 41.1318 22.7971 13.6082 55.5496 0L63.7354 12.0904C33.1646 27.9184 27.1879 48.458 24.8046 61.4082C29.7271 58.9691 36.1712 58.118 42.4871 58.6795C59.0238 60.1448 72.0588 73.1388 72.0588 89.2915C72.0588 97.4359 68.6786 105.247 62.6618 111.006C56.645 116.765 48.4844 120 39.9754 120C30.1396 120 20.7346 115.701 14.7854 109.656ZM106.452 109.656C97.0104 100.057 91.9413 89.2915 91.9413 71.8403C91.9413 41.1318 114.464 13.6082 147.216 0L155.402 12.0904C124.831 27.9184 118.855 48.458 116.471 61.4082C121.394 58.9691 127.838 58.118 134.154 58.6795C150.69 60.1448 163.725 73.1388 163.725 89.2915C163.725 97.4359 160.345 105.247 154.328 111.006C148.312 116.765 140.151 120 131.642 120C121.806 120 112.401 115.701 106.452 109.656Z"
              fill="#3B0668"
            />
          </svg>
        </div>
        <div className="flex flex-col mt-[64px] justify-center font-poppins text-center content-center">
          <hr className="w-[72px] h-[8px] mx-auto bg-[#7c2ccc] border-0 rounded-[24px] dark:bg-[#7c2ccc]" />
          <h1 className="text-[36px] font-semibold">
            Hear It From Our Customers!
          </h1>
          <span className="text-[21px] space-y-[16px] font-normal font-open-sans text-[#474747]">
            See what our customers have to say after using ZCommerce.
          </span>
        </div>
      </div>
      <div className="flex flex-row mt-[96px] overflow-x-auto  overflow-y-auto hide-scrollbar scroll-smooth ">
        {data_for_testimonal.map((data, id) => (
          <TestimonalCard
            key={id}
            title={data.title}
            review={data.review}
            name={data.name}
            job={data.job}
            img={data.img}
          />
        ))}
      </div>
    </div>
  );
}

export default Testimonal;
