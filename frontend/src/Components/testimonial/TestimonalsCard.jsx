/* eslint-disable react/prop-types */
function TestimonalsCard({ title, review, name, job, img }) {
  return (
    <div className="m-5 cursor-pointer  ">
      <div className="border-r max-md:ml-2  ml-4 max-md:w-[350px]  max-md:h-[400px]   border-[#787676] w-[594px] h-[517px] border-b border-l shadow-lg rounded-tr-[170px] rounded-bl-[171px]  rounded-l  border-t   bg-white  rounded-r  flex flex-col justify-between leading-normal">
        <div className="w-[549px] max-md:w-[280px] max-md:ml-12 overflow-hidden max-md:h-auto max-md:p-1  h-auto ml-[60px]  p-8 gap-[16px] m-auto  ">
          <div className=" w-auto h-auto">
            <div className="bg-[#ddb2ff]  max-md:h-1/6 mb-5 text-center content-center  w-[143px] h-[53px]">
              <span className="font-open-sans  text-[21px] font-bold text-white">
                Brand logo
              </span>
            </div>
            <div className="text-gray-900 max-md:text-[15px] max-md:leading-6 text-[24px] font-semibold leading-[28.8px] mb-4">
              {title}
            </div>
          </div>
          <div className="max-md:w-[250px] w-[440px]  h-auto">
            <p className="text-black  text-[21px] max-md:text-[12px] max-md:leading-4  leading-[25.2px] font-open-sans font-normal text-base">
              {review}
            </p>
          </div>

          <div className="flex mt-5  items-center">
            <img
              className="w-[84px] h-[86px] max-md:w-[50px] max-md:h-[50px] rounded-full mr-4"
              src={img}
              alt="Avatar"
            />
            <div className="text-sm">
              <p className="text-balck text-[21px] max-md:text-[12px] leading-none">
                {name}
              </p>
              <p className="text-[#474747]">{job}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TestimonalsCard;
