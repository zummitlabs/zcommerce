// src/components/WhyChooseUs.jsx
import whychooseus from '../assets/images/whychooseus.png'
import { useNavigate } from 'react-router-dom';
const WhyChooseUs = () => {
    const navigate=useNavigate();
    return (
    <>
    <div className="px-24 flex flex-col lg:flex-row items-center justify-center p-6">
    <div className="flex-shrink-3 min-w-40 p-28 pb-0 mb-6 lg:mb-0 lg:mr-6">
        <img src={whychooseus} alt="People working on a laptop" className="rounded-lg min-w-64" />
    </div>
    <div className="text-center mt-28 lg:text-left max-w-[420px]">
        <h2 className="text-2xl font-poppins font-semibold mb-4">Why Choose Us</h2>
        <p className="font-open-sans font-normal text-gray-600 mb-6">
        Transform your business with our e-commerce platform designed for sellers. Our 
advanced features ensure a seamless and efficient selling experience.
        </p>
        <ul>
        <li className="font-poppins font-semibold border-2 border-purple-700 p-1 rounded-[16px] flex items-center mb-4">
            <div className=" bg-yellow-500 text-white rounded-full w-8 h-8 flex items-center justify-center font-bold mr-4">1</div>
            <span className="text-lg font-sans">Set up your store effortlessly</span>
        </li>
        <li className="font-poppins font-semibold border-2 border-purple-700 p-1 rounded-[16px] flex items-center mb-4">
            <div className=" bg-yellow-500 text-white rounded-full w-8 h-8 flex items-center justify-center font-bold mr-4">2</div>
            <span className="text-lg font-sans">Intuitive and user-friendly interface</span>
        </li>
        <li className="font-poppins font-semibold border-2 border-purple-700 p-1 rounded-[16px] flex items-center mb-4">
            <div className=" bg-yellow-500 text-white rounded-full w-8 h-8 flex items-center justify-center font-bold mr-4">3</div>
            <span className="text-lg font-sans"> Wide range of customizable designs and themes </span>
        </li>
        </ul>
    </div>
    </div>
    <div className='grid mt-5 mb-24  place-items-center h-[50px]'>
        <button
            onClick={()=>navigate('/sellerregistration')}
            className="shadow-custom-inset px-12 py-3 bg-purple-800 text-white font-poppins font-bold rounded-lg transition-transform transform hover:scale-105 active:scale-95"
        
        >
        Start Selling
        </button>
    </div>
    <SignupPrompt/>
    </>
    );
};

const SignupPrompt = () => {
    return (
    <div className="mb-12 px-4 gap-6 flex justify-center w-full border-t border-b border-gray-300 py-4">
    <div className='font-open-sans font-normal cursor-pointer'>
        Just have few items to sell ?
    </div>
    <div className='font-open-sans font-semibold cursor-pointer'>
        Sign up to become an individual seller
    </div>
    </div>
    );
};

export default WhyChooseUs;
