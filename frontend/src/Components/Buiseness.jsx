import { useNavigate } from "react-router-dom"




function Buiseness(){
    const navigate=useNavigate();
    return (
        <>
      <div className="bg-custom-gradient h-96 text-white w-full flex flex-col items-center justify-center p-4">
  <h1 className="font-bold text-5xl mb-6 mt-12">Grow Your Business Here</h1>
  <p className="mt-6 text-2xl flex flex-col items-center justify-center text-center">
    From Setup to Sales, we provide everything you 
    <span>need to start and grow your online store.</span>
  </p>
  <button
    onClick={()=>navigate('/sellerregistration')}
    className="mt-8 font-bold text-2xl  bg-white text-purple-800 px-4 py-2 rounded border-2 border-transparent hover:bg-purple-800 hover:text-white">
    Start your Free Trial Now
  </button>
</div>
        </>
    )
}
export default Buiseness