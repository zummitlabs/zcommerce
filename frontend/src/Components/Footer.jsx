import { Link } from 'react-router-dom';

import x from '../assets/socials/x.png';
import fb from '../assets/socials/fb.png'
import insta from '../assets/socials/insta.png'
import yt from '../assets/socials/yt.png'

const Footer = () => {
    return (
        <footer className="text-black py-20 overflow-hidden">
        <div className="container mx-auto px-4 md:px-8 lg:px-16 text-black">
            <div className="flex gap-x-8 justify-around">
                    {/* Product */}
                    <div className='text-black'>
                        <h5 className="text-lg font-normal font-open-sans mb-4">Product</h5>
                        <ul className="text-black font-open-sans text-xl">
                            <li className="mb-2">
                                <Link to="/" className="hover:underline font-open-sans font-semibold">Templates</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="/ " className="hover:underline font-open-sans font-semibold">Examples</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="/ " className="hover:underline font-open-sans font-semibold">Pricing</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="/ " className="hover:underline font-open-sans font-semibold">For Makers</Link>
                            </li>
                        </ul>
                    </div>
                    {/* Resources */}
                    <div className=''>
                        <h5 className="text-lg font-open-sans font-normal mb-4">Resources</h5>
                        <ul className="text-black font-semibold text-xl">
                            <li className="mb-2">
                                <Link to="/" className="hover:underline font-open-sans font-semibold">Blog</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="/ " className="hover:underline font-open-sans font-semibold">Help</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="/ " className="hover:underline font-open-sans font-semibold">Developers</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="/ " className="hover:underline font-open-sans font-semibold">Contact Us</Link>
                            </li>
                        </ul>
                    </div>
                    {/* Company */}
                    <div className=''>
                        <h5 className="text-lg font-normal font-open-sans mb-4">Company</h5>
                        <ul className="text-black font-semibold text-xl">
                            <li className="mb-2">
                                <Link to=" /" className="hover:underline font-open-sans font-semibold">About</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="/ " className="hover:underline font-open-sans font-semibold">Jobs</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="/ " className="hover:underline font-open-sans font-semibold">Newsletter</Link>
                            </li>
                        </ul>
                    </div>

                    {/* Compare */}
                    <div className=''>
                        <h5 className="text-lg font-normal font-open-sans mb-4">Compare</h5>
                        <ul className="text-black font-semibold text-xl">
                            <li className="mb-2">
                                <Link to="https://www.etsy.com/in-en/" className="hover:underline font-open-sans font-semibold">Etsy</Link>
                            </li>
                            <li className="mb-2 mt-3">
                                <Link to="https://www.wix.com/" className="hover:underline font-open-sans font-semibold">Wix</Link>
                            </li>
                            <li className="mb-2 mt-3 ">
                                <Link to="https://www.squarespace.com/" className="hover:underline font-open-sans font-semibold">Squarespace</Link>
                            </li>
                        </ul>
                    </div>

                    {/* zCommerce */}
                    <div className=' w-fit col-span-1 md:col-span-5 lg:col-span-3 xl:col-span-3 flex justify-center'>
                        <div>
                        <div className="font-bold text-4xl lg:text-5xl bg-gradient-to-r from-black to-gray-200 text-transparent bg-clip-text mb-4">
                            zCommerce
                        </div>
                        <div className='flex  justify-around gap-4'>
                            <Link to='https://www.instagram.com/zummitinfolabs/?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw%3D%3D' className='w-8 h-8'><img src={insta} alt="Instagram" className='w-full h-full object-contain'/></Link>
                            <Link to='https://www.facebook.com/Zummitinfolabs' className='w-8 h-8'><img src={fb} alt="Facebook" className='w-full h-full object-contain'/></Link>
                            <Link to='https://x.com/zummiti' className='w-8 h-8'><img src={x} alt="X" className='w-full h-full object-contain'/></Link>
                            <Link to='https://www.youtube.com/channel/UC-SMgvrECm81JUF6JIPm6pA/about' className='w-8 h-8'><img src={yt} alt="YouTube" className='w-full h-full object-contain'/></Link>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
    </footer>
  );
};

export default Footer;
