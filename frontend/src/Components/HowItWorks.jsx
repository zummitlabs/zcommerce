import  { useState } from 'react';
import signup from '../assets/images/signup.png';
import customize from '../assets/images/customize.png';
import launch from '../assets/images/launch.png';
import SmallBar from './SmallBar';

const HowItWorks = () => {
  const [hoveredImage, setHoveredImage] = useState(null);

  const handleMouseEnter = (image) => {
    setHoveredImage(image);
  };

  const handleMouseLeave = () => {
    setHoveredImage(null);
  };

  return (
    <><div className="w-full flex flex-col items-center justify-center p-4">
    <div className="text-center mb-8">
      <div className="font-poppins text-3xl font-semibold leading-54 text-center">How It Works</div>
      <div className="font-open-sans text-base font-normal leading-31 text-center">Simple steps to get your store online.</div>
    </div>

    <div className="grid grid-cols-1 gap-8 md:grid-cols-3 md:gap-4 pb-6">
      {/* SIGN UP */}
      <div
        className="rounded overflow-hidden shadow-lg bg-white"
        onMouseEnter={() => handleMouseEnter('signup')}
        onMouseLeave={handleMouseLeave}
      >
        <div className="mx-auto flex items-center justify-center relative h-40 w-40 rounded-full bg-gradient-to-r from-[#FCAC16]/80 via-[#BB6C58] to-[#671AAE] p-1">
          <div className="flex h-full w-full bg-white items-center justify-center rounded-full ">
            <img
              className={`w-20 h-20 ${hoveredImage === 'signup' ? 'hidden' : 'hover-img'}`}
              src={signup}
              alt="Sign Up"
            />
            <img
              className={`w-20 h-20 absolute ${hoveredImage === 'signup' ? 'hover-gif' : 'hidden'}`}
              src="https://s3-alpha-sig.figma.com/img/3561/6448/354a6cd94b7113a2400486ee4227bfeb?Expires=1723420800&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=GIwbtJAtNpiRznvxM8lHtNndUY1vEIPtmtkcPx8wBfI3njqS-5bnltgMGYDrUHxTLyzrKHG8Uykk5Mvk1zl0UI~39aZ2gLe-xnLN8OzBhileTWaYxf37AC2O71m2~64nYZUy40nShqcerI4x~lZhm6W~NK5wTL1sSLGOBek4GjB5fcdcFZjoMYxASMTdVZokJHZn7sx49~4pbmSABmrjLapiDQOr4UFBFhe4g~ico-sZTSZed5eHqfwAp8ewHWXW8xZD~FLERzLM6f-HB21Bf7tvUdHFcO54G0AkcYC-MdeBBVVfXLpAhtR8~KAp2TSPN8pmu265T3a-AxkCxNPeqA__"
              alt="signup GIF"
            />
          </div>
        </div>
        <div className="px-6 py-4 text-center">
          <div className="font-poppins text-lg font-semibold leading-41 text-center">Sign Up</div>
          <p className="font-open-sans text-base font-semibold leading-27 text-center">
            Create your account in minutes.
          </p>
        </div>
      </div>

      {/* CUSTOMIZE */}
      <div
        className="rounded overflow-hidden shadow-lg bg-white"
        onMouseEnter={() => handleMouseEnter('customize')}
        onMouseLeave={handleMouseLeave}
      >
        <div className="mx-auto flex items-center justify-center relative h-40 w-40 rounded-full bg-gradient-to-r from-[#FCAC16]/80 via-[#BB6C58] to-[#671AAE] p-1">
          <div className="flex h-full w-full bg-white items-center justify-center rounded-full">
            <img
              className={`w-20 h-20 ${hoveredImage === 'customize' ? 'hidden' : ''}`}
              src={customize}
              alt="Customize"
            />
            <img
              className={`w-20 h-20 absolute ${hoveredImage === 'customize' ? 'hover-gif' : 'hidden'}`}
              src="https://s3-alpha-sig.figma.com/img/55ea/bb11/57b4f62b46decae499695328d1a23f51?Expires=1723420800&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Md-G41TjWRK1M77NKqsWUVuXNGXA9VvpoX-StUjRAvEv076Co3oAujQXrzeVZCzOZDAJRLmHGLQJ5JrUyA6mKLwm5KVzeG75mORV3~O56g8NUjtCw5GBVkcxOoDbOcMocU2BXUBk~fuVxaaNAcEjkWmA0bpVeYSz59OwyG5Nm~mOSMYajNQEpVYvQR8sKLL~GJg0Otw0zya2oD7OrPdrd3XjM8siPdqaNp1IPyMUTLF7SsD7TFaXIGgsyScThzDa3auEwCXd5MdNe3Hzybbw10MurNj8oNnonZ7D-oyM6SsxX24TUZBOJVQwJqQXLe2d751HpE~pSANMxOra1~jIMw__"
              alt="customize GIF"
            />
          </div>
        </div>
        <div className="px-6 py-4 text-center">
          <div className="font-poppins text-lg font-semibold leading-41 text-center">Customize</div>
          <p className="font-open-sans text-base font-semibold leading-27 text-center">
            Personalize your store with our easy-to-use tools.
          </p>
        </div>
      </div>

      {/* LAUNCH */}
      <div
        className="rounded overflow-hidden shadow-lg bg-white"
        onMouseEnter={() => handleMouseEnter('launch')}
        onMouseLeave={handleMouseLeave}
      >
        <div className="mx-auto flex items-center justify-center relative h-40 w-40 rounded-full bg-gradient-to-r from-[#FCAC16]/80 via-[#BB6C58] to-[#671AAE] p-1">
          <div className="flex h-full w-full bg-white items-center justify-center rounded-full">
            <img
              className={`w-20 h-20 ${hoveredImage === 'launch' ? 'hidden' : ''}`}
              src={launch}
              alt="Launch"
            />
            <img
              className={`w-20 h-20 absolute ${hoveredImage === 'launch' ? 'hover-gif' : 'hidden'}`}
              src="https://s3-alpha-sig.figma.com/img/c060/52a9/98874d4950019a80f7ae5ee40fe79c6d?Expires=1723420800&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=kOM2kT56pfk5yw0WXB6q39GpTI8FQMconqZ8-hSsaSeTC9YpYgopg1gFex~G5LZd2kd~CWkfKOQwyju-XQw2XL2Or6ruvQfda5PQmfcynBfsJ~wYMzYKiWIT03CdpLFnetYLKaLlPjWGn-4y5VESLro~JIENYrpqWjPz4tqi2Zj5xgxUdqKRM~qMdl4yB9WYF7nQN3pLF1ARM64Rpw40obwmBBlAoPQ0vZKwpp2fe0f8rBSGH33uiv2e-~hfBZ68JZ5W42hOb7yUsxjZItu~M7QSIox6BNfolGlX1Su3iyG~RfuqkztOORklU0YbiX-1ke061zWUpIjfrzY3GzPIoA__"
              alt="Launch GIF"
            />
          </div>
        </div>
        <div className="px-6 py-4 text-center">
          <div className="font-poppins text-lg font-semibold leading-41 text-center">Launch</div>
          <p className="font-open-sans text-base font-semibold leading-27 text-center">
            Launch your store with confidence.
          </p>
        </div>
      </div>
    </div>
  </div>
  <SmallBar/>
  </>
    
  );
};

export default HowItWorks;
