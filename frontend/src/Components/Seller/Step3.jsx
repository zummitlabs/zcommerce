import React from 'react';
import EmailVerify from '../../assets/images/EmailVerify.gif';

const Step3 = () => {
  return (
    <div className='flex flex-col justify-center items-center'>
      <div>
        <img
          src={EmailVerify}
          alt="emailverify"
          className="w-[200px] h-[200px] object-center "
        />
      </div>
      <div>
        <h1 className='text-[#FCAC16] font-[600] font-poppins text-[31px]'>Email Verification Required</h1>
      </div>
      <div>
        <p className='text-center font-[400] font-open-sans text-[18px]'>
          To complete your registration, please check your email for a verification link.
          <br />
          <br />
          If you don't see the email in your inbox, please check your spam or junk folder.
          <br />
          Thank you for joining us!
        </p>
      </div>
    </div>
  );
};

export default Step3;
