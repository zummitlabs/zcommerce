import React, { useState } from 'react';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';

const Step1 = ({
  formData,
  handleFormDataChange,
  errors,
  setErrors,
  handleCheckboxChange,
}) => {
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  
  
 const validateEmail = (email) => {
  const regex = /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[\W_]).{8,}$/;
  return regex.test(email)
 }

  const validatePassword = (password) => {
    const regex = /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[\W_]).{8,}$/;
    return regex.test(password);
  };

  //valid checkbox

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    handleFormDataChange({ ...formData, [name]: value });

    if (name === "password") {
      if (!validatePassword(value)) {
        setErrors((prevErrors) => ({
          ...prevErrors,
          password:
            "Password must be at least 8 characters long and include at least one letter, one digit, and one special character.",
        }));
      } else {
        setErrors((prevErrors) => {
          const newErrors = { ...prevErrors };
          delete newErrors.password;
          return newErrors;
        });
      }
    } else if (name === "confirmPassword") {
      if (value !== formData.password) {
        setErrors((prevErrors) => ({
          ...prevErrors,
          confirmPassword: "Passwords do not match.",
        }));
      } else {
        setErrors((prevErrors) => {
          const newErrors = { ...prevErrors };
          delete newErrors.confirmPassword;
          return newErrors;
        });
      }
    }
    else if(name==='email'){
      if(!validateEmail(value)){
        setErrors(prevErrors=>({
          ...prevErrors,
          email:"Invalid Email Address"
        }))
      } else{
        setErrors(prevErrors => {
          const newErrors = {...prevErrors};
          delete newErrors.email;
          return newErrors
        } )
      }
    }
  };


  const navigate=useNavigate();

  return (
    <>
      
      
      <div>
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Full Name*
        </label>
        <input
          type="text"
          name="firstName"
          
          value={formData.firstName}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
        />
        {errors.email && (
          <p className="text-red-400  text-sm">{errors.firstName}</p>
        )}
      </div>


      <div>
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Email Address*
        </label>
        <input
          type="email"
          name="email"
          
          value={formData.email}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
        />
        {errors.email && (
          <p className="text-red-400  text-sm">{errors.email}</p>
        )}
      </div>
      <div className="relative">
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Password*
        </label>
        <input
          type={showPassword ? "text" : "password"}
          name="password"
          value={formData.password}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#808080] border-[1px] rounded pr-10"
        />
        <div
          className="absolute inset-y-10 left-[490px] cursor-pointer"
          onClick={() => setShowPassword(!showPassword)}
        >
          {showPassword ? <FaEye /> : <FaEyeSlash />}
        </div>
        {errors.password && (
          <p className="text-red-400  text-sm mt-1">{errors.password}</p>
        )}
      </div>
      <div className="relative">
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Confirm Password*
        </label>
        <input
          type={showConfirmPassword ? "text" : "password"}
          name="confirmPassword"
          value={formData.confirmPassword}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#808080] border-[1px] rounded pr-10"
        />
        <div
          className="absolute inset-y-10 left-[490px] cursor-pointer"
          onClick={() => setShowConfirmPassword(!showConfirmPassword)}
        >
          {showConfirmPassword ? <FaEye /> : <FaEyeSlash />}
        </div>
        {errors.confirmPassword && (
          <p className="text-red-400  text-sm mt-1">{errors.confirmPassword}</p>
        )}
      </div>
      <div>
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Phone Number*
        </label>
        <input
          type="tel"
          name="phoneNumber"
          value={formData.phoneNumber}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#403838] border-[1px] rounded"
        />
        {errors.phoneNumber && (
          <p className="text-red-400  text-sm">{errors.phoneNumber}</p>
        )}
      </div>
      <div className="flex items-center w-[532px]">
        <input
          type="checkbox"
          id="terms"
          className="mr-3"
          value={formData.ischecked}
          onChange={(event) => handleCheckboxChange(event)}
        />
        <label htmlFor="terms" className="font-poppins text-[16px]">
          I agree to ZCommerce’s <span 
            className='text-[#530993] font-[500] cursor-pointer' 
            onClick={() => navigate('/terms-of-service')}
          >
            Terms of Service
          </span>{' '}
          and{' '}
          <span 
            className='text-[#530993] font-[500] cursor-pointer' 
            onClick={() => navigate('/privacy-policy')}
          >
            Privacy Policy
          </span>.
        </label>
      </div>
      
    </>
  );
};

export default Step1;
