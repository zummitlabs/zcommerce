import React, { useState } from "react";
import logo from "../../assets/images/logo.png";
import sellerregistration from "../../assets/images/sellerregistration.png";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import { postcodeValidator } from "postcode-validator";
import AuthHeader from "../Auth/AuthHeader";
import { useAuthSellerRegister } from "../../Hooks/Auth";
import { useNavigate } from "react-router-dom";


const SellerRegistrationForm = () => {
  const navigate = useNavigate();
  const {sellerRegister} = useAuthSellerRegister();
  const [isBoxChecked, setIsBoxChecked] = useState(false);
  const [currentStep, setCurrentStep] = useState(1);
  const [postalCodeValid, setpostalCodeValid] = useState(false);
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    phoneNumber: "",
    businessName: "",
    addressLine1: "",
    addressLine2: "",
    state: "",
    city: "",
    postalZipcode: "",
    country: "",
    businessRegNumber: "",
  });
  const [errors, setErrors] = useState({});

  // Define the steps array
  const steps = [
    "Storeowner details",
    "Business details",
    "Email Verification",
  ];

  //check box handler to check is checked or not
  const handleCheckboxChange = (event) => {
    if (event.target.checked) {
      setIsBoxChecked(true);
    } else {
      setIsBoxChecked(false);
    }
  };

  // validate email
  const isValidEmail = (email) => {
    // List of valid email providers
    const validProviders = [
      "gmail.com",
      "yahoo.com",
      "outlook.com",
      "hotmail.com",
    ];

    // List of invalid TLDs

    // Basic regex for email validation
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (!emailRegex.test(email)) {
      return false;
    }

    // Extract the domain part
    const domain = email.split("@")[1];
    const domainParts = domain.split(".");

    // Check for invalid structure like 'sam@.com'
    if (
      domainParts.length < 2 ||
      domainParts.some((part) => part.length === 0)
    ) {
      return false;
    }

    // Check if the domain is in the list of valid providers
    if (!validProviders.includes(domain)) {
      return false;
    }

    return true;
  };

  //phone number validate
  const isValidPhoneNumber = (phoneNumber) => {
    return true;
    // const pattern = new RegExp(
    //   "^\\+[1-9]{1}[0-9]{0,2}[\\s-]?[2-9]{1}[0-9]{2}[\\s-]?[2-9]{1}[0-9]{2}[\\s-]?[0-9]{4}$"
    // );
    // if (pattern.test(phoneNumber)) {
    //   return true;
    // }
    // return false;
  };

  //postalcode Validator
  const postalCodevalidate = (countryCode) => {
    const isValid = postcodeValidator(formData.postalZipcode, countryCode);
    if (isValid) {
      setpostalCodeValid(true);
    } else {
      setpostalCodeValid(false);
    }
  };

  // Function to validate step 1 fields
  const validateStep1 = () => {
    const {
      firstName,
      lastName,
      email,
      password,
      confirmPassword,
      phoneNumber,
    } = formData;
    const newErrors = {};

    if (!firstName) newErrors.firstName = "First Name is required";
    if (!lastName) newErrors.lastName = "Last Name is required";
    if (!email) newErrors.email = "Email is required";
    if (!isValidEmail(email)) newErrors.email = "Enter Valid Email";
    if (!password) newErrors.password = "Password is required";
    if (!confirmPassword)
      newErrors.confirmPassword = "Confirm Password is required";
    if (password !== confirmPassword)
      newErrors.confirmPassword = "Passwords do not match";
    if (!phoneNumber) newErrors.phoneNumber = "Phone Number is required";
    if (!isValidPhoneNumber(phoneNumber))
      newErrors.phoneNumber = "Enter valid Phone Number";
    if (!isBoxChecked) newErrors.checkedBox = alert("Check the CheckBox"); //checkBox checking
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  // Function to validate step 2 fields
  const validateStep2 = () => {
    const {
      businessName,
      addressLine1,
      addressLine2,
      state,
      city,
      postalZipcode,
      country,
      businessRegNumber,
    } = formData;
    const newErrors = {};

    if (!businessName) newErrors.businessName = "Business Name is required";
    if (!addressLine1) newErrors.addressLine1 = "Address Line 1 is required";
    if (!addressLine2) newErrors.addressLine2 = "Address Line 2 is required";
    if (!state) newErrors.state = "State/Region is required";
    if (!city) newErrors.city = "City is required";
    if (!postalZipcode) newErrors.postalZipcode = "Postal Zipcode is required";
    if (!postalCodeValid)
      newErrors.postalZipcode = "Enter valid Postal Zipcode";
    if (!country) newErrors.country = "Country is required";
    if (!businessRegNumber)
      newErrors.businessRegNumber = "Business Registration Number is required";

    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  // Function to handle the "Next" button click
  const handleNext = async () => {
    console.log(formData);
    const role = "SELLER";
    const response = await sellerRegister(formData, role );
    if(response.success){
      navigate('/verify')
      
    }
  }
  

  // Function to handle the "Back" button click
  const handleBack = () => {
    if (currentStep > 1) setCurrentStep(currentStep - 1);

    //if back from from 2 then reset the checkBox
    if (currentStep === 2) {
      setIsBoxChecked(false);
    }
  };

  // Function to handle form data changes
  const handleFormDataChange = (newData) => {
    setFormData((prevData) => ({
      ...prevData,
      ...newData,
    }));
  };

  return (
    <div className="flex flex-col h-screen">
      {/* Left Section */}
      <div className="width-full">
        <AuthHeader />
      </div>

      {/* Right Section */}
      <div className="flex-1 flex flex-col justify-center items-center px-10 ">
        <div className="w-full max-w-md">
          {currentStep < 3 && (
            <>
              <h1 className="font-bold text-3xl text-center mb-3">
                Welcome to ZCommerce!
              </h1>
              <p className="text-gray-600 text-center mb-4">
                This is the beginning of your seller registration journey.
              </p>
            </>
          )}
          <form className="space-y-4">
            {currentStep === 1 && (
              <Step1
                formData={formData}
                handleFormDataChange={handleFormDataChange}
                errors={errors}
                setErrors={setErrors}
                handleCheckboxChange={handleCheckboxChange}
              />
            )}
              
            <div className="relative w-[522px] pt-5">
              {currentStep > 1 && currentStep < 3 && (
                <button
                  type="button"
                  className="bg-gradient-to-r from-[#7C2CCC] to-[#530993] w-[100px] h-[40px] text-white rounded-lg font-[600] font-open-sans text-[18px] absolute left-0"
                  onClick={handleBack}
                >
                  Back
                </button>
              )}
              {currentStep < 3 && (
                <button
                  type="button"
                  className="bg-gradient-to-r from-[#7C2CCC] to-[#530993] w-[100px] h-[40px] text-white rounded-lg font-[600] font-open-sans text-[18px] absolute right-0 "
                  onClick={handleNext}
                >
                  Next
                </button>
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SellerRegistrationForm;
