/* eslint-disable react/prop-types */
import React from "react";
import {
  CitySelect,
  CountrySelect,
  StateSelect,
} from "react-country-state-city";
import "react-country-state-city/dist/react-country-state-city.css";
import { useState } from "react";

const Step2 = ({
  formData,
  handleFormDataChange,
  errors,
  setErrors,
  postalCodevalidate,
}) => {
  const [countryid, setCountryid] = useState(0);
  const [stateid, setstateid] = useState(0);
  const [countryCode, setCountryCode] = useState(0);
  const [cityid, setCityid] = useState(0);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    handleFormDataChange({ [name]: value });
  };

  const validateStep2 = () => {
    const {
      businessName,
      addressLine1,
      addressLine2,
      state,
      city,
      postalZipcode,
      country,
      businessRegNumber,
    } = formData;

    const newErrors = {};

    if (!businessName) newErrors.businessName = "Business Name is required";
    if (!addressLine1) newErrors.addressLine1 = "Address Line 1 is required";
    if (!addressLine2) newErrors.addressLine2 = "Address Line 2 is required";
    if (!state) newErrors.state = "State/Region is required";
    if (!city) newErrors.city = "City is required";
    if (!postalZipcode) newErrors.postalZipcode = "Postal Zipcode is required";
    if (!country) newErrors.country = "Country is required";
    if (!businessRegNumber)
      newErrors.businessRegNumber = "Business Registration Number is required";
    if (!postalCodevalidate(postalZipcode))
      newErrors.postalZipcode = "please enter a validPostal Zipcode";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  return (
    <>
      <div>
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Business Name*
        </label>
        <input
          type="text"
          name="businessName"
          value={formData.businessName}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
        />
        {errors.businessName && (
          <p className="text-red-500 text-sm">{errors.businessName}</p>
        )}
      </div>
      <div>
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Address Line 1*
        </label>
        <input
          type="text"
          name="addressLine1"
          value={formData.addressLine1}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
        />
        {errors.addressLine1 && (
          <p className="text-red-500 text-sm">{errors.addressLine1}</p>
        )}
      </div>
      <div>
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Address Line 2*
        </label>
        <input
          type="text"
          name="addressLine2"
          value={formData.addressLine2}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
        />
        {errors.addressLine2 && (
          <p className="text-red-500 text-sm">{errors.addressLine2}</p>
        )}
      </div>
      <div className="flex space-x-4">
        <div className="w-1/2">
          <label className="block font-[400] font-open-sans text-[16px] mb-1">
            State/Region*
          </label>
          {/* <input
            type="text"
            name="state"
            value={formData.state}
            onChange={handleInputChange}
            className="w-[232px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
          /> */}

          <StateSelect
            countryid={countryid}
            //value={formData.state}
            onChange={(e) => {
              setstateid(e.id);
              formData.state = e.name;
            }}
            placeHolder="Select State"
          />

          {errors.state && (
            <p className="text-red-500 text-sm">{errors.state}</p>
          )}
        </div>
        <div className="w-1/2">
          <label className="block ml-[50px] font-[400] font-open-sans text-[16px] mb-1">
            City*
          </label>
          {/* <input
            type="text"
            name="city"
            value={formData.city}
            onChange={handleInputChange}
            className="ml-[50px] w-[232px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
          /> */}
          <CitySelect
            countryid={countryid}
            stateid={stateid}
            onChange={(e) => {
              setCityid(e.id);
              formData.city = e.name;
            }}
            placeHolder="Select City"
          />

          {errors.city && <p className="text-red-500 text-sm">{errors.city}</p>}
        </div>
      </div>
      <div className="flex space-x-4">
        <div className="w-1/2">
          <label className="block font-[400] font-open-sans text-[16px] mb-1">
            Postal Zipcode*
          </label>
          <input
            type="text"
            name="postalZipcode"
            value={formData.postalZipcode}
            onChange={(e) => {
              handleInputChange(e);
              postalCodevalidate(countryCode);
            }}
            className="w-[232px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
          />
          {errors.postalZipcode && (
            <p className="text-red-500 text-sm">{errors.postalZipcode}</p>
          )}
        </div>
        <div className="w-1/2">
          <label className="block ml-[50px] font-[400] font-open-sans text-[16px] mb-1">
            Country*
          </label>
          {/* <input
            type="text"
            name="country"
            value={formData.country}
            onChange={handleInputChange}
            className="ml-[50px] w-[232px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
          /> */}
          <CountrySelect
            onChange={(e) => {
              console.log(e);
              setCountryid(e.id);
              setCountryCode(e.iso2);
              formData.country = e.name;
            }}
            placeHolder="Select Country"
          />
          {errors.country && (
            <p className="text-red-500 text-sm">{errors.country}</p>
          )}
        </div>
      </div>
      <div>
        <label className="block font-[400] font-open-sans text-[16px] mb-1">
          Business Registration Number*
        </label>
        <input
          type="text"
          name="businessRegNumber"
          value={formData.businessRegNumber}
          onChange={handleInputChange}
          className="w-[522px] h-[38px] p-2 border-[#808080] border-[1px] rounded"
        />

        {errors.businessRegNumber && (
          <p className="text-red-500 text-sm">{errors.businessRegNumber}</p>
        )}
      </div>
    </>
  );
};

export default Step2;
