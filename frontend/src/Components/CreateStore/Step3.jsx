import  { useState, useEffect } from "react";
import axios from "axios";
import PropTypes from 'prop-types'

const Step3 = ({ formData, handleFormDataChange, errors, setErrors }) => {
const [categories, setCategories] = useState([]);
const [loading, setLoading] = useState(true);

console.log("Errors ki category", errors.category || 'no category');

useEffect(() => {
    axios.get("https://fakestoreapi.com/products/categories")
    .then(response => {
        setCategories(response.data);
        setLoading(false);
    })
    .catch(error => {
        console.error("Error fetching categories:", error);
        setLoading(false);
});
}, []);

const handleChange = (e) => {
    handleFormDataChange({ category: e.target.value });
    if (errors && errors.category) {
    setErrors((prevErrors) => ({ ...prevErrors, category: null }));
    }
};

return (
    <div className="w-full max-w-lg mx-auto">
    <h2 className="text-2xl font-semibold text-center mb-4">Tell us more about your shop.</h2>
    <p className="text-center text-gray-600 mb-6">
        Help us understand more about your shop by selecting what category you identify with.
        This will also help potential customers find your store faster.
    </p>
    {loading ? (
        <p className="text-center text-gray-600">Loading categories...</p>
    ) : (
        <select
        value={formData.category || ""}
        onChange={handleChange}
        className="w-full p-3 border rounded-lg mb-2"
        >
        <option value="" disabled>Select a category</option>
        {categories.map((category, index) => (
            <option key={index} value={category}>{category}</option>
        ))}
        </select>
    )}
    {errors?.category && <p className="text-red-500 text-sm">{errors.category}</p>}
    </div>
);
};

Step3.propTypes = {
    formData: PropTypes.object.isRequired,
    handleFormDataChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    setErrors: PropTypes.func.isRequired
};
export default Step3;
