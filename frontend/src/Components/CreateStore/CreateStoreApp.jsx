import { useState } from "react";
import { useNavigate } from "react-router-dom";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import Step4 from "./Step4";
import Step5 from "./Step5";

const CreateStoreApp = () => {
  const [currentStep, setCurrentStep] = useState(1);
  const [formData, setFormData] = useState({
    shopName: "",
    imgLink: "",
    category: "",
    accountName: "",
    cardNumber: "",
    expires: "",
    cvv: "",
    addressLine1: "",
    addressLine2: "",
    state: "",
    city: "",
    postalZipcode: "",
    country: ""
  });
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();

  const steps = [
    "Shop Name",
    "Shop Logo",
    "Shop Details",
    "Payment",
    "Shipping"
  ];

  const validateStep1 = () => {
    const { shopName } = formData;
    let newErrors = {};
    if (!shopName) {
      newErrors.shopName = "Shop Name is required";
    } else if (shopName.length < 5 || shopName.length > 20) {
      newErrors.shopName = "Shop Name must be between 5 and 20 characters";
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const validateStep3=()=>{
    const {category}=formData;
    console.log(category)
    const newErrors={}
    console.log(category+"my shope")
    if(!category) newErrors.category="shopCategory required";
    setErrors(newErrors);
    return Object.keys(newErrors).length===0;
  }

  const validateStep4 = () => {
    const { accountName, cardNumber, expires, cvv } = formData;
    const newErrors = {};
    if (!accountName) newErrors.accountName = "Account Name is required";
    if (!cardNumber) newErrors.cardNumber = "Card Number is required";
    if (!expires) newErrors.expires = "Expiration date is required";
    if (!cvv) newErrors.cvv = "CVV is required";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const validateStep5 = () => {
    const { addressLine1, state, city, postalZipcode, country } = formData;
    const newErrors = {};
    if (!addressLine1) newErrors.addressLine1 = "Address Line 1 is required";
    if (!state) newErrors.state = "State/Region is required";
    if (!city) newErrors.city = "City is required";
    if (!postalZipcode) newErrors.postalZipcode = "Postal/Zip Code is required";
    if (!country) newErrors.country = "Country is required";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleNext = () => {
    let isValid = false;
    if (currentStep === 1) {
      isValid = validateStep1();
      console.log("first step validation :", isValid);
    } else if (currentStep === 2) {
      isValid = true;
    } else if (currentStep === 3) {
      isValid = validateStep3();
    } else if (currentStep === 4) {
      isValid =validateStep4();
    } else if (currentStep === 5) {
      isValid = validateStep5();
    }
    if (isValid && currentStep < 5) {
      setCurrentStep(prevStep => prevStep + 1);
      setErrors({}); // Reset errors when moving to the next step
    }
  };

  const handleBack = () => {
    if (currentStep > 1) {
      setCurrentStep(currentStep - 1);
      setErrors({}); // Reset errors when moving back to the previous step
    }
  };

  const handleFormDataChange = (newData) => {
    setFormData((prevData) => ({
      ...prevData,
      ...newData
    }));
  };

  const handleViewStore = () => {
    if(validateStep5()){
      console.log("is valide step 5 : ", validateStep5());
    navigate("/store");}
  };

  return (
    <div className="flex flex-col h-screen">
      <div className="relative flex flex-col items-center w-full">
        <div
          className="absolute inset-0"
          style={{
            background:
              "linear-gradient(90deg, #6B1F91 0%, #8C28CC 50%, #A83EFF 100%)",
            opacity: 1
          }}
        />
        <div className="flex relative flex-row items-center justify-evenly w-full mt-4 mb-4">
          <div className="absolute w-[69vw] h-[3px] top-[11px] flex">
            {steps.map((step, index) => (
              <div
                key={index}
                className={`flex-1 border-b-[4px] mx-0.5 border-dotted transition-colors duration-300 ${
                  index < currentStep - 1 ? 'border-yellow-500' : index === currentStep - 1 ?'border-[#dbcdf5f6]' : 'border-[#cfc2e83d]'
                }`}
              ></div>
            ))}
          </div>
          {steps.map((step, index) => (
            <div key={index} className="relative flex flex-col items-center">
              <div
                className={`z-10 w-6 h-6 rounded-full transition-all duration-300 ${
                  index < currentStep ? "bg-[#FFB900]" : "bg-[#CFC2E8]"
                } flex items-center justify-center`}
              >
                <span
                  className={`w-6 h-6 transition-all duration-300 ${
                    index === currentStep - 1 ? "bg-white rounded-full" : "rounded-none"
                  }`}
                ></span>
              </div>
              <span
                className={`mt-2 text-sm font-open-sans transition-colors duration-300 ${
                  index === currentStep - 1 ? "text-white font-bold font-open-sans" : index < currentStep ? "text-[#FFB900]" : "text-[#cfc2e83d]"
                }`}
              >
                {step}
              </span>
            </div>
          ))}
        </div>
      </div>
      <div className="flex-1 flex flex-col justify-center items-center px-10">
        <div className="w-full max-w-md">
          <form className="space-y-4">
            {currentStep === 1 && (
              <Step1
                formData={formData}
                handleFormDataChange={handleFormDataChange}
                errors={errors}
                setErrors={setErrors}
              />
            )}
            {currentStep === 2 && (
              <Step2
                formData={formData}
                handleFormDataChange={handleFormDataChange}
                errors={errors}
                setErrors={setErrors}
              />
            )}
            {currentStep === 3 && (
              <Step3
                formData={formData}
                handleFormDataChange={handleFormDataChange}
                errors={errors}
                setErrors={setErrors}
              />
            )}
            {currentStep === 4 && (
              <Step4
                formData={formData}
                handleFormDataChange={handleFormDataChange}
                errors={errors}
                setErrors={setErrors}
              />
            )}
            {currentStep === 5 && (
              <Step5
                formData={formData}
                handleFormDataChange={handleFormDataChange}
                errors={errors}
                setErrors={setErrors}
              />
            )}
            <div className="relative w-full pt-5 flex justify-between">
              {currentStep > 1 && currentStep < 6 && (
                <button
                  type="button"
                  className="bg-gradient-to-r from-[#7C2CCC] to-[#530993] w-[115px] h-[45px] text-white rounded-lg font-bold text-xl"
                  onClick={handleBack}
                >
                  Back
                </button>
              )}
              {currentStep < 5 && (
                <button
                  type="button"
                  className="bg-gradient-to-r from-[#7C2CCC] to-[#530993] w-[115px] h-[45px] text-white rounded-lg font-bold text-xl"
                  onClick={handleNext}
                >
                  Next
                </button>
              )}
              {currentStep === 5 && (
                <button
                  type="button"
                  className="bg-gradient-to-r from-[#7C2CCC] to-[#530993] w-[115px] h-[45px] text-white rounded-lg font-bold text-xl"
                  onClick={handleViewStore}
                >
                  View store
                </button>
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateStoreApp;
