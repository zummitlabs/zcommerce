import React, { useEffect, useState } from 'react';
import { Country, State, City } from 'country-state-city';
import CustomDropdown from './customDropdown';

const Step5 = ({ formData, handleFormDataChange, errors, setErrors }) => {
  const [countries, setCountries] = useState([]);
  const [states, setStates] = useState([]);
  const [cities, setCities] = useState([]);

  useEffect(() => {
    const allCountries = Country.getAllCountries();
    setCountries(allCountries);
  }, []);

  useEffect(() => {
    if (formData.country) {
      setStates(State.getStatesOfCountry(formData.country.isoCode));
    } else {
      setStates([]);
    }
  }, [formData.country]);

  useEffect(() => {
    if (formData.country && formData.state) {
      setCities(City.getCitiesOfState(formData.country.isoCode, formData.state.isoCode));
    } else {
      setCities([]);
    }
  }, [formData.state, formData.country]);

  const handleChange = (name, value) => {
    let newFormData = { ...formData, [name]: value };
  
    if (name === 'country') {
      setStates(State.getStatesOfCountry(value.isoCode));
      newFormData.state = null;
      newFormData.city = null;
    } else if (name === 'state') {
      setCities(City.getCitiesOfState(formData.country.isoCode, value.isoCode));
      newFormData.city = null;
    }
  
    handleFormDataChange(newFormData);
    validateStep5(newFormData);
  };


  const handleBlur = () => {
    validateStep5();
  };

  const validateStep5 = (data) => {
    const { addressLine1, state, city, postalZipcode, country } = data;
    const newErrors = {};
    if (!addressLine1) newErrors.addressLine1 = "Address Line 1 is required";
    if (!state) newErrors.state = "State/Region is required";
    if (!city) newErrors.city = "City is required";
    if (!postalZipcode) {
      newErrors.postalZipcode = "Postal/Zip Code is required";
    } else if (!/^[A-Za-z0-9\s-]{3,10}$/.test(postalZipcode)) { // Updated regex for international format
      newErrors.postalZipcode = "Invalid Postal/Zip Code format";
    }
    if (!country) newErrors.country = "Country is required";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  return (
    <div className="w-full max-w-md">
      <h2 className="text-xl font-semibold mb-4">From warehouse to housefront.</h2>
      <p className="mb-4">Please input the address of where your products will be picked up by our logistic services for delivery to your customers.</p>
      <div className="space-y-4">
        <div>
          <label className="block mb-1" htmlFor="addressLine1">Address Line 1:</label>
          <input
            type="text"
            id="addressLine1"
            name="addressLine1"
            value={formData.addressLine1 || ''}
            onChange={(e) => handleChange(e.target.name, e.target.value)}
            className="w-full px-3 py-2 border border-gray-300 rounded"
          />
          {errors.addressLine1 && <p className="text-red-500 text-sm">{errors.addressLine1}</p>}
        </div>
        <div>
          <label className="block mb-1" htmlFor="addressLine2">Address Line 2:</label>
          <input
            type="text"
            id="addressLine2"
            name="addressLine2"
            value={formData.addressLine2 || ''}
            onChange={(e) => handleChange(e.target.name, e.target.value)}
            className="w-full px-3 py-2 border border-gray-300 rounded"
          />
        </div>

        <div className='flex justify-between'>
          <div>
            <CustomDropdown
              label="Country"
              name="country"
              value={formData.country ? formData.country.name : ''}
              options={countries}
              onSelect={(name, value) => handleChange(name, value)}
              placeholder="Select a country"
            />
            {errors.country && <p className="text-red-500 text-sm">{errors.country}</p>}
          </div>
          <div>
            <CustomDropdown
              label="State/Region"
              name="state"
              value={formData.state ? formData.state.name : ''}
              options={states}
              onSelect={(name, value) => handleChange(name, value)}
              placeholder="Select a state"
            />
            {errors.state && <p className="text-red-500 text-sm">{errors.state}</p>}
          </div>
        </div>
        <div>
        <div className='flex justify-between'>
          <div>
            <CustomDropdown
              label="City"
              name="city"
              value={formData.city}
              options={cities}
              onSelect={(name, value) => handleChange(name, value)}
              placeholder="Select a city"
            />
            {errors.city && <p className="text-red-500 text-sm">{errors.city}</p>}
          </div>
          <div>
              <label className="block mb-1" htmlFor="postalZipcode">Postal Code:</label>
              <input
                type="text"
                id="postalZipcode"
                name="postalZipcode"
                value={formData.postalZipcode}
                onChange={(e) => handleChange(e.target.name, e.target.value)}
                onBlur={handleBlur}
                className="w-full px-3 py-2 border border-gray-300 rounded"
              />
              {errors.postalZipcode && <p className="text-red-500 text-sm">{errors.postalZipcode}</p>}
        </div>
        </div>
           
        </div>
      </div>
    </div>
  );
};

export default Step5;
