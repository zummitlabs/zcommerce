import React, { useState, useEffect, useRef } from 'react';

const CustomDropdown = ({ label, name, value, options, onSelect, placeholder }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [filteredOptions, setFilteredOptions] = useState(options);
  const [inputValue, setInputValue] = useState(value || '');
  const dropdownRef = useRef();

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setIsOpen(false);
      }
    };
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  useEffect(() => {
    setFilteredOptions(
      options.filter(option => option.name.toLowerCase().includes(inputValue.toLowerCase()))
    );
  }, [inputValue, options]);

  const handleSelect = (option) => {
    setInputValue(option.name);
    setIsOpen(false);
    onSelect(name, option);
  };

  return (
    <div className="relative" ref={dropdownRef}>
      <label className="block mb-1">{label}</label>
      <input
        type="text"
        name={name}
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
        onClick={() => setIsOpen(true)}
        placeholder={placeholder}
        className="w-full px-3 py-2 border border-gray-300 rounded"
      />
      {isOpen && (
        <ul className="absolute z-10 w-full bg-white border border-gray-300 rounded mt-1 max-h-60 overflow-y-auto">
          {filteredOptions.map(option => (
            <li
              key={option.isoCode || option.name}
              onClick={() => handleSelect(option)}
              className="px-3 py-2 cursor-pointer hover:bg-gray-200"
            >
              {option.name}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default CustomDropdown;
