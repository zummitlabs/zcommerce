/* eslint no-unused-vars: "off" */
import React from 'react';
import PropTypes from 'prop-types';

const Step4 = ({ formData, handleFormDataChange, errors, setErrors }) => {

const validateAccountName = (value) => {
    return value.length >= 0;
};

const validateCardNumber = (value) => {
    const regex = /^[0-9]{16}$/; // Adjust regex for your needs (e.g., 16 digits)
    return regex.test(value) && luhnCheck(value);
};

const luhnCheck = (value) => {
    let sum = 0;
    let alternate = false;
    for (let i = value.length - 1; i >= 0; i--) {
        let n = parseInt(value.charAt(i), 10);
        if (alternate) {
            n *= 2;
            if (n > 9) n -= 9;
        }
        sum += n;
        alternate = !alternate;
    }
    return sum % 10 === 0;
};


const validateExpires = (value) => {
    const [month, year] = value.split('/');
    const currentYear = new Date().getFullYear() % 100; // Get last two digits of current year
    const currentMonth = new Date().getMonth() + 1; // Get current month (1-12)

    if (!/^\d{2}\/\d{2}$/.test(value)) {
      return false;
    }

    const numMonth = parseInt(month, 10);
    const numYear = parseInt(year, 10);

    if (numMonth < 1 || numMonth > 12) {
      return false;
    }

    if (numYear < currentYear || (numYear === currentYear && numMonth < currentMonth)) {
      return false;
    }

    return true;
  };

const validateCVV = (value) => {
    return /^[0-9]{3,4}$/.test(value);
};

const handleChange = (e) => {
    const { name, value } = e.target;
    console.log({name,value});
    handleFormDataChange({ [name]: value });


    if(name==='accountName'){
        if(!validateAccountName(value)){
            setErrors(prev=>({
                ...prev,
                accountName:'Account Name should be atleast 10 characters'
            }))
        }else{
            setErrors(prev=>{
                const newErrors={...prev};
                delete newErrors.accountName;
                return newErrors;
            })
        }
    }
    if(name==='cardNumber'){
        if(!validateCardNumber(value)){
            setErrors(prev=>({
                ...prev,
                cardNumber:'Invalid card number . Please check and try again.'
            }))
        }else{
            setErrors(prev=>{
                const newErrors={...prev};
                delete newErrors.cardNumber;
                return newErrors;
            })
        }
    }
    if(name==='cvv'){
        if(!validateCVV(value)){
            setErrors(prev=>({
                ...prev,
                cvv:'Account Name should be atleast 2-3 characters'
            }))
        }else{
            setErrors(prev=>{
                const newErrors={...prev};
                delete newErrors.cvv;
                return newErrors;
            })
        }
    }
    if(name==='expires'){
        if(!validateExpires(value)){
            setErrors(prev=>({
                ...prev,
                expires:'MM/YY format'
            }))
        }else{
            setErrors(prev=>{
                const newErrors={...prev};
                delete newErrors.expires;
                return newErrors;
            })
        }
    }
    
    
};

return (
    <div className="flex flex-col items-center justify-center bg-gray-100">
    <div className="w-full max-w-md p-8 space-y-6 bg-white rounded-lg">
        <h2 className="text-2xl font-bold text-center">Secure your payment.</h2>
        <p className="text-center">
        Transfer your sales directly to your preferred debit card. All
        transactions are secured by our encrypted channel.
        </p>
        <form className="mt-8 space-y-6">
        <div className="space-y-4">
            <div>
                <label htmlFor="accountName" className="block text-sm font-medium text-gray-700">
                Account name:
                </label>
                <input
                required
                type="text"
                id="accountName"
                name="accountName"
                value={formData.accountName}
                onChange={handleChange}
                className="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md  focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
                {errors.accountName && <p className="text-red-500 text-xs">{errors.accountName}</p>}
            </div>
            <div>
                <label htmlFor="cardNumber" className="block text-sm font-medium text-gray-700">
                Card number:
                </label>
                <input
                required
                type="text"
                id="cardNumber"
                name="cardNumber"
                value={formData.cardNumber}
                onChange={handleChange}
                className="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
                {errors.cardNumber && <p className="text-red-500 text-xs">{errors.cardNumber}</p>}
            </div>
            <div className="grid grid-cols-2 gap-4">
                <div>
                <label htmlFor="expires" className="block text-sm font-medium text-gray-700">
                    Expires:
                </label>
                <input
                    required
                    type="text"
                    id="expires"
                    name="expires"
                    value ={formData.expires}
                    onChange={handleChange}
                    className="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
                {errors.expires && <p className="text-red-500 text-xs">{errors.expires}</p>}
                </div>
                <div>
                <label htmlFor="cvv" className="block text-sm font-medium text-gray-700">
                    CVV:
                </label>
                <input
                    required
                    type="text"
                    id="cvv"
                    name="cvv"
                    value={formData.cvv}
                    onChange={handleChange}
                    className="w-full px-3 py-2 mt-1 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
                {errors.cvv && <p className="text-red-500 text-xs">{errors.cvv}</p>}
                </div>
            </div>
            </div>
        </form>
        </div>
    </div>
    );
};

Step4.propTypes = {
    formData: PropTypes.shape({
    accountName: PropTypes.string,
    cardNumber: PropTypes.string,
    expires: PropTypes.string,
    cvv: PropTypes.string
    }).isRequired,
    handleFormDataChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    setErrors: PropTypes.func.isRequired
};

export default Step4;
