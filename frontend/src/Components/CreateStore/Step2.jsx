import React from "react";
import { FaUpload } from "react-icons/fa";

const Step2 = ({ formData, handleFormDataChange, errors, setErrors }) => {
  const handleFileChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      // Assuming the file is uploaded to a server, we'll mock this by setting the file's URL
      const fileURL = URL.createObjectURL(file);
      handleFormDataChange({ imgLink: fileURL });
      if (errors.imgLink) {
        setErrors((prevErrors) => ({ ...prevErrors, imgLink: null }));
      }
    }
  };

  return (
    <div className="w-full max-w-lg mx-auto">
      <h2 className="text-2xl font-semibold text-center mb-4">Upload your shop logo.</h2>
      <p className="text-center text-gray-600 mb-6">
        Your shop logo represents the symbol that customers use to recognize your brand.
        Make sure that your logo is distinct to create a lasting impression.
      </p>
      <div className="w-full p-3 border rounded-lg mb-2 relative">
        <input
          type="file"
          onChange={handleFileChange}
          className="absolute inset-0 opacity-0 w-full h-full cursor-pointer"
        />
        <div className="flex items-center justify-center">
          <FaUpload className="mr-2" />
          <span className="text-gray-600">UPLOAD IMAGE</span>
        </div>
      </div>
      {errors.imgLink && <p className="text-red-500 text-sm">{errors.imgLink}</p>}
      <p className="text-gray-600 mt-4">
        <ul>
          <li>The max. file is 5mb.</li>
          <li>File size is 2400px by 2400px.</li>
        </ul>
      </p>
    </div>
  );
};

export default Step2;
