import React from 'react';

const Step1 = ({ formData, handleFormDataChange, errors, setErrors }) => {
  const validateShopName = (shopName) => {
    let newErrors = {};
    if (!shopName) {
      newErrors.shopName = "Shop Name is required";
    } else if (shopName.length < 5 || shopName.length > 20) {
      newErrors.shopName = "Shop Name must be between 5 and 20 characters";
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleChange = (e) => {
    const shopName = e.target.value;
    handleFormDataChange({ shopName });
    validateShopName(shopName);
  };

  const handleBlur = () => {
    validateShopName(formData.shopName);
  };

  return (
    <div className="w-full max-w-lg mx-auto">
      <h2 className="text-2xl font-semibold text-center mb-4">Name your shop.</h2>
      <p className="text-center text-gray-600 mb-6">
        This will be the name your customers will see. <br />
        Unsure with your name? Don't worry, you can change it anytime.
      </p>
      <input
        type="text"
        value={formData.shopName}
        onChange={handleChange}
        onBlur={handleBlur}
        className="w-full p-3 border rounded-lg mb-2"
        placeholder="Enter shop name"
      />
      {errors.shopName && <p className="text-red-500 text-sm">{errors.shopName}</p>}
      <ul className="text-gray-600 mt-2 list-disc list-inside">
        <li>Between 5-20 characters.</li>
        <li>You can use special characters and spaces.</li>
      </ul>
    </div>
  );
};

export default Step1;