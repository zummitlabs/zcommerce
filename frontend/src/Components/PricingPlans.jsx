import { useState } from 'react';
import PlanCard from './PlanCard';
import { default as bgPricing } from '../assets/images/bg-pricing.svg';

const PricingPlans = () => {
  const [hoveredIndex, setHoveredIndex] = useState(null);

  const plans = [
    {
      price: '$15/mo',
      type:'Basic',
      features: [
        {
          title: 'Sell Your Products',
          points: ['Unlimited listings', '5% transaction fee', 'Customer support'],
        },
        {
          title: 'Manage Your Business',
          points: ['Inventory management', 'Order tracking', 'Sales reports'],
        },
        {
          title: 'Customize Your Store',
          points: ['Basic themes', 'Custom domain', 'SEO tools'],
        },
      ],
    },
    {
      price: '$0/mo',
      type:'Professional',
      features: [
        {
          title: 'Sell Your Products',
          points: ['Limited listings', '10% transaction fee', 'Email support'],
        },
        {
          title: 'Manage Your Business',
          points: ['Basic inventory management', 'Order notifications', 'Basic sales reports'],
        },
        {
          title: 'Customize Your Store',
          points: ['Pre-built themes', 'Subdomain', 'Basic SEO tools'],
        },
      ],
    },
    {
      price: '$30/mo',
      type:'Enterprise',
      features: [
        {
          title: 'Sell Your Products',
          points: ['Unlimited listings', '2% transaction fee', 'Priority support'],
        },
        {
          title: 'Manage Your Business',
          points: ['Adv. inventory management', 'Real-time order tracking', 'Detailed sales reports'],
        },
        {
          title: 'Customize Your Store',
          points: ['Premium themes', 'Custom domain with SSL', 'Advanced SEO tools'],
        },
      ],
    },
  ];

  return (
    <div
      className="flex flex-col items-center w-full min-h-screen bg-cover bg-center"
      style={{ backgroundImage: `url("${bgPricing}")` }}
    >
      <h1 className="text-3xl font-semibold font-poppins mb-20">Choose a plan</h1>
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 px-1 w-full max-w-screen-lg">
        {plans.map((plan, index) => (
          <div className="flex justify-center w-full" key={index}>
            <div className="w-full max-w-xs">
              <PlanCard
                price={plan.price}
                type={plan.type}
                features={plan.features}
                onHover={() => setHoveredIndex(index === hoveredIndex ? null : index)}
                isHovered={hoveredIndex === index}
              />
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default PricingPlans;