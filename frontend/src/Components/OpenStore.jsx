import React from 'react';
import HeroImage from '../assets/images/HeroImage.png';
import SmallBar from './SmallBar';
import { Link } from 'react-router-dom';


const OpenStore = () => {
  return (
    <>
      <div className="bg-custom-gradient h-full text-white flex items-center justify-center p-4 py-0 flex-row">

        {/* Left Section */}
        <div className="flex flex-col items-start justify-center w-1/2 px-24">
          <div className="w-541 h-116 pt-6">
            <h1 className="font-poppins font-semibold text-5xl leading-14 text-left">
            LAUNCH YOUR ONLINE STORE IN ONLY 3 STEPS
            </h1>
          </div>

          <div className="w-454 h-44">
            <p className="font-open-sans font-normal leading-6 mt-6 text-2xl  text-left">
            From setup to sales, we provide everything you need to start and grow your online store.
            </p>
          </div>

          <Link to="/sellerregistration" className="self-center font-bold text-2xl bg-white text-purple-800 px-4 py-2 rounded border-2 border-transparent hover:bg-purple-800 hover:text-white">
            Open your Store Now
          </Link>
        </div>

        {/* Right Section */}
        <div className="flex justify-center items-center w-1/2">
          <img src={HeroImage} alt="Hero" className="" />
        </div>

      </div>
      <SmallBar />
    </>



  );
}

export default OpenStore;
