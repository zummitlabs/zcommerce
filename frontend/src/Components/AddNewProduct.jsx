import React, { useState } from "react";
import sampleProduct from "../assets/images/sampleproduct.png";
import { CiImageOn } from "react-icons/ci";

import { FaStar } from "react-icons/fa";
const AddNewProduct = () => {
  const [selected, setSelected] = useState(false);



  const handleRemove = () => {
    // Ensure handleImage is correctly passed and used
    if (typeof handleImage === 'function') {
      handleImage(index);
    } else {
      console.error('handleImage is not a function or is not provided.');
    }
  };
  const [images, setImages] = useState([
    {
      name: "Nike Air Shoes 1.png",
      img: sampleProduct,
      size: "828 KB",
      progress: 100,
    },
    {
      name: "Nike Air Shoes 2.png",
      img: sampleProduct,
      size: "625 KB",
      progress: 100,
    },
    {
      name: "Nike Air Shoes 3.png",
      img: sampleProduct,
      size: "724 KB",
      progress: 100,
    },
    {
      name: "Nike Air Shoes 4.png",
      img: sampleProduct,
      size: "224 KB",
      progress: 60,
    },
    {
      name: "Nike Air Shoes 3.png",
      img: sampleProduct,
      size: "724 KB",
      progress: 50,
    },
  ]);

  const handleRemoveImage = (index) => {
    setImages(images.filter((_, i) => i !== index));
  };

  return (
    <div className="flex flex-col w-full p-8 bg-[#F9F9F9]">
      <p className="text-sm text-gray-500 pl-[14px] pb-2">Dashboard / Orders</p>
      <div className="flex justify-between items-center mb-7">
        <h2 className="text-3xl font-semibold">Add New Product</h2>
        <div>
          <button className="border-purple-500 border-2 text-purple-500 px-6 py-2 rounded-[3px] mr-4">
            Save Draft
          </button>
          <button className="bg-custom-gradient  text-white px-6 py-3 rounded-[3px]">
            <span >Publish Product</span>
          </button>
        </div>
      </div>

      <form className="flex space-x-8 h-full">
        {/* Left Column */}
        <div className="flex flex-col space-y-4 flex-1 border-2 px-[16px] py-[18px]">
          {/* Content of the Left Column */}
          <div>
            <label
              htmlFor="product-name"
              className="block text-sm font-medium text-gray-700 mb-2"
            >
              Product Name
            </label>
            <input
              id="product-name"
              type="text"
              placeholder="Nike Air Max Sc Shoes"
              className="p-4 border rounded-md w-full"
            />
          </div>

          <div className="grid grid-cols-2 gap-4">
            <div>
              <label
                htmlFor="category"
                className="block text-sm font-medium text-gray-700 mb-2"
              >
                Category
              </label>
              <input
                id="category"
                type="text"
                placeholder="Ex: Footwear"
                className="p-4 border rounded-md w-full"
              />
            </div>
            <div>
              <label
                htmlFor="sub-category"
                className="block text-sm font-medium text-gray-700 mb-2"
              >
                Sub Category
              </label>
              <input
                id="sub-category"
                type="text"
                placeholder="Ex: Sneakers"
                className="p-4 border rounded-md w-full"
              />
            </div>
          </div>

          <div className="grid grid-cols-2 gap-4">
            <div>
              <label
                htmlFor="color"
                className="block text-sm font-medium text-gray-700 mb-2"
              >
                Select Color
              </label>
              <select id="color" className="p-4 border rounded-md w-full">
                <option>Select Color</option>
                <option>Black</option>
              </select>
            </div>
            <div>
              <label
                htmlFor="size"
                className="block text-sm font-medium text-gray-700 mb-2"
              >
                Select Size
              </label>
              <select id="size" className="p-4 border rounded-md w-full">
                <option>Select Size</option>
                <option>38 EU</option>
              </select>
              <span className="text-gray-500 text-sm">Size Chart</span>
            </div>
          </div>

          <div className="grid grid-cols-2 gap-4">
            <div>
              <label
                htmlFor="product-price"
                className="block text-sm font-medium text-gray-700 mb-2"
              >
                Product Price
              </label>
              <input
                id="product-price"
                type="text"
                placeholder="Rs. 7,000"
                className="p-4 border rounded-md w-full"
              />
            </div>
            <div>
              <label
                htmlFor="discount"
                className="block text-sm font-medium text-gray-700 mb-2"
              >
                Discount (Optional)
              </label>
              <input
                id="discount"
                type="text"
                placeholder="25% - Rs. 5,250"
                className="p-4 border rounded-md w-full"
              />
            </div>
          </div>

          <div>
            <label
              htmlFor="stock-quantity"
              className="block text-sm font-medium text-gray-700 mb-2"
            >
              Stock Quantity
            </label>
            <input
              id="stock-quantity"
              type="text"
              placeholder="48"
              className="p-4 border rounded-md w-full"
            />
          </div>
          <label
            htmlFor="product-tag"
            className="block text-sm font-medium text-gray-700 mb-2"
          >
            Product Tag
          </label>
          <div className="flex space-x-4">
            <button className="px-6 py-2 rounded-full border">Shoes</button>
            <button className="px-6 py-2 rounded-full border">Sneakers</button>
            <button className="px-6 py-2 rounded-full bg-purple-500 text-white">
              Nike
            </button>
            <button className="px-6 py-2 rounded-full border">Footwear</button>
          </div>

          <div>
            <label
              htmlFor="product-description"
              className="block text-sm font-medium text-gray-700 mb-2"
            >
              Product Description
            </label>
            <textarea
              id="product-description"
              placeholder="Input your Product Description here..."
              className="p-4 border rounded-md h-[178px] w-full"
            />
          </div>
        </div>

        {/* Right Column (Image Upload Section) */}
        <div className="flex-1 h-full border-2 px-[26px] pt-[45px] pb-[36px] flex flex-col">
          <div className="border-2 border-dashed h-[273px] border-purple-300 p-6 text-center rounded-md mb-6">
            <div className="flex flex-col items-center justify-center">
              <div className="p-4 mb-4">
                <img src="src\assets\images\Upload.png" alt="Upload" />
              </div>
              <p className="text-lg text-gray-500">
                <span className="text-black font-medium">
                  Drop your Files here.
                </span>{" "}
                Or select <span className="text-purple-500">Browse</span>
              </p>
            </div>
          </div>
          <div className="flex-1 overflow-y-auto scrollbar-hide">
            {" "}
            {/* Flex-grow applied here */}
            <ul className="space-y-4">
              {images.map((image, index) => (
                <li
                  key={index}
                  className="flex items-center justify-between border border-gray-400 p-4 rounded-md"
                >
                  <div className="flex items-center space-x-4">
                    <img
                      src={image.img}
                      alt={image.name}
                      className="w-12 h-12 object-cover"
                    />
                    <div className="flex-1">
                      <p className="text-gray-800">{image.name}</p>
                      <div className="relative">
                        <div className="w-[450px] bg-gray-200 rounded-full h-2">
                          <div
                            className="bg-purple-500 h-2 rounded-full"
                            style={{ width: `${image.progress}%` }}
                          />
                        </div>
                        <p className="absolute right-0 bottom-[-20px] text-gray-500 text-sm">
                          {image.size}
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="flex items-center space-x-4 mt-2">
      <div className="relative inline-block">
        <button
          className={`text-4xl relative ${selected ? 'border-2 border-black' : ''}`}
          
        >
          <CiImageOn
            className="text-black"
          />
          
            <FaStar className="absolute text-yellow-400 bottom-[2px] right-[1.8px] text-lg" />
           
        </button>
      </div>
      <button
        onClick={() => handleRemove(index)} 
        className="text-red-500 mb-2 text-2xl"
      >
        <i className="fas fa-trash"></i>
      </button>
    </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddNewProduct;
