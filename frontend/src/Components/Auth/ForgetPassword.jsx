import React from 'react'
import AuthHeader from './AuthHeader'
import { Link , useNavigate } from 'react-router-dom'
import { usePasswordResetCode } from '../../Hooks/Auth'

const ForgetPassword = () => {
  const navigate = useNavigate()
  const {passwordResetCode} = usePasswordResetCode()
  const [email , setEmail] = React.useState('')

  const handleOnSubmit = async(e) => {
    e.preventDefault()
    const response = await passwordResetCode(email)
    if(response.success){
      localStorage.setItem('email',email)
      navigate('/verify')
      console.log(response)
    }

  }
  return (
    <div className="min-h-screen bg-white">
      {/* Header */}
      <AuthHeader />

      {/* Form Container */}
      <main className="max-w-xl mx-auto mt-10 p-6 space-y-8 min-h-screen">
        <div className="space-y-8">
          <h2 className="text-2xl font-bold tracking-tight">Password Assistance ?</h2>
          <p className="text-gray-600 text-sm  ">
            Enter email address associated with your ZCommerce Account. 
          </p>
        </div>

        <form className="space-y-6" onSubmit={handleOnSubmit}>
          <div className="space-y-2">
            <label htmlFor="email" className="block text-sm font-medium text-gray-700">
              E-mail address
            </label>
            <input
              id="email"
              type="email"
              onChange={(e) => setEmail(e.target.value)}
              required
              className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-1 focus:ring-[#6b21a8] focus:border-[#6b21a8]"
            />
          </div>

          <button
            type="submit"
            className=" bg-black text-white py-2 px-16 rounded-md hover:bg-black/90 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-black"
          >
            continue
          </button>
        </form>
      </main>
    </div>
  )
}

export default ForgetPassword