import React, { useState } from 'react'
import AuthHeader from './AuthHeader'
import { Link, useNavigate } from 'react-router-dom'
import { useAuthVerify } from '../../Hooks/Auth'

const OtpVerify = () => {
  const email = localStorage.getItem('email')
    const navigate = useNavigate()
    const {verify} = useAuthVerify()
    const [token, setToken] = useState('')
    const handleOnSubmit = async(e) => {
        e.preventDefault()
        const response = await verify(token)
        console.log(response?.data?.tokenType , "this is it")
        if(response.success){
          console.log(response?.data?.tokenType , "this is it fkjsdd")

            if(response?.data?.tokenType === "EMAIL_VERIFICATION"){
                navigate('/login')
            }
            else if(response?.data?.tokenType === "PASSWORD_RESET"){
                navigate('/reset-password')
            }
        }
    }
  return (
    <div className="min-h-screen bg-white">
      {/* Header */}
      <AuthHeader />

      {/* Form Container */}
      <main className="max-w-xl mx-auto mt-10 p-6 space-y-8 min-h-screen">
        <div className="space-y-8">
          <h2 className="text-2xl font-bold tracking-tight">Password Assistance ?</h2>
          <p className="text-gray-600 text-sm  ">
            For your security, we have sent the code to your  
            {email}
          </p>
        </div>

        <form className="space-y-6" onSubmit={handleOnSubmit}>
          <div className="space-y-2">
            
            <input
              id="email"
              type="text"
              onChange={(e) => setToken(e.target.value)}
              required
              className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-1 focus:ring-[#6b21a8] focus:border-[#6b21a8]"
            />
          </div>
          <div>
          <p className="text-gray-600">
                     
                      <Link  className="text-[#6b21a8] hover:underline">
                        resend otp
                      </Link>
                    </p>
          </div>

          <button
            type="submit"
            className=" bg-black text-white py-2 px-16 rounded-md hover:bg-black/90 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-black"
          >
            continue
          </button>
        </form>
      </main>
    </div>
  )
}

export default OtpVerify