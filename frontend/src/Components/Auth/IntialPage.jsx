import React from 'react'
import { Link } from 'react-router-dom'

const IntialPage = () => {
  return (
    <div className='flex flex-col min-h-screen items-center justify-center gap-4'>   
    <Link to='/register'> 
        <button className='px-16 py-2 w-72 focus:ring-1 underline rounded-md text-purple-500 border border-black'>Signup</button>
    </Link>
    <Link to='/login'>
        <button className='px-16 py-2 w-72 focus:ring-1 underline rounded-md text-purple-500 border border-black'>Login</button>
    </Link>
    <Link to='/'>
        <button className='px-16 py-2 w-72 focus:ring-1 underline rounded-md text-purple-500 border border-black'>Signup as a guest</button>
    </Link>
    </div>


  )
}

export default IntialPage