import React from 'react'
import logo from '../../assets/images/logo.png';
const AuthHeader = () => {
  return (
    <>
        <header className="bg-custom-gradient p-4 pl-10">
            <div className="max-w-md flex items-center space-x-1">
                <img src={logo} alt="Commerce Logo" className="h-10" />
            <h1 className="text-2xl font-bold text-black">Commerce</h1>
            </div>
        </header>
    </>
  )
}

export default AuthHeader