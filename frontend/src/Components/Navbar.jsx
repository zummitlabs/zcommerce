import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import logo from "../assets/images/logo.png";

export default function Navbar({ scrollToSection, refs }) {
  const [isClicked, setIsClicked] = useState(true);
  const navigate = useNavigate();
  const handleClick = () => {
    setIsClicked(true);
    setTimeout(() => {
      setIsClicked(false);
      navigate("/register");
    }, 100);
  };

  return (
    <nav className="w-full bg-white">
      <div className="px-4 sm:px-6 lg:px-20 border-b border-slate-900">
        <div className="w-full flex justify-between items-center p-1">
          <div className="flex items-center space-x-4">
            <select className="border-none">
              <option value="english">English</option>
              <option value="hindi">Hindi</option>
            </select>
          </div>
          <div className="hidden md:flex space-x-4 lg:space-x-10">
            <Link
              to="/download"
              className="text-black hover:font-semibold font-normal font-open-sans"
            >
              Download App
            </Link>
            <Link
              to="/support"
              className="text-black hover:font-semibold font-open-sans font-normal"
            >
              Support Center
            </Link>
            <Link
              to="/marketplace"
              className="text-black hover:font-semibold font-open-sans font-normal"
            >
              Marketplace
            </Link>
          </div>
        </div>
      </div>
      <div className="w-full h-15 flex flex-col md:flex-row justify-between items-center px-4 sm:px-6 lg:px-20 py-1 border-b">
        <div className="flex items-center space-x-1">
          <img src={logo} alt="Commerce Logo" className="h-12" />
          <span className="text-purple-900 text-3xl font-bold font-poppins">
            Commerce
          </span>
        </div>
        <div className="flex flex-col md:flex-row space-y-2 md:space-y-0 md:space-x-8 mt-2 md:mt-0">
          <div
            onClick={() => scrollToSection(refs.whyChooseUsRef)}
            className="cursor-pointer text-black font-semibold font-open-sans text-md hover:text-purple-900"
          >
            Features
          </div>
          <div className="cursor-pointer text-black text-md font-semibold font-open-sans hover:text-purple-900">
            Templates
          </div>
          <div
            onClick={() => scrollToSection(refs.pricingPlanRef)}
            className="cursor-pointer text-black text-md font-semibold font-open-sans hover:text-purple-900"
          >
            Pricing
          </div>
        </div>
        <button
          onClick={handleClick}
          className={`mt-2 md:mt-0 py-2 px-6 rounded-lg text-[21px] font-[700] font-open-sans shadow-custom-inset ${
            isClicked
              ? "bg-purple-700 text-white scale-95 duration-75 ease-out"
              : "bg-purple-700 text-white hover:from-purple-700 hover:to-purple-900"
          }`}
        >
          Login/Sign Up
        </button>
      </div>
    </nav>
  );
}
