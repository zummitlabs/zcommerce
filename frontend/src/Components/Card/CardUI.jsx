const CardUI = ({ img, title, desc }) => {
  return (
    <div className="bg-white rounded-tr-[10vw] rounded-bl-[6vw] shadow-lg overflow-hidden flex flex-col">
      <img
        src={img}
        alt={title}
        className="w-full h-48 sm:h-56 md:h-64 lg:h-[276px] object-cover"
      />
      <div className="p-4 md:p-6 flex-grow flex flex-col justify-between">
        <h3 className="text-xl sm:text-2xl md:text-[29px] font-semibold text-black mb-2">
          {title}
        </h3>
        <p className="text-black mb-4">{desc}</p>
      </div>
    </div>
  );
};

export default CardUI;