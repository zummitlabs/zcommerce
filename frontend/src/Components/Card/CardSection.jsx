import Card from "./CardUI";

function CardSection() {
  const cardList = [
    {
      img: "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRRsEwBE1XWL0SXC_KOK6_Q8-l0A6GvNnOfv-KZM9oFsZF_clbO",
      title: "Easy Setup",
      description: "As easy 1-2-3 in less than 10 minutes.",
    },
    {
      img: "https://www.therapidmind.com/_next/image/?url=%2Fassets%2Fimages%2Fstill-life-graphic-design-office.jpg&w=1920&q=75",
      title: "Customizable Themes",
      description: "Browse through our theme gallery",
    },
    {
      img: "https://techriffs.com/images/seo.jpg",
      title: "SEO Tools",
      description:
        "Get insight in organic traffic and engagement of our website",
    },
  ];

  return (
    <section className="w-full px-4 sm:px-8 md:px-16 lg:px-24 xl:px-[116px] py-12 md:py-16 lg:py-20">
      <h2 className="text-2xl sm:text-3xl md:text-4xl lg:text-[43px] mb-8 md:mb-12 lg:mb-[80px] text-center text-black font-bold">
        Everything You Need For Ecommerce Success.
      </h2>

      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
        {cardList.map((card, id) => (
          <Card
            key={id}
            img={card.img}
            title={card.title}
            desc={card.description}
          />
        ))}
      </div>
    </section>
  );
}

export default CardSection;
