import React from 'react';

const Reviews_achievemnets = () => {
  // Inner data for reviews and announcements
  const reviews = [
    {
      id: 1,
      name: "Theo James",
      title: "Data Analyst",
      imgUrl: "src/assets/images/sampleprofile.png",
      rating: 5,
      reviewText: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 2,
      name: "Maria Roberts",
      title: "Data Analyst",
      imgUrl: "src/assets/images/sampleprofile.png",
      rating: 5,
      reviewText: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 3,
      name: "Talissa T.",
      title: "Data Analyst",
      imgUrl: "src/assets/images/sampleprofile.png",
      rating: 5,
      reviewText: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 2,
      name: "Maria Roberts",
      title: "Data Analyst",
      imgUrl: "src/assets/images/sampleprofile.png",
      rating: 5,
      reviewText: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 3,
      name: "Talissa T.",
      title: "Data Analyst",
      imgUrl: "src/assets/images/sampleprofile.png",
      rating: 5,
      reviewText: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    }
  ];

  const announcements = [
    {
      id: 1,
      date: "Today 3:00",
      title: "Welcome to the Era of ZCommerce!",
      description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 2,
      date: "June 03 2023",
      title: "Welcome to the Era of ZCommerce!",
      description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 3,
      date: "May 29 2023",
      title: "Welcome to the Era of ZCommerce!",
      description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 4,
      date: "May 25 2023",
      title: "Welcome to the Era of ZCommerce!",
      description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 3,
      date: "May 29 2023",
      title: "Welcome to the Era of ZCommerce!",
      description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
      id: 4,
      date: "May 25 2023",
      title: "Welcome to the Era of ZCommerce!",
      description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    }
  ];

  return (
    <div className="flex w-full h-[737px] bg-white  ">
    {/* Reviews Section */}
    <div className="w-[461px] border-gray-400 bg-gray-50 border-[1px] mx-5 p-2 rounded overflow-y-scroll">
      <div className="flex justify-between items-center mb-4 border-b-2 border-gray-400 ">
        <h2 className="text-xl font-semibold ">Reviews</h2>
        <button className="text-gray-400">...</button>
      </div>
      <div className="space-y-6">
        {/* Mapping over review data */}
        {reviews.map((review) => (
          <ReviewItem 
            key={review.id} 
            name={review.name} 
            title={review.title} 
            imgUrl={review.imgUrl} 
            rating={review.rating}
            reviewText={review.reviewText}
          />
        ))}
      </div>
    </div>

    {/* Announcements Section */}
    <div className="w-[544px] p-4 border-gray-400 rounded bg-gray-50 border-[1px] mx-5  overflow-y-scroll">
      <div className="flex justify-between items-center mb-4 border-b-2 border-gray-400">
        <h2 className="text-xl font-semibold">Announcements</h2>
        <button className="text-gray-400">...</button>
      </div>
      <div className="space-y-4">
        {/* Mapping over announcement data */}
        {announcements.map((announcement) => (
          <AnnouncementItem 
            key={announcement.id}
            date={announcement.date} 
            title={announcement.title} 
            description={announcement.description}
          />
        ))}
      </div>
    </div>
  </div>
);
};

const ReviewItem = ({ name, title, imgUrl, rating, reviewText }) => (
<div className="flex space-x-4  p-4">
  <img src={imgUrl} alt={name} className="w-12 h-12 rounded-full object-cover" />
  <div>
    <h3 className="font-semibold">{name}</h3>
    <p className="text-sm text-black">{title}</p>
    <div className="text-yellow-400">
      {'★'.repeat(rating)}{'☆'.repeat(5 - rating)}
    </div>
    <p className="text-black">{reviewText}</p>
  </div>
</div>
);

const AnnouncementItem = ({ date, title, description }) => (
<div className=" p-4">
  <div className='flex items-center  '>
  <div className='flex border-r-2 mx-5'>
  <p className="text-sm text-black">{date}</p>
  </div>
  <div >
  <h4 className="font-semibold">{title}</h4>
  <p className="text-black">{description}</p>
  </div>
  </div>
</div>
);

export default Reviews_achievemnets;
