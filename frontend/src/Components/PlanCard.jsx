
import PropTypes from 'prop-types';

const PlanCard = ({ price, features, onHover, isHovered ,type }) => {
  return (
    <div
      className={`border-2 border-purple-800 pt-0 px-0 p-6 max-w-[290px] rounded-md shadow-lg transition-transform duration-300 ease-in-out ${
        isHovered ? 'bg-gradient-to-b from-purple-900 to-purple-500 text-white scale-105' : 'bg-white'
      }`}
      onMouseEnter={onHover}
      onMouseLeave={onHover}
    >
      <div className='grid grid-cols-2'>
      <h2 className="text-2xl font-bold py-1 flex items-center justify-center pt-2">{price}</h2>
      <div className={` py-1 flex items-center justify-center font-bold rounded-tr-sm font-open-sans ${isHovered?'text-purple-800 rounded-tr-[5px]  bg-white':'bg-purple-800 text-white'}`}>{type}</div>
      </div>
      <div className='px-6 pt-4'>
      <ul className="text-left">
        {features.map((feature, index) => (
          <li key={index} className="mb-2">
            <h3 className={`font-semibold font-poppins ${isHovered?'text-white':'text-purple-800'}`}>{feature.title}</h3>
            <ul className="list-disc list-inside">
              {feature.points.map((point, idx) => (
                <li className={`font-reguler font-open-sans ${isHovered?'text-white':'text-purple-700'}`} key={idx}>{point}</li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
      <button className="mt-4 w-full bg-white text-purple-800 font-semibold border-purple-700 border-2 py-2 transition-all duration-300 ease-in-out rounded hover:bg-purple-800 hover:border-white hover:text-white active:bg-gradient-to-b from-purple-800 to-purple-950 active:scale-90">
        SELECT
      </button>
      </div>
    </div>
  );
};

PlanCard.propTypes = {
  price: PropTypes.string.isRequired,
  type:PropTypes.string.isRequired,
  features: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      points: PropTypes.arrayOf(PropTypes.string).isRequired,
    })
  ).isRequired,
  onHover: PropTypes.func.isRequired,
  isHovered: PropTypes.bool.isRequired,
};

export default PlanCard;