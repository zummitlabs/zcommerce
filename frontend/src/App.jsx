import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "./Pages/Home.jsx";
import MarketPlace from "./Pages/MarketPlace.jsx";
import UserRegistration from "./Pages/UserRegistration.jsx";
/* eslint no-unused-vars: "off" */
import Footer from "./Components/Footer.jsx";
import HowItWorks from "./Components/HowItWorks.jsx";
import PricingPlans from "./Components/PricingPlans.jsx";
import CardSection from "./Components/Card/CardSection.jsx";
import WhyChooseUs from "./Components/WhyChooseUs.jsx";
import OpenStore from "./Components/OpenStore.jsx";
import Testimonal from "./Components/testimonial/Testimonal.jsx";
import SellerRegistrationForm from "./Components/Seller/SellerRegistrationForm.jsx";
import CreateStoreApp from "./Components/CreateStore/CreateStoreApp.jsx";
import ProductDetails from "./Pages/ProductDetails.jsx";
import Cart from "./Pages/Cart/Cart.jsx";
// import LeftPanel from "./Pages/SellerDashboard/LeftPanel/LeftPanel.jsx";
import Dashboard from "./Pages/SellerDashboard/Dashboard.jsx";
import ViewOrdersDashboard from "./Pages/SellerDashboard/ViewcartDashboard.jsx";
import Register from "./Components/Auth/Register.jsx";
import LoginPage from "./Components/Auth/LoginPage.jsx";
import ForgetPassword from "./Components/Auth/ForgetPassword.jsx";
import OtpVerify from "./Components/Auth/OtpVerify.jsx";
import {ToastContainer} from "react-toastify"
import ResetPasswordPage from "./Components/Auth/ResetPasswordPage.jsx";
import IntialPage from "./Components/Auth/IntialPage.jsx";


const DownloadApp = () => <div>Download App</div>;
const SupportCenter = () => <div>Support Center</div>;
const Features = () => <div>Features</div>;
const Templates = () => <div>Templates</div>;
const Pricing = () => <div>Pricing</div>;

function App() {
  return (
    <div className="flex flex-col min-h-screen">
      <ToastContainer autoClose={2000}
      position="top-center" />
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/seller-reg" element={<SellerRegistrationForm />} />

          <Route path="/forgetpassword" element={<ForgetPassword />} />
          <Route path="/reset-password" element={<ResetPasswordPage />} />
          <Route path="/intial-page" element={<IntialPage />} />

          <Route path="/verify" element={<OtpVerify />} />
          <Route path="/download" element={<DownloadApp />} />
          <Route path="/support" element={<SupportCenter />} />
          <Route path="/marketplace" element={<MarketPlace />} />
          <Route path="/productdetails" element={<ProductDetails />} />
          <Route path="/features" element={<Features />} />
          <Route path="/templates" element={<Templates />} />
          <Route path="/pricing" element={<Pricing />} />
          <Route path="/sellerregistration" element={<SellerRegistrationForm />} />
          <Route path='/createStore' element={<CreateStoreApp/>}/>
          <Route path='/sellerdashboard' element={<Dashboard/>}/>
          <Route path="/cart" element={<Cart />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
