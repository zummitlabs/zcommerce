import React, { useState, useEffect, useRef } from 'react';
import FooterProductDetails from "./Marketplace/Components/FooterProductDetails";
import Navbar from "./Marketplace/Components/Navbar";
import SimilarProduct from "./Marketplace/Components/Similar/SimilarProduct";
import ProductDescription from "./Marketplace/ProductDescription/ProductDescription";
import ProductCard from "./Marketplace/ProductDisplay/ProductCard";
import ProductDisplay from "./Marketplace/ProductDisplay/ProductDisplay";
import ReviewDetail from "./Marketplace/Review/ReviewDetail";

function ProductDetails() {
    const [showProductCard, setShowProductCard] = useState(true);
    const similarProductRef = useRef(null);

    useEffect(() => {
        const observer = new IntersectionObserver(
            ([entry]) => {
                // When SimilarProduct is intersecting (visible), hide the ProductCard
                setShowProductCard(!entry.isIntersecting);
            },
            {
                root: null,
                rootMargin: '0px',
                threshold: 0, // Trigger when 10% of the SimilarProduct is visible
            }
        );

        if (similarProductRef.current) {
            observer.observe(similarProductRef.current);
        }

        return () => {
            if (similarProductRef.current) {
                observer.unobserve(similarProductRef.current);
            }
        };
    }, []);

    return (
        <>
            <Navbar/>
            <div className="flex relative">
                <div className="basis-9/12 ">
                    <ProductDisplay/>
                    <div className=" h-[2px] bg-gradient-to-r from-purple-500 to-yellow-500"></div>
                    <ReviewDetail/>
                    <ProductDescription/>
                </div>
                <div className='basis-3/12'>
                {showProductCard && (
                    <div className="w-[375px] fixed right-0 top-48 border-l-purple-900">
                        <ProductCard/>
                    </div>
                )}
                </div>
            </div>
            <div ref={similarProductRef}>
                <SimilarProduct/>
            </div>
            <FooterProductDetails/>
        </>
    );
}

export default ProductDetails;