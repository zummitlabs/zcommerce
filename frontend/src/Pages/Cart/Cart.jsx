import React from "react";
import Navbar from "../Marketplace/Components/Navbar";
import image1 from "./Image.jpg";
import CartItem from "./CartItem";
import icon from "./dummyIcons1.png";
import favorite from "./Favorite.png";
import trash from "./Trash Can.png";
import visa from "./visa-logo.png";
import master from "./card.png";
import discover from "./discover-card.png";
import shield from "./shield.png";
import love from "./love.png";

const Cart = () => {
  const items = [
    {
      icon: icon,
      image: image1,
      price: 230,
      text: "Lorem ipsum dolor sit amet consectetur. Porttitor lacus metus diam faucibus congue.",
      shipping: "1-3 August",
      discount: 50,
      quantity: 1,
    },
    {
      icon: icon,
      image: image1,
      price: 230,
      text: "Lorem ipsum dolor sit amet consectetur. Porttitor lacus metus diam faucibus congue.",
      shipping: "1-3 August",
      discount: 50,
      quantity: 1,
    },
    {
      icon: icon,
      image: image1,
      price: 230,
      text: "Lorem ipsum dolor sit amet consectetur. Porttitor lacus metus diam faucibus congue.",
      shipping: "1-3 August",
      discount: 50,
      quantity: 1,
    },
    {
      icon: icon,
      image: image1,
      price: 230,
      text: "Lorem ipsum dolor sit amet consectetur. Porttitor lacus metus diam faucibus congue.",
      shipping: "1-3 August",
      discount: 50,
      quantity: 1,
    },
    {
      icon: icon,
      image: image1,
      price: 230,
      text: "Lorem ipsum dolor sit amet consectetur. Porttitor lacus metus diam faucibus congue.",
      shipping: "1-3 August",
      discount: 50,
      quantity: 1,
    },
  ];

  return (
    <>
      <Navbar />
      <div className=" min-h-screen px-8 py-10 ">
        <h1 className="py-5 text-2xl ">Shopping Cart</h1>
        <div className=" grid grid-cols-1 lg:grid-cols-12 gap-4 ">
          <div className="h-full md:col-span-8 ">
            <div className="flex justify-between pt-5 pb-2 px-5">
              <div className="flex items-center">
                <input
                  type="checkbox"
                  className="w-6 h-6 rounded-full border-gray-300 text-blue-600 focus:ring-blue-500"
                />
                <label className="ml-2">Select All</label>
              </div>
              <div className="flex ">
                <div className=" flex items-center">
                  <img src={favorite} alt="fav" className="mr-2 w-8 h-8" />
                  <p>Move to wishlist</p>
                </div>
                <div className="ml-5 flex items-center">
                  <img src={trash} alt="trash" className=" mr-2 w-8 h-8" />
                  <p>Delete selected items</p>
                </div>
              </div>
            </div>

            <div>
              {items.map((item) => (
                <CartItem {...item} />
              ))}
            </div>
          </div>
          <div className="h-full md:col-span-4 ">
            <div className="p-8">
              <div className="border-2 border-solid border-purple-700 py-6 px-10 rounded-xl">
                <h1 className=" text-2xl">Order Summary</h1>
                <div className="flex justify-between mt-6 ">
                  <div>
                    <p className="py-1">Subtotal: </p>
                    <p className="py-1">Discount (-50%): </p>
                    <p className="py-1">Shipping: </p>
                  </div>
                  <div>
                    <p className="py-1">400$</p>
                    <p className="py-1">200$</p>
                    <p className="py-1">Free</p>
                  </div>
                </div>
                <div className="mt-10 border border-solid border-purple-700 "></div>
                <div className="flex justify-between mt-10 ">
                  <div>
                    <p>Total(2): </p>
                  </div>
                  <div>
                    <p>50$ </p>
                  </div>
                </div>
                <div className="w-full flex justify-center items-center mt-8">
                  <button className="bg-purple-700 py-3 rounded-lg text-white font-semibold  w-40">
                    Checkout
                  </button>
                </div>
              </div>
            </div>
            <div className="px-7">
              <div className="border-2 border-solid border-gray-400 rounded-lg p-6">
                <h3>Pay with:</h3>
                <div className="flex items-center mt-1">
                  <img src={visa} alt="visa-card" height="40" width="40" />
                  <img
                    src={master}
                    alt="master-card"
                    height="40"
                    width="40"
                    className="ml-1"
                  />
                  <img
                    src={discover}
                    alt="discover-card"
                    height="40"
                    width="40"
                    className="ml-1"
                  />
                </div>

                <div className="pt-5">
                  <h2 className="text-xl font-semibold">Buyer protection</h2>
                  <div className="mt-3 flex items-start">
                    <img
                      src={shield}
                      alt="shield"
                      height="20"
                      width="20"
                      className="mt-1"
                    />
                    <p className="ml-1 ">
                      Get full refund id the product is not as described or it
                      is not delivered.
                    </p>
                  </div>
                </div>
              </div>
              <div className="flex justify-center items-center mt-8">
                <button className="bg-white border-2 py-3 border-solid border-purple-500 w-52 text-purple-500 rounded-lg text-lg font-semibold flex  items-center justify-center">
                  <img src={love} alt="love-png" height="20" width="20" />
                  <p className="ml-1">View wishlist</p>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Cart;
