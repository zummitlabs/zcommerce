import React from "react";

const CartItem = ({
  icon,
  image,
  price,
  text,
  shipping,
  discount,
  quantity,
}) => {
  return (
    <div className="border border-solid border-gray-500 p-3 m-5 rounded-xl flex">
      <div>
        <img
          src={image}
          alt="product-img"
          className="h-36 w-36 rounded-2xl shadow-md"
        />
      </div>
      <div className="p-3 ">
        <div className="flex items-center ">
          <p>
            Lorem ipsum dolor sit amet consectetur. Porttitor lacus metus diam
            faucibus congue.
          </p>
          <img src={icon} alt="dummy-icons-img" className="h-8 w-16 ml-7" />
        </div>
        <p className="text-gray-500 text-sm">Shipping: {shipping}</p>
        <div className="flex justify-between items-center mt-7">
          <button className="border border-solid border-gray-400  px-7 rounded-sm ml-5">
            Color/Size
          </button>
          <div className="flex items-center">
            <div className="flex mr-10 ">
              <div className="h-7 w-7 bg-purple-700 text-white rounded-full flex justify-center items-center ">
                -
              </div>
              <p className="px-3">1</p>
              <div className="h-7 w-7 bg-purple-700 text-white rounded-full flex justify-center items-center ">
                +
              </div>
            </div>
            <div>
              <p className="text-xs text-orange-400">{discount}%</p>
              <p className="text-xl     ">{price}$</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartItem;
