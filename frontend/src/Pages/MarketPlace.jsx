import ExploreCategories from "./Marketplace/Components/ExploreCategories";
import Footer from "./Marketplace/Components/Footer";
import Navbar from "./Marketplace/Components/Navbar";
import TrendingCategories from "./Marketplace/Components/TrendingCategories";
import JustForSection from "./Marketplace/JustForYou/JustForSection";
import CopyRight from "./Marketplace/Components/CopyRight";
import SimilarProduct from "./Marketplace/SimilarProducts/SimilarProduct";

function MarketPlace() {
  return (
    <div className="flex flex-col min-h-screen" >
           <Navbar/>
        <div className="flex-grow">
          <ExploreCategories/>
          <JustForSection/>
          <TrendingCategories/>
          <SimilarProduct/>
        </div>
           <Footer/>
           <CopyRight/>
    </div>
  );
}

export default MarketPlace;
