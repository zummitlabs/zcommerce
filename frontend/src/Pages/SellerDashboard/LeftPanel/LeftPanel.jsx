import React, { useState } from 'react';

import logo from '../../../assets/images/zcommerce-logo.png'
import dashboard from '../../../assets/images/dashboard.png'
import dropdown from '../../../assets/images/Dropdown.png'
import dropdown_white from '../../../assets/images/Dropdown-white.png'
import orderslogo from '../../../assets/images/Orders.png'
import mystore from '../../../assets/images/MyStore.png'
import marketing from '../../../assets/images/Marketing.png'
import finance from '../../../assets/images/Finance.png'  
import support from '../../../assets/images/support.png'
import settings from '../../../assets/images/settings.png'


const LeftPanel = () => {
  const [showOrdersDropdown, setShowOrdersDropdown] = useState(false);

  const toggleOrdersDropdown = () => {
    setShowOrdersDropdown(!showOrdersDropdown);
  };

  return (
    <div style={{width:376}} className="left-panel bg-gradient-to-b to-purple-700 from-purple-900 min-h-screen text-white p-4 font-poppins">
      <div className="flex items-center justify-center mb-6">
        <img src={logo} alt="Logo" className="h-14" />
      </div>
      <ul className="list-none p-0 ml-auto w-11/12 flex flex-col items-end mt-8">
        <li className="mb-5 w-full">
          <div className="flex items-center ">
            <img src={dashboard} alt="Dashboard" className="mr-2" />
            <span>Dashboard</span>
          </div>
        </li>
        <li className="mb-5 relative w-full">
          <div onClick={toggleOrdersDropdown} className="cursor-pointer bg-white text-black rounded-l-full p-2 pr-10 flex items-center justify-end">
            <img src={orderslogo} alt="Orders Logo" className="mr-2" />
            <span className='font-medium'>Orders</span>
            <img src={dropdown} className="ml-auto" alt="Dropdown Icon" />
          </div>
          {showOrdersDropdown && (
            <div className="text-white mt-2 p-2 rounded w-full">
              <div className="relative ml-8">
                <div className="absolute left-[-1.3rem] top-0 bottom-0 w-0.5 bg-white"></div>
                <ul className="list-none m-0 p-0">
                  {['View Orders', 'Cancellations', 'Shipping Setting', 'Drafts'].map((item, index) => (
                    <li key={index} className="py-1.5 pl-4 relative flex items-center">
                      <div className="absolute left-[-1.5rem] w-2 h-2 bg-white rounded-full"></div>
                      {item}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          )}
        </li>
        {[
          { src: mystore, label: 'My Store' },
          { src: marketing, label: 'Marketing' },
          { src: finance, label: 'Finance' }
        ].map((item, index) => (
          <li key={index} className="mb-6 flex items-center cursor-pointer w-full justify-end">
            <img src={item.src} alt={item.label} className="mr-2" />
            <span>{item.label}</span>
            <img src={dropdown_white} className="ml-auto mr-8" alt="Dropdown Icon" />
          </li>
        ))}
        <li className="mb-20 w-full"></li> {/* Adding gap between Finance and Support */}
        {[
          { src: support, label: 'Support' },
          { src: settings, label: 'Settings' }
        ].map((item, index) => (
          <li key={index} className="mb-5 flex items-center cursor-pointer w-full justify-end">
            <img src={item.src} alt={item.label} className="mr-2" />
            <span>{item.label}</span>
            <img src={dropdown_white} className="ml-auto mr-8" alt="Dropdown Icon" />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default LeftPanel;
