import ProductTable from './ProductTable';

const ProductsTable = () => {
  const products = [
    {
      id: 1,
      image: 'https://s3-alpha-sig.figma.com/img/01a0/8a50/68588a3bbebc627aedc97291e6fbe306?Expires=1724025600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=ZWeFaPfcjHIO80B0Mr8KrjlM798MTCf-ICeH7-Y-VphFYRUwPqjvufdypGFNSHSGQo~E7uEbvgP0-dlVlvI89OJkyP3wLLRJchQcAbPNtKgVfkc7E0U3nz5CzPrMLvw8rGkhuApCQ6LP3lBN7Zmy9a3eCrTUqnD1vVsa6jRfjrIRFFCNNum-zC5DBFqWKHHR6yqxv~ogoX5vje-1alVNsxTJO8ecw5BDc5N0JWGSYqzmh7wNwUNm4FTwGuU1bz3CN2r0e6xiE8QP3LzMMigd4Nzp6HyZI4CQhSDnQe2nu8iyzw2VhF-BAgbM~VtawilD9pKmTGER1uveu~0gr~zxEQ__',
      name: 'Nike Running Shoes',
      category: 'Shoes',
      sales: 12,
      price: 30,
      stock: 220,
    },
    {
        id: 0,
        image: 'https://s3-alpha-sig.figma.com/img/77f3/effb/fb100e13d1fda6e3214650f8f469e9e2?Expires=1724025600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=h~EjNpwq2iuoz0o1ZvLMAeYFWly-IE0XDIYPfcfXmkhnEPhf2dKWO2JKb0S8wqBDQungsrmgb~buLvVlIyzEf9swY926aHQmpI6dXlHc1VU5x0YVpO5GJ3652YyNt9cmlvnuTPDF6hmPhp31ch9UhMykqUeNI~fRvSitXcJiJeSgUtuZF09UH~DmaU9blSFTXwQ29vCMltVBMkRmRcpkjAKSI8yclyBvcK3cC8JfkekInedG4yAS3-wal92oiyUUldcU~wmkd~5cLo7FPqda8aPKEDBm6gOYInQJDCnvHt--3pVUfx1p6UJ7cPAh~klk~Ck8OlE81aM0CT6kCfBUig__',
        name: 'Nike Running Shoes',
        category: 'Shoes',
        sales: 12,
        price: 30,
        stock: 220,
      },
      {
        id: 2,
        image: 'https://s3-alpha-sig.figma.com/img/65d7/24e3/f3827e3fb4e0e2f672f5dcb2e53890cc?Expires=1724025600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Se-CQiDKR7L0D9lSTgx64it3mfhqrDGnw2Kdbpt37kQx-8zLBiTLaq8rNHpBN0r4kWmm-GX3vARyUJqS89Hz6AhZ7Kjhm0Knn4TY~TD6~UBS9o2MQRI5zrO08tb1EsRA0Er5l~tp--JxN9~GbTtBF-1MocJToNcnEti7HMAiUYQo~1g~2B4Q4toHRzi46ASxorLhNojnavCj18wjoubCKQwBsWkB3yZAZtifrODEw0LXiKzw0rVm7YVgQxVn~jFC6eDt4FI9SZfY2lGuGsdBmDPlRR2nR6DeJWJ4w1VsztZAw6sydqDvMeXAqegHkzj8Fhs0el~xIL29aD242sTiWw__',
        name: 'Nike Running Shoes',
        category: 'Shoes',
        sales: 12,
        price: 30,
        stock: 220,
      },
      {
        id: 3,
        image: 'https://s3-alpha-sig.figma.com/img/1a62/db54/01ff529f40e4e3bcd0cedae884141d3f?Expires=1724025600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=C8T19PorW0JXEW9NQmQP1UOxeSCRyPAY9lDHiPfO1LoYCUmqzA8AOt6aPtZLr~TSqUdXf~INkkclZbz1bkHlCBHbIOHcVd~pBiB9V7ATbLtMGNHh7Y9YDhOED~D-iaaxTZJWbp-IfZMrK7tC1X9qQPdEiLDNpoDBfWObI~9FtHvcMf~TzAuaPW8-A-qDSGc-yjlCFv6DQQWuIUneZKNKGffo5Q3jAcJNEz6RyUynvfmvVTVKTR0N5m-oVwG4qHdNj4OQ2w6S1JYbgdmHFqA8fkn2o3U0YmtVH1g5BTtuVzFmqZicpSa7gEm1ce8dMateH56~dkEFxNYV9X0UDNx58w__',
        name: 'Nike Running Shoes',
        category: 'Shoes',
        sales: 12,
        price: 30,
        stock: 220,
      },
      {
        id: 4,
        image: 'https://s3-alpha-sig.figma.com/img/2502/934e/d09886ebd0daed9aed1498999ce1dd42?Expires=1724025600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=iJWtxQDjBwEhPprAXs39aIepNBInXwma8rKuvVED4-b~PLyVscO7MtQo6EPkZyxFRphVJkSTrbmETPdU9T~s3yatnfzb8PgeIOYf7CpmLYr62eZ3ZU5S9VgWunbRVXHwTfOFleH73y3v-CThgAIoCQAayh58eRuciRGXjoslTv36neIJKYJ-rE5y5hNzKJWA3U0j~o6sZNuHhIuNAY-2f-ubPPTwTzdC1-aJSiqH1VaDJr~D7h-4~8ypN7Q05Omdg0zwrIRkl~8gfYPF0cSjXnRmqyOAtRTUG7fYX5hV-VHs5y2OqkJMQtBBrnEsIIG3XH6pe589ISyYZaSzB3Dfew__',
        name: 'Nike Running Shoes',
        category: 'Shoes',
        sales: 12,
        price: 30,
        stock: 220,
      },
    // ... more products
  ];

  return <ProductTable products={products} />;
};

export default ProductsTable;