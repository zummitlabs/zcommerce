import React from 'react';


const ProductRow = ({ product }) => (
  <tr className="border-b hover:bg-gray-50">
    <td className="py-2 px-4">
      <input type="checkbox" className="rounded" />
    </td>
    <td className="py-2 px-4">
      <img src={product.image} alt={product.name} className="w-12 h-12 object-cover rounded" />
    </td>
    <td className="py-2 px-4 font-open-sans font-semibold">{product.name}</td>
    <td className="py-2 px-4 font-open-sans font-semibold">{product.category}</td>
    <td className="py-2 px-4 font-open-sans font-semibold">{product.sales}</td>
    <td className="py-2 px-4 font-open-sans font-semibold">${product.price}</td>
    <td className="py-2 px-4 font-open-sans font-semibold">{product.stock}</td>
    <td className="py-2 px-4 font-open-sans font-semibold">
      <button className="text-gray-500 mr-2">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
          <path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z" />
        </svg>
      </button>
      <button className="text-gray-500">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
          <path fillRule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clipRule="evenodd" />
        </svg>
      </button>
    </td>
  </tr>
);

const ProductTable = ({ products }) => {
  return (
    <div className="container mx-auto p-6">
      <h1 className="text-2xl font-bold mb-4">Products (22)</h1>
      <div className="overflow-x-auto bg-white rounded-lg shadow">
        <table className="w-full table-auto">
          <thead>
            <tr className="bg-[#FEE5B7] text-gray-600 uppercase text-sm leading-normal">
              <th className="py-3 px-4 text-left"></th>
              <th className="py-3 px-4 text-left font-poppins font-medium">Product</th>
              <th className="py-3 px-4 text-left font-poppins font-medium">Name</th>
              <th className="py-3 px-4 text-left font-poppins font-medium">Category</th>
              <th className="py-3 px-4 text-left font-poppins font-medium">Sales</th>
              <th className="py-3 px-4 text-left font-poppins font-medium">Price</th>
              <th className="py-3 px-4 text-left font-poppins font-medium">Stock</th>
              <th className="py-3 px-4 text-left font-poppins font-medium">Action</th>
            </tr>
          </thead>
          <tbody className="text-gray-600 text-sm font-light">
            {products.map((product) => (
              <ProductRow key={product.id} product={product} />
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ProductTable;