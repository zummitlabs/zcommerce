import React from 'react';

import { BsBell, BsFileEarmarkText, BsChatRightDots, BsFilter } from 'react-icons/bs';
import { LuCalendar, LuArrowDownUp } from 'react-icons/lu';
import { IoIosArrowForward, IoMdArrowDropdown, IoIosSearch, IoIosAddCircleOutline } from 'react-icons/io';


import { AiFillCaretUp } from 'react-icons/ai';
import { RxDotsHorizontal } from 'react-icons/rx';






const ViewOrdersDashboard = () => {
  return (
    <section>
        
        <div className="p-2 pl-10">
          <div className="mb-4 flex justify-between items-center">
            <div>
              <p className="text-sm text-gray-500 pb-3">Dashboard / Orders</p>
              <h2 className="text-3xl font-semibold">View Orders</h2>
            </div>
            <div className="mr-5">
              <button className="border-purple-800 border-2 text-purple-800 px-3 py-1 rounded mr-2 bg-white hover:bg-purple-800 hover:text-white shadow-lg transform hover:translate-y-0.5 transition duration-200">
                More Options
              </button>
              <button className="bg-purple-800 text-white px-3 py-1 rounded shadow-lg hover:bg-white hover:text-purple-800 hover:border-purple-800 border-2 border-transparent transform hover:translate-y-0.5 transition duration-200">
                Create Order
              </button>
            </div>
          </div>
          <div className="flex items-center bg-[#F9F9F9] rounded">
            <span className="mr-2 text-2xl">
              <LuCalendar />
            </span>
            <select className=" px-2 py-1">
              <option>March 1 - 31 March</option>
            </select>
          </div>
        </div>
        <div className="p-6 pl-16">
          {/* Stats cards */}
          <div className="grid grid-cols-4 gap-20 mb-6">
            <div className="relative p-[2px] rounded-xl bg-gradient-to-r from-[#FCAF3E] via-[#6B22AE] to-[#3B0076] w-[300px] h-[150px]">
              <div className="bg-white p-4 rounded-xl shadow">
                <h3 className="text-sm font-semibold mb-2 pb-2 border-b-2">
                  Total Orders
                </h3>
                <p className="text-3xl font-bold pt-3 flex items-center">
                  31 -
                  <span className="ml-auto">
                    <IoIosArrowForward className="text-3xl p-1 border-2 rounded shadow border-gray-400 hover:bg-gray-400 hover:text-white" />
                  </span>
                </p>
                <p className="text-sm text-gray-500 pt-1 flex items-center">
                  <AiFillCaretUp className="text-2xl mr-1" /> 30.2% last week
                </p>
              </div>
            </div>
            <div className="relative p-[2px] rounded-xl bg-gradient-to-r from-[#6B22AE] via-[#FCAF3E] to-[#3B0076] w-[300px] h-[150px]">
              <div className="bg-white p-4 rounded-xl shadow  ">
                <h3 className="text-sm font-semibold-xl mb-2 pb-2 border-b-2">
                  Total Returns
                </h3>
                <p className="text-3xl font-bold pt-3 flex items-center">
                  0 -
                  <span className="ml-auto">
                    <IoIosArrowForward className="text-3xl p-1 border-2 rounded shadow border-gray-400  hover:bg-gray-400 hover:text-white" />
                  </span>
                </p>
                <p className="text-sm text-gray-500 pt-1 flex items-center">
                  <IoMdArrowDropdown className="text-2xl mr-1" /> 0.2% last
                  week
                </p>
              </div>
            </div>
            <div className="relative p-[2px] rounded-xl bg-gradient-to-r from-[#6B22AE] via-[#FCAF3E] to-[#3B0076] w-[300px] h-[150px]">
              <div className="bg-white p-4 rounded-xl shadow  ">
                <h3 className="text-sm font-semibold mb-2  pb-2 border-b-2">
                  Total Cancellations
                </h3>
                <p className="text-3xl font-bold pt-3 flex items-center">
                  2 -
                  <span className="ml-auto">
                    <IoIosArrowForward className="text-3xl p-1 border-2 rounded shadow border-gray-400  hover:bg-gray-400 hover:text-white" />
                  </span>
                </p>
                <p className="text-sm text-gray-500 pt-1 flex items-center">
                  <AiFillCaretUp className="text-2xl mr-1" /> 5.8% last week
                </p>
              </div>
            </div>
            <div className="relative p-[2px] rounded-xl bg-gradient-to-r from-[#6B22AE] via-[#3B0076] to-[#FCAF3E] w-[300px] h-[150px]">
              <div className="bg-white p-4 rounded-xl shadow  ">
                <h3 className="text-sm font-semibold mb-2  pb-2 border-b-2">
                  Fulfilled Orders over time
                </h3>
                <p className="text-3xl font-bold pt-3 flex items-center">
                  22 -
                  <span className="ml-auto">
                    <IoIosArrowForward className="text-3xl p-1 border-2 rounded shadow border-gray-400  hover:bg-gray-400 hover:text-white" />
                  </span>
                </p>
                <p className="text-sm text-gray-500 pt-1 flex items-center">
                  <AiFillCaretUp className="text-2xl mr-1" /> 45.8% last week
                </p>
              </div>
            </div>
          </div>

          {/* Orders table */}
          <div className="p-3 flex items-center ">
            <div className="bg-white rounded-xl shadow   p-3 border-b flex items-center w-max mb-6 border  border-gray-300 gap-9">
              <button className="bg-yellow-200 text-black px-5 py-1 rounded mr-2 text-xl">
                All
              </button>
              <button className="text-gray-500 mr-2 text-lg">
                Unfulfilled
              </button>
              <button className="text-gray-500 mr-2 text-lg">Open</button>
              <button className="text-gray-500 mr-2 text-lg">Closed</button>
              <button className="text-gray-500 flex items-center text-lg">
                <IoIosAddCircleOutline className="text-l mr-1" /> Add
              </button>
            </div>
            <div className="flex items-center gap-5 ml-auto">
              <button className="text-2xl border-2 border-gray-200 text-gray-400 p-2 rounded-md hover:bg-gray-200 hover:text-gray-700">
                <IoIosSearch />
              </button>
              <button className="text-2xl border-2 border-gray-200 text-gray-400 p-2 rounded-md hover:bg-gray-200 hover:text-gray-700">
                <BsFilter />
              </button>
              <button className="text-2xl border-2 border-gray-200 text-gray-400 p-2 rounded-md hover:bg-gray-200 hover:text-gray-700">
                <RxDotsHorizontal />
              </button>
              <button className="text-2xl border-2 border-gray-200 text-gray-400 p-2 rounded-md hover:bg-gray-200 hover:text-gray-700">
                <LuArrowDownUp />
              </button>
            </div>
          </div>

          <div className="bg-white rounded shadow ">
            <table className="w-full">
              <thead>
                <tr className="bg-yellow-200 rounded-full text-left">
                  <th className="p-3">Order</th>
                  <th className="p-3">Date</th>
                  <th className="p-3">Customer</th>
                  <th className="p-3">Payment</th>
                  <th className="p-3">Total</th>
                  <th className="p-3">Items</th>
                  <th className="p-3">Fulfillment</th>
                  <th className="p-3">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr className="border-b">
                  <td className="p-3">#1003</td>
                  <td className="p-3">11-04-24</td>
                  <td className="p-3">Jake Evans</td>
                  <td className="p-3">
                    <span className="border border-yellow-400 bg-transparent text-yellow-400 font-semibold px-3 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-yellow-400 hover:text-white ">
                      Pending
                    </span>
                  </td>
                  <td className="p-3">$20.00</td>
                  <td className="p-3">2 items</td>
                  <td className="p-3">
                    <span className="border border-red-800 bg-transparent text-red-800 px-3 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-red-800 hover:text-white">
                      Unfulfilled
                    </span>
                  </td>
                  <td className="p-3">
                    <button className="text-gray-500 mr-4 text-xl">
                      <BsFileEarmarkText />
                    </button>
                    <button className="text-gray-500 text-lg">
                      <BsChatRightDots />
                    </button>
                  </td>
                </tr>
                <tr className="border-b">
                  <td className="p-3">#1003</td>
                  <td className="p-3">11-04-24</td>
                  <td className="p-3">Jake Evans</td>
                  <td className="p-3">
                    <span className="border border-yellow-400 bg-transparent text-yellow-400 font-semibold px-3 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-yellow-400 hover:text-white ">
                      Pending
                    </span>
                  </td>
                  <td className="p-3">$20.00</td>
                  <td className="p-3">2 items</td>
                  <td className="p-3">
                    <span className="border border-red-800 bg-transparent text-red-800 px-3 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-red-800 hover:text-white">
                      Unfulfilled
                    </span>
                  </td>
                  <td className="p-3">
                    <button className="text-gray-500 mr-4 text-xl">
                      <BsFileEarmarkText />
                    </button>
                    <button className="text-gray-500 text-lg">
                      <BsChatRightDots />
                    </button>
                  </td>
                </tr>
                <tr className="border-b">
                  <td className="p-3">#1003</td>
                  <td className="p-3">11-04-24</td>
                  <td className="p-3">Jake Evans</td>
                  <td className="p-3">
                    <span className="border border-green-500 bg-transparent text-green-500 font-semibold px-4 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-green-500 hover:text-white ">
                      Success
                    </span>
                  </td>
                  <td className="p-3">$20.00</td>
                  <td className="p-3">2 items</td>
                  <td className="p-3">
                    <span className="border border-green-500 bg-transparent text-green-500 px-5 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-green-500 hover:text-white">
                      Fulfilled
                    </span>
                  </td>
                  <td className="p-3">
                    <button className="text-gray-500 mr-4 text-xl">
                      <BsFileEarmarkText />
                    </button>
                    <button className="text-gray-500 text-lg">
                      <BsChatRightDots />
                    </button>
                  </td>
                </tr>
                <tr className="border-b">
                  <td className="p-3">#1003</td>
                  <td className="p-3">11-04-24</td>
                  <td className="p-3">Jake Evans</td>
                  <td className="p-3">
                    <span className="border border-green-500 bg-transparent text-green-500 font-semibold px-4 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-green-500 hover:text-white ">
                      Success
                    </span>
                  </td>
                  <td className="p-3">$20.00</td>
                  <td className="p-3">2 items</td>
                  <td className="p-3">
                    <span className="border border-green-500 bg-transparent text-green-500 px-5 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-green-500 hover:text-white">
                      Fulfilled
                    </span>
                  </td>
                  <td className="p-3">
                    <button className="text-gray-500 mr-4 text-xl">
                      <BsFileEarmarkText />
                    </button>
                    <button className="text-gray-500 text-lg">
                      <BsChatRightDots />
                    </button>
                  </td>
                </tr>
                <tr className="border-b">
                  <td className="p-3">#1003</td>
                  <td className="p-3">11-04-24</td>
                  <td className="p-3">Jake Evans</td>
                  <td className="p-3">
                    <span className="border border-yellow-400 bg-transparent text-yellow-400 font-semibold px-3 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-yellow-400 hover:text-white ">
                      Pending
                    </span>
                  </td>
                  <td className="p-3">$20.00</td>
                  <td className="p-3">2 items</td>
                  <td className="p-3">
                    <span className="border border-red-800 bg-transparent text-red-800 px-3 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-red-800 hover:text-white">
                      Unfulfilled
                    </span>
                  </td>
                  <td className="p-3">
                    <button className="text-gray-500 mr-4 text-xl">
                      <BsFileEarmarkText />
                    </button>
                    <button className="text-gray-500 text-lg">
                      <BsChatRightDots />
                    </button>
                  </td>
                </tr>
                <tr className="border-b">
                  <td className="p-3">#1003</td>
                  <td className="p-3">11-04-24</td>
                  <td className="p-3">Jake Evans</td>
                  <td className="p-3">
                    <span className="border border-green-500 bg-transparent text-green-500 font-semibold px-4 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-green-500 hover:text-white ">
                      Success
                    </span>
                  </td>
                  <td className="p-3">$20.00</td>
                  <td className="p-3">2 items</td>
                  <td className="p-3">
                    <span className="border border-green-500 bg-transparent text-green-500 px-5 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-green-500 hover:text-white">
                      Fulfilled
                    </span>
                  </td>
                  <td className="p-3">
                    <button className="text-gray-500 mr-4 text-xl">
                      <BsFileEarmarkText />
                    </button>
                    <button className="text-gray-500 text-lg">
                      <BsChatRightDots />
                    </button>
                  </td>
                </tr>
                <tr className="border-b">
                  <td className="p-3">#1003</td>
                  <td className="p-3">11-04-24</td>
                  <td className="p-3">Jake Evans</td>
                  <td className="p-3">
                    <span className="border border-yellow-400 bg-transparent text-yellow-400 font-semibold px-3 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-yellow-400 hover:text-white ">
                      Pending
                    </span>
                  </td>
                  <td className="p-3">$20.00</td>
                  <td className="p-3">2 items</td>
                  <td className="p-3">
                    <span className="border border-red-800 bg-transparent text-red-800 px-3 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-red-800 hover:text-white">
                      Unfulfilled
                    </span>
                  </td>
                  <td className="p-3">
                    <button className="text-gray-500 mr-4 text-xl">
                      <BsFileEarmarkText />
                    </button>
                    <button className="text-gray-500 text-lg">
                      <BsChatRightDots />
                    </button>
                  </td>
                </tr>
                <tr className="border-b">
                  <td className="p-3">#1003</td>
                  <td className="p-3">11-04-24</td>
                  <td className="p-3">Jake Evans</td>
                  <td className="p-3">
                    <span className="border border-green-500 bg-transparent text-green-500 font-semibold px-4 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-green-500 hover:text-white ">
                      Success
                    </span>
                  </td>
                  <td className="p-3">$20.00</td>
                  <td className="p-3">2 items</td>
                  <td className="p-3">
                    <span className="border border-green-500 bg-transparent text-green-500 px-5 py-1 rounded-lg text-m transition duration-300 ease-in-out hover:bg-green-500 hover:text-white">
                      Fulfilled
                    </span>
                  </td>
                  <td className="p-3">
                    <button className="text-gray-500 mr-4 text-xl">
                      <BsFileEarmarkText />
                    </button>
                    <button className="text-gray-500 text-lg">
                      <BsChatRightDots />
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        </section>
  )
}

export default ViewOrdersDashboard;
