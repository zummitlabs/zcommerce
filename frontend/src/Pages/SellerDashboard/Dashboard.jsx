import React, { useState } from "react";
import { LuLayoutDashboard, LuSettings } from "react-icons/lu";
import { TbChevronDown, TbChevronUp, TbBuildingStore } from "react-icons/tb";
import { PiNotepadBold } from "react-icons/pi";
import { BsFileEarmarkBarGraph, BsBell, BsHandbag } from "react-icons/bs";
import { TfiWallet } from "react-icons/tfi";
import { AiOutlineQuestionCircle } from "react-icons/ai";
import { HiOutlineBookOpen } from "react-icons/hi";
import AddNewProduct from "../../Components/AddNewProduct";
import ViewOrdersDashboard from "./ViewcartDashboard";
import Reviews_achievemnets from "../../Components/Reviews&achievemnets";
const Dashboard = () => {
  const [activeMenu, setActiveMenu] = useState(""); // State to track the active menu item

  const handleMenuClick = (menu) => {
    // Toggle the menu or set it as active
    setActiveMenu(activeMenu === menu ? "" : menu);
  };

  return (
    <div className="flex bg-[#F9F9F9]">
      {/* Sidebar */}
      <aside
        className="fixed top-0 left-0 w-[350px] h-screen bg-gradient-to-b from-[#31035a] via-[#8620C0] to-[#B739F2] text-white p-4 rounded-r-lg"
        style={{ fontFamily: "Poppins, sans-serif" }}
      >
        <div className="p-4 h-full flex flex-col">
          <div className="flex items-center justify-center mb-8">
            <img
              src="zcommerce/frontend/src/assets/images/logo.png"
              alt="Logo"
              className="w-10 h-10 mr-3"
            />
            <h1 className="text-2xl font-bold">Commerce</h1>
          </div>
          <nav>
            <ul>
              <div className="ml-4 space-y-4">
                <li className="mb-4">
                  <a
                    href="#"
                    onClick={() => handleMenuClick("dashboard")}
                    className={`flex items-center text-xl font-extralight ml-[-10px] p-3 rounded-l-full w-[320px] rounded-r-xl ${
                      activeMenu === "dashboard"
                        ? "bg-[#F9F9F9] text-black"
                        : "bg-none text-white"
                    }`}
                  >
                    <span className="mr-2 flex items-center text-xl">
                      <LuLayoutDashboard className="mr-2 text-2xl font-extralight" />
                      Dashboard
                      {activeMenu === "dashboard" ? (
                        <TbChevronUp className="ml-[100px] text-3xl" />
                      ) : (
                        <TbChevronDown className="ml-[100px] text-3xl" />
                      )}
                    </span>
                  </a>
                </li>
                <li className="mb-4">
                  <a
                    href="#"
                    onClick={() => handleMenuClick("orders")}
                    className={`flex items-center text-xl font-extralight ml-[-10px] p-3 rounded-l-full w-[320px] rounded-r-xl ${
                      activeMenu === "orders"
                        ? "bg-[#F9F9F9] text-black"
                        : "bg-none text-white"
                    }`}
                  >
                    <span className="mr-1 text-2xl">
                      <PiNotepadBold />
                    </span>
                    Orders
                    {activeMenu === "orders" ? (
                      <TbChevronUp className="ml-[150px] text-3xl" />
                    ) : (
                      <TbChevronDown className="ml-[150px] text-3xl" />
                    )}
                  </a>
                </li>
                <li className="mb-4">
                  <a
                    href="#"
                    onClick={() => handleMenuClick("productDetails")}
                    className={`flex items-center text-xl font-extralight ml-[-10px] p-3 rounded-l-full w-[320px] rounded-r-xl ${
                      activeMenu === "productDetails"
                        ? "bg-[#F9F9F9] text-black"
                        : "bg-none text-white"
                    }`}
                  >
                    <span className="mr-2 text-2xl">
                      <BsHandbag />
                    </span>
                    Product Details
                    {activeMenu === "productDetails" ? (
                      <TbChevronUp className="ml-[63px] text-3xl" />
                    ) : (
                      <TbChevronDown className="ml-[63px] text-3xl" />
                    )}
                  </a>
                  {activeMenu === "productDetails" && (
                    <ul className="list-disc ml-8 my-16">
                      <li className="mb-5">
                        <a
                          href="#"
                          className="text-purple-300 hover:text-purple-400 transition duration-150"
                        >
                          View Orders
                        </a>
                      </li>
                      <li className="mb-5">
                        <a
                          href="#"
                          className="text-purple-300 hover:text-purple-400 transition duration-150"
                        >
                          Cancellations
                        </a>
                      </li>
                      <li className="mb-5">
                        <a
                          href="#"
                          className="text-purple-300 hover:text-purple-400 transition duration-150"
                        >
                          Shipping Setting
                        </a>
                      </li>
                      <li className="mb-5">
                        <a
                          href="#"
                          className="text-purple-300 hover:text-purple-400 transition duration-150"
                        >
                          Drafts
                        </a>
                      </li>
                    </ul>
                  )}
                </li>
                <li className="mb-4">
                  <a
                    href="#"
                    onClick={() => handleMenuClick("myStore")}
                    className={`flex items-center text-xl font-extralight ml-[-10px] p-3 rounded-l-full w-[320px] rounded-r-xl ${
                      activeMenu === "myStore"
                        ? "bg-[#F9F9F9] text-black"
                        : "bg-none text-white"
                    }`}
                  >
                    <span className="mr-2 text-2xl font-extralight">
                      <TbBuildingStore />
                    </span>
                    My Store
                    {activeMenu === "myStore" ? (
                      <TbChevronUp className="ml-32 text-3xl" />
                    ) : (
                      <TbChevronDown className="ml-32 text-3xl" />
                    )}
                  </a>
                </li>
                <li className="mb-4">
                  <a
                    href="#"
                    onClick={() => handleMenuClick("marketing")}
                    className={`flex items-center text-xl font-extralight ml-[-10px] p-3 rounded-l-full w-[320px] rounded-r-xl ${
                      activeMenu === "marketing"
                        ? "bg-[#F9F9F9] text-black"
                        : "bg-none text-white"
                    }`}
                  >
                    <span className="mr-1 text-2xl font-extralight">
                      <BsFileEarmarkBarGraph />
                    </span>
                    Marketing
                    {activeMenu === "marketing" ? (
                      <TbChevronUp className="ml-[118px] text-3xl" />
                    ) : (
                      <TbChevronDown className="ml-[118px] text-3xl" />
                    )}
                  </a>
                </li>
                <li className="mb-4">
                  <a
                    href="#"
                    onClick={() => handleMenuClick("finance")}
                    className={`flex items-center text-xl font-extralight ml-[-10px] p-3 rounded-l-full w-[320px] rounded-r-xl ${
                      activeMenu === "finance"
                        ? "bg-[#F9F9F9] text-black"
                        : "bg-none text-white"
                    }`}
                  >
                    <span className="mr-2 text-2xl font-extralight">
                      <TfiWallet />
                    </span>
                    Finance
                    {activeMenu === "finance" ? (
                      <TbChevronUp className="ml-[134px] text-3xl" />
                    ) : (
                      <TbChevronDown className="ml-[134px] text-3xl" />
                    )}
                  </a>
                </li>
                <li className="list-none">
              <a
                href="#"
                onClick={() => handleMenuClick("support")}
                className={`flex items-center text-xl font-extralight ml-[-10px] p-2 rounded-l-full w-[320px] rounded-r-xl ${
                  activeMenu === "support"
                    ? "bg-[#F9F9F9] text-black"
                    : "bg-none text-white"
                }`}
              >
                <span className="mr-2 text-2xl font-extralight">
                  <AiOutlineQuestionCircle />
                </span>
                Support
                {activeMenu === "support" ? (
                  <TbChevronUp className="ml-[134px] text-3xl" />
                ) : (
                  <TbChevronDown className="ml-[134px] text-3xl" />
                )}
              </a>
            </li>
            <li className="mb-4 list-none">
              <a
                href="#"
                onClick={() => handleMenuClick("settings")}
                className={`flex items-center text-xl font-extralight ml-[-10px] p-2 rounded-l-full w-[320px] rounded-r-xl ${
                  activeMenu === "settings"
                    ? "bg-[#F9F9F9] text-black"
                    : "bg-none text-white"
                }`}
              >
                <span className="mr-2 text-2xl">
                  <LuSettings />
                </span>
                Settings
                {activeMenu === "settings" ? (
                  <TbChevronUp className="ml-[134px] text-3xl" />
                ) : (
                  <TbChevronDown className="ml-[134px] text-3xl" />
                )}
              </a>
            </li>
              </div>
              
            </ul>
          </nav>
          
        </div>
      </aside>
      {/* Main content */}
      <main className="ml-[350px] flex-1  overflow-y-auto">
        <header className="px-5 pt-5">
          <div className="flex justify-end mr-5">
            <button className="mr-3 text-2xl">
              <HiOutlineBookOpen />
            </button>
            <button className="pr-3 text-2xl border-r-2">
              <BsBell />
            </button>
            <div className="flex items-center">
              <img
                src="zcommerce/frontend/src/assets/images/Profile.png"
                alt="User"
                className="w-8 h-8 rounded-full ml-2 mr-2"
              />
              <select className="flex items-center">
                <option>Jane Roger</option>
              </select>
            </div>
          </div>
        </header>
                <ViewOrdersDashboard/>
      </main>
    </div>
  );
};

export default Dashboard;
