import { useState } from "react";
import axios from "axios";
import productData from "./productDisplay.json"

const ProductCard = () => {
  //hardcoded these because rightnow the frontend code for product is hardcoded and cant fetch product ids
  const [quantity, setQuantity] = useState(1);
  const [product_id, setProductId] = useState(productData.product_id);
  const [customer_id, setCustomerId] = useState(2);
  const [cartItems, setCartItems] = useState([]);
  const addToCartHandler = () => {
    axios.post('http://localhost:4000/addToCart', { customer_id, product_id, quantity })
      .then(response => {
        console.log(response.data);
        fetchCartItems();
      })
      .catch(error => {
        console.error('There was an error adding the product to the cart!', error);
      });
  };


  const fetchCartItems = () => {
    axios.get('http://localhost:4000/cart', { params: { customer_id } })
      .then(response => {
        setCartItems(response.data);
      })
      .catch(error => {
        console.error('There was an error fetching the cart items!', error);
      });
  };

  const addToWishlistHandler = () => {
    axios.post('http://localhost:4000/wishlist', { customer_id, product_id, })
      .then(response => {
        console.log(response.data.message);
      })
      .catch(error => {
        console.error('There was an error adding to the wishlist!', error);
      });
  };

  return (
    <div className="w-full bg-white h-[600px]  overflow-hidden  border-[2px] rounded-tl-[20px] border-purple-800 border-r-0 border-b-0">
      <div className="p-6 flex justify-between items-center mb-4">
        <div>
          <span className="text-3xl font-medium font-poppins">4.49$</span>
          <span className="text-gray-500 font-poppins line-through ml-2">8.99$</span>
        </div>
        <span className="bg-custom-gradient text-transparent bg-clip-text font-poppins font-bold text-3xl px-2 py-1 rounded">50%OFF</span>
      </div>
      <div className=" h-[1.5px] bg-gradient-to-r from-purple-500 to-yellow-500"></div>

      <div className="p-4  mb-4">
        <p className="text-black">Quantity:</p>
        <div className="w-full flex  justify-center mt-2">
          <button
            className="bg-orange-400 text-white rounded-full w-8 h-8 flex items-center justify-center"
            onClick={() => setQuantity(Math.max(1, quantity - 1))}
          >
            -
          </button>
          <span className="mx-4">{quantity}</span>
          <button
            className="bg-orange-400 text-white rounded-full w-8 h-8 flex items-center justify-center"
            onClick={() => setQuantity(quantity + 1)}
          >
            +
          </button>
        </div>
      </div>

      {/**/}
      <div className=" h-[1.5px] bg-gradient-to-r from-purple-500 to-yellow-500"></div>
      <div className="flex items-center justify-between px-4 py-2 text-sm text-black">
        <div className="ml-6 font-open-sans">Free Shipping</div>
        <div className="flex items-center justify-center mx-4">
          <div className="w-0.5 h-6 bg-gradient-to-b from-purple-500 to-yellow-400"></div>
        </div>
        <div className="mr-6 text-black font-open-sans">30 July - 1 August</div>
      </div>
      <div className="mb-2 h-[2px] bg-gradient-to-r from-purple-500 to-yellow-500"></div>
      {/**/}


      <div className="p-4 px-20">
        <button className="w-full bg-gradient-to-b from-[#7C2CCC] to-[#530993] text-white py-2 rounded-md mb-2">
          Buy Now
        </button>

        <button className="w-full border-2 border-transparent bg-clip-padding p-[2px] rounded-md mb-4" onClick={addToCartHandler}>
          <div className="bg-white text-purple-800 py-2 rounded-[5px]  gradient-border">
            Add to Cart
          </div>
        </button>
      </div>
      <div className="px-32 flex justify-between">
        <button className="border rounded px-4 py-2">
          <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
            <path d="M15 8a3 3 0 10-2.977-2.63l-4.94 2.47a3 3 0 100 4.319l4.94 2.47a3 3 0 10.895-1.789l-4.94-2.47a3.027 3.027 0 000-.74l4.94-2.47C13.456 7.68 14.19 8 15 8z" />
          </svg>
        </button>
        <button className="border rounded px-4 py-2" onClick={addToWishlistHandler}>
          <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
            <path fillRule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clipRule="evenodd" />
          </svg>
        </button>
      </div>
    </div>
  );
};

export default ProductCard;