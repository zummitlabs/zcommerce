import React, { useState } from 'react';
import productData from './productDisplay.json';
import ProductCard  from './ProductCard';

const ProductDisplay = () => {
  const [selectedImage, setSelectedImage] = useState(0);
  const [selectedAttributes, setSelectedAttributes] = useState({});

  const handleAttributeSelect = (attributeName, value) => {
    setSelectedAttributes(prev => ({ ...prev, [attributeName]: value }));
  };

const scrollToProductDiscription=()=>{
    const producDescriptionElement=document.getElementById('product-description');
    if(producDescriptionElement){
        producDescriptionElement.scrollIntoView({behavior:'smooth'})
    }
}

const categoryVar="Home / Women's Fashion / Accessories / Jewelry"
  return (
    <div className=" max-w-full ml-4 p-4 pr-0 relative">
        <div className=''>
            <div className='p-2 mb-1'>{categoryVar}</div>
            <div className="flex flex-col md:flex-row ">     
                {/* Image Section */}
                <div className="mr-8">

                <img 
                    src={productData.images[selectedImage]} 
                    alt={productData.name} 
                    className="w-[413px] h-[413px] rounded-lg shadow-lg mb-4"
                />
                <div 
                    className="w-[413px] overflow-auto pb-2 rounded-[5px]"
                    style={{
                    msOverflowStyle: 'none',  // IE and Edge
                    scrollbarWidth: 'none',  // Firefox
                    '::-webkit-scrollbar': {
                        display: 'none'  // Chrome, Safari and Opera
                    }
                    }}
                >
            
                    <div className="flex gap-[12px] min-w-max rounded-lg">
                        {productData.images.map((img, index) => (
                        <img 
                            key={index}
                            src={img} 
                            alt={`${productData.name} thumbnail ${index + 1}`}
                            className={`w-[75px] h-[75px] flex-shrink-0 object-cover rounded cursor-pointer ${
                            selectedImage === index ? 'border-2 border-blue-500' : ''
                            }`}
                            onClick={() => setSelectedImage(index)}
                        />
                        ))}
                    </div>
                </div>
                </div>
                {/* Product Details Section */}
                <div className="md:w-1/2 relative">
                <h1 className="text-2xl font-semibold font-poppins mb-2">{productData.name}</h1>
                <div className="flex items-center justify-between mb-4">
                    <div className="flex items-center">
                    <div className="flex text-black">
                        {[...Array(5)].map((_, i) => (
                        <svg key={i} className="w-5 h-5 fill-current" viewBox="0 0 24 24">
                            <path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />
                        </svg>
                        ))}
                    </div>
                    <span className="ml-2 text-gray-600">{productData.rating}</span>
                    </div>
                    <div className="flex items-center mr-10">
                    <span className="text-gray-600">{productData.reviews} reviews</span>
                    <span className="mx-2 text-gray-400">|</span>
                    <span className="text-gray-600">{productData.sold}+ sold</span>
                    </div>
                </div>

                {/* Dynamic Product Attributes */}

                {productData.attributes.map((attr) => (
                    <div key={attr.name} className="mb-4 ">
                    <h2 className="text-lg font-semibold mb-2">{attr.name}:</h2>
                    <div className="flex flex-wrap gap-2">
                        {attr.options.map((option) => (
                        <button 
                            key={option}
                            className={`px-4 py-2 border rounded-full hover:bg-gray-100 ${
                            selectedAttributes[attr.name] === option ? 'bg-blue-100 border-blue-500' : ''
                            }`}
                            onClick={() => handleAttributeSelect(attr.name, option)}
                        >
                            {option}
                        </button>
                        ))}
                    </div>
                    </div>
                ))}

                {/*Scroller*/}

                <div 
                onClick={scrollToProductDiscription}
                className='absolute bottom-3 right-2 cursor-pointer'
                >
                {`See product details >`}

                </div>
                </div>
            </div>
        </div>
    </div>
  );
};






export default ProductDisplay;