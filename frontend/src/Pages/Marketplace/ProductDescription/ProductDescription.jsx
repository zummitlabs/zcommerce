// ProductDescription.js
import React from 'react';
import productData from './productData.json';
import sellerData from './sellerData.json';

const ProductDescription = () => {
  return (
    <div className='max-w-full  ' id="product-description">
    <div className="max-w-3xl ml-0 p-6 bg-white rounded-lg">
      <h1 className="text-3xl font-bold mb-4">{productData.title}</h1>
      <p className="text-gray-600 mb-6">{productData.description}</p>

      <section className="mb-6">
        <h2 className="text-xl font-semibold mb-2">Key Features:</h2>
        <ul className="list-disc pl-6">
          {productData.keyFeatures.map((feature, index) => (
            <li key={index} className="text-gray-700 mb-1">{feature}</li>
          ))}
        </ul>
      </section>

      <section className="mb-6">
        <h2 className="text-xl font-semibold mb-2">Details:</h2>
        <ul className="list-disc pl-6">
          {productData.details.map((detail, index) => (
            <li key={index} className="text-gray-700 mb-1">{detail}</li>
          ))}
        </ul>
      </section>

      <section className="mb-6">
        <h2 className="text-xl font-semibold mb-2">Care Instructions:</h2>
        <ul className="list-disc pl-6">
          {productData.careInstructions.map((instruction, index) => (
            <li key={index} className="text-gray-700 mb-1">{instruction}</li>
          ))}
        </ul>
      </section>

      <section className="mb-6">
        <h2 className="text-xl font-semibold mb-2">Shipping and Returns:</h2>
        <ul className="list-disc pl-6">
          {productData.shippingReturns.map((item, index) => (
            <li key={index} className="text-gray-700 mb-1">{item}</li>
          ))}
        </ul>
      </section>

      <section>
        <h2 className="text-xl font-semibold mb-2">Why Shop with Us:</h2>
        <ul className="list-disc pl-6">
          {productData.whyShopWithUs.map((reason, index) => (
            <li key={index} className="text-gray-700 mb-1">{reason}</li>
          ))}
        </ul>
      </section>
      <div className='p-6 mt-10'>
      <SellerInfo/>
      </div>
    </div>

   
  </div>
  );
};



const SellerInfo = () => {
  return (
    <div className="flex items-center justify-between p-4 border border-gray-200 rounded-lg shadow-sm mb-6">
      <div className="flex items-center">
        <img 
          src={sellerData.logoUrl} 
          alt={sellerData.storeName} 
          className="w-12 h-12 rounded-full mr-4"
        />
        <div>
          <h2 className="text-lg font-semibold">{sellerData.storeName}</h2>
          <div className="flex items-center">
            <svg className="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20">
              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
            </svg>
            <span className="ml-1 text-gray-600">
              {sellerData.rating} / {sellerData.totalReviews.toLocaleString()}+
            </span>
          </div>
        </div>
      </div>
      <svg className="w-6 h-6 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24">
        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
      </svg>
    </div>
  );
};




export default ProductDescription;