import Review from "./Review";

function ReviewDetail() {


    const reviews = [
        {
          rating: 5,
          color: 'White',
          text: 'Beautiful necklace!!',
          date: '2 June 2024',
          user: 'zCommerce Shopper',
          images: [
            'https://picsum.photos/200/300?random=1',
            'https://picsum.photos/250/350?random=2'
          ],
        },
        {
          rating: 4,
          color: 'White',
          text: 'Very nice necklace.',
          date: '1 June 2024',
          user: 'zCommerce Shopper',
          images: [
            'https://picsum.photos/220/320?random=3',
            'https://picsum.photos/240/330?random=4'
          ],
        },
        {
          rating: 3,
          color: 'White',
          text: 'Good, but not great.',
          date: '31 May 2024',
          user: 'zCommerce Shopper',
          images: [
            'https://picsum.photos/210/310?random=5',
            'https://picsum.photos/230/340?random=6'
          ],
        },
        {
          rating: 2,
          color: 'White',
          text: 'Not what I expected.',
          date: '30 May 2024',
          user: 'zCommerce Shopper',
          images: [
            'https://picsum.photos/260/360?random=7',
            'https://picsum.photos/270/370?random=8'
          ],
        },
        {
          rating: 1,
          color: 'White',
          text: 'Disappointed.',
          date: '29 May 2024',
          user: 'zCommerce Shopper',
          images: [],
        },
        {
            rating: 1,
            color: 'White',
            text: 'Disappointed.',
            date: '29 May 2024',
            user: 'zCommerce Shopper',
            images: [],
          },
          {
            rating: 1,
            color: 'White',
            text: 'Disappointed.',
            date: '29 May 2024',
            user: 'zCommerce Shopper',
            images: [],
          },
          {
            rating: 3,
            color: 'White',
            text: 'Disappointed.',
            date: '29 May 2024',
            user: 'zCommerce Shopper',
            images: [],
          },
          {
              rating: 2,
              color: 'White',
              text: 'Disappointed.',
              date: '29 May 2024',
              user: 'zCommerce Shopper',
              images: [],
            },
            {
              rating: 4,
              color: 'White',
              text: 'Disappointed.',
              date: '29 May 2024',
              user: 'zCommerce Shopper',
              images: [],
            },
      ];
    

    return ( <>
     <Review
        totalReviews={528}
        averageRating={4.8}
        reviews={reviews}
      />
    </> );
}

export default ReviewDetail;