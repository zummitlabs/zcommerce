import React, { useState, useEffect } from 'react';
import { FaStar, FaRegStar, FaCamera, FaCommentDots } from 'react-icons/fa';
import ImageModal from './ImageModal';

const Review = ({ reviews }) => {
  const [selectedImages, setSelectedImages] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [filter, setFilter] = useState('all');
  const [starFilter, setStarFilter] = useState(null);
  const [visibleReviews, setVisibleReviews] = useState(3); // Number of reviews initially visible

  const openModal = (images) => {
    setSelectedImages(images);
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setSelectedImages([]);
  };

  const filterReviews = () => {
    let filteredReviews = reviews;

    if (filter === 'images') {
      filteredReviews = filteredReviews.filter(review => review.images.length > 0);
    } else if (filter === 'comments') {
      filteredReviews = filteredReviews.filter(review => review.images.length === 0);
    }

    if (starFilter !== null) {
      filteredReviews = filteredReviews.filter(review => review.rating === starFilter);
    }

    return filteredReviews;
  };

  const loadMoreReviews = () => {
    setVisibleReviews(prevVisibleReviews => prevVisibleReviews + 3);
  };

  const showAllImages = () => {
    const allImages = reviews.flatMap(review => review.images);
    openModal(allImages);
  };

  // Calculate total number of reviews and average rating
  const totalReviews = reviews.length;
  const averageRating = (reviews.reduce((sum, review) => sum + review.rating, 0) / totalReviews).toFixed(1);

  return (
    <div className="p-4">
      <div className="flex items-center">
        <h2 className="text-xl font-bold">Reviews</h2>
        <span className="ml-2 text-gray-600">({totalReviews})</span>
      </div>
      <div className="flex items-center mt-2">
        <div className="flex">
          {[...Array(5)].map((star, index) => (
            <span key={index}>
              {index < averageRating ? (
                <FaStar className="text-black" />
              ) : (
                <FaRegStar className="text-black" />
              )}
            </span>
          ))}
        </div>
        <span className="ml-2 text-lg font-semibold">{averageRating}</span>
      </div>
      <div className="flex mt-4 space-x-2">
        <button
          className={`px-4 py-2 rounded-full ${filter === 'all' ? 'bg-gray-300' : 'bg-gray-200'}`}
          onClick={() => setFilter('all')}
        >
          All
        </button>
        <button
          className={`px-4 py-2 rounded-full flex items-center ${filter === 'images' ? 'bg-gray-300' : 'bg-[#fff4b8]'}`}
          onClick={showAllImages}
        >
          <FaCamera className="mr-1" /> {reviews.flatMap(review => review.images).length}
        </button>
        <button
          className={`px-4 py-2 rounded-full flex items-center ${filter === 'comments' ? 'bg-gray-300' : 'bg-[#fff4b8]'}`}
          onClick={() => setFilter('comments')}
        >
          <FaCommentDots className="mr-1" /> {reviews.filter(review => review.images.length === 0).length}
        </button>
        {[5, 4, 3, 2, 1].map((rating) => (
          <button
            key={rating}
            className={`px-4 py-2 rounded-full flex items-center ${starFilter === rating ? 'bg-gray-300' : 'bg-[#fff4b8]'}`}
            onClick={() => setStarFilter(starFilter === rating ? null : rating)}
          >
            {rating} <FaStar className="ml-1 text-black" />
          </button>
        ))}
      </div>
      <div className="mt-4">
        {filterReviews().slice(0, visibleReviews).map((review, index) => (
          <div key={index} className="border-b pb-4 mb-4">
            <div className="flex items-center">
              {[...Array(5)].map((star, idx) => (
                <span key={idx}>
                  {idx < review.rating ? (
                    <FaStar className="text-black" />
                  ) : (
                    <FaRegStar className="text-black" />
                  )}
                </span>
              ))}
            </div>

            <div className="flex justify-between">
              <div>
                <div className="mt-2">Color: {review.color}</div>
                <div className="mt-2">{review.text}</div>
              </div>
              <div>
                <div className="text-gray-600 mt-2">{review.date}</div>
                <div className="text-gray-600">{review.user}</div>
              </div>
            </div>
            <div className="flex mt-2 space-x-2">
              {review.images.map((image, idx) => (
                <img
                  key={idx}
                  src={image}
                  alt={`review-${index}-image-${idx}`}
                  className="w-16 h-16 object-cover cursor-pointer"
                  onClick={() => openModal(review.images)}
                />
              ))}
            </div>
          </div>
        ))}
      </div>
      {visibleReviews < filterReviews().length && (
             <div className="flex justify-center mt-4">
             <button 
               className="w-[161px] h-[45px] py-[10px] font-open-sans font-bold rounded-[8px] text-white"
               onClick={loadMoreReviews}
               style={
                {
                'background': 'linear-gradient(180deg, #7C2CCC 0%, #530993 100%)'
               }
            }
             >
               See More
             </button>
           </div>
      )}
      {isModalOpen && (
        <ImageModal images={selectedImages} onClose={closeModal} />
      )}
    </div>
  );
};

export default Review;
