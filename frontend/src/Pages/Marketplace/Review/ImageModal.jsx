import React, { useState } from 'react';
import { FaTimes, FaChevronLeft, FaChevronRight } from 'react-icons/fa';

const ImageModal = ({ images, onClose }) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  const handlePrev = () => {
    setCurrentIndex((prevIndex) => (prevIndex === 0 ? images.length - 1 : prevIndex - 1));
  };

  const handleNext = () => {
    setCurrentIndex((prevIndex) => (prevIndex === images.length - 1 ? 0 : prevIndex + 1));
  };

  return (
    <div className="fixed inset-0 bg-black bg-opacity-75 flex items-center justify-center z-50">
      <div className="relative w-11/12 max-w-3xl bg-white rounded-lg">
        <button className="absolute top-2 right-2 text-black" onClick={onClose}>
          <FaTimes size={24} />
        </button>
        <div className="flex items-center justify-between p-4">
          <button onClick={handlePrev}>
            <FaChevronLeft size={24} />
          </button>
          <img src={images[currentIndex]} alt={`modal-img-${currentIndex}`} className="max-h-96 object-contain" />
          <button onClick={handleNext}>
            <FaChevronRight size={24} />
          </button>
        </div>
        <div className="p-4">
          <p className="text-center">{currentIndex + 1} / {images.length}</p>
        </div>
      </div>
    </div>
  );
};

export default ImageModal;
