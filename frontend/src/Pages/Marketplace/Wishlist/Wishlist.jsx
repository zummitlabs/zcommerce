import React, { useState } from "react";
import { PiShoppingCartSimpleFill } from "react-icons/pi";
import { IoMdHeartEmpty, IoMdHeart } from "react-icons/io";
import { FaTrash } from "react-icons/fa";
import { HiPlus } from "react-icons/hi";
import Navbar from "../Components/Navbar";
import FooterProductDetails from "../Components/FooterProductDetails";

const WishlistPage = () => {
  const HeartButton = () => {
    const [liked, setLiked] = useState(false);

    const toggleLiked = () => {
      setLiked(!liked);
    };

    return (
      <button
        onClick={toggleLiked}
        className="absolute bottom-2 right-2 bg-white rounded-[15px] pt-3 pr-[14px] pb-3 pl-3 w-[61px] h-[59px] shadow-md"
      >
        {liked ? (
          <IoMdHeart className="w-[35px] h-[35px] text-[#530993]" />
        ) : (
          <IoMdHeartEmpty className="w-[35px] h-[35px] text-[#530993]" />
        )}
      </button>
    );
  };
  const RadioButton = ({ onClick }) => {
    return (
      <input
        type="radio"
        className="mr-2 w-[26px] h-[26px] accent-purple-900"
        onClick={onClick}
      />
    );
  };

  const WishlistItem = ({ item }) => {
    const [isSelected, setIsSelected] = useState(false);

    const handleSelect = () => {
      setIsSelected(true);
    };

    return (
      <div
        key={item.id}
        className={`flex items-start p-4 border-2 mb-4 rounded-[16px] h-[143px] font-[Open_Sans] font-regular relative ${
          isSelected ? "shadow-md" : ""
        }`}
      >
        {/* Checkbox Positioned Absolutely in the Top-Left of the Card */}
        <RadioButton onClick={handleSelect} />

        <img
          src={item.image}
          alt={item.description}
          className="w-20 h-20 object-cover rounded-md mr-4 ml-5"
        />

        <div className="flex-1 ml-[24px]">
          <p className="text-[16px] mb-10">{item.description}</p>
          <p className="text-[14px] text-gray-500">Color/Size</p>
        </div>
        <div className="text-right flex flex-col ml-[188px] items-end gap-3">
          <button>
            <FaTrash className="text-xl" />
          </button>
          <div className="flex items-end gap-3">
            <div className="flex flex-col">
              <p className="opacity-50 text-right text-[14px]">
                {item.discount}
              </p>
              <p className="font-bold text-[24px]">₹{item.price.toFixed(2)}</p>
            </div>
            <button className="border-[2px] border-purple-900 text-purple-900 px-4 py-2 w-[104px] h-[72px] rounded-md mt-2 flex items-center hover:shadow-[4px_4px_0px_0px_rgba(91,20,153,1)] transition-shadow duration-300 ease-in-out text-3xl">
              <PiShoppingCartSimpleFill className="w-[40px] h-[40px]" />
              <HiPlus className="text-[24px]" />
            </button>
          </div>
        </div>
      </div>
    );
  };

  const wishlistItems = [
    {
      id: 1,
      image: "src/assets/images/wish-1.png",
      description:
        "Lorem ipsum dolor sit amet consectetur. Porttitor lorem mattis diam faucibus congue.",
      discount: "50%",
      price: 288.48,
    },
    {
      id: 2,
      image: "src/assets/images/wish-2.png",
      description:
        "Lorem ipsum dolor sit amet consectetur. Porttitor lorem mattis diam faucibus congue.",
      discount: "50%",
      price: 288.48,
    },
    {
      id: 3,
      image: "src/assets/images/wish-3.png",
      description:
        "Lorem ipsum dolor sit amet consectetur. Porttitor lorem mattis diam faucibus congue.",
      discount: "50%",
      price: 288.48,
    },
  ];

  const recommendedItems = [
    {
      id: 1,
      name: "Gold Pearl Earrings",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-1.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 2,
      name: "Set of 2 - Round Sunglasses",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-2.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 3,
      name: "White Plain T-Shirt",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-3.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 4,
      name: "Pearl Rose Pendant",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-4.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 5,
      name: "Pearl Necklace",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-5.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 6,
      name: "White Marble Beads Bracelet",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-1.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 7,
      name: "Beads Necklace",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-2.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 8,
      name: "Pearl Diamond Necklace",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-3.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 9,
      name: "Gold Layered Necklace",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-4.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 10,
      name: "Gold Multi-colored Earrings",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-5.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 11,
      name: "White Long Coat",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-1.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 12,
      name: "Golden Hoop Earrings",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-2.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 13,
      name: "Silver Necklace",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-3.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 14,
      name: "Orange Blazer",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-4.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 15,
      name: "Beige Heels",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-5.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 16,
      name: "Yellow Top Up Boots",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-1.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 17,
      name: "Lavender Jacket",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-2.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 18,
      name: "Green Sunglasses",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-3.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 19,
      name: "Brown Long Jumper",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-4.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 20,
      name: "Printed Cheetah Sun Hat",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-5.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 21,
      name: "Pink Shrug",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-1.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 22,
      name: "Blue Men’s Shirt",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-2.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 23,
      name: "Pink Long Sleeve Shrug",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-3.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 24,
      name: "Blue Ankle Length Shoes",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-4.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
    {
      id: 25,
      name: "White Heels",
      price: 25,
      rating: 5,
      image: "src/assets/images/sample-5.png",
      sold: "1000+ sold",
      shipping: "Free Shipping",
    },
  ];

  return (
    <>
      <div className="min-h-screen flex flex-col bg-white">
        {/* Navigation */}
        <Navbar/>

        {/* Main Content */}

        <main className="flex-grow container mx-auto px-4 py-8">
          <h1 className="text-2xl font-bold font-poppins mb-6 mt-[60px]">My Wishlist</h1>

          {/* Wishlist Items */}
          <div className="mb-8 ">
            <div className="py-4 flex justify-between items-center">
            
              <label className="flex items-center font-[Open_Sans] " >
                <input type="radio" className="bg-[#530993] w-[26px] h-[26px] mr-[11px]"></input>
                <span>Select All</span>
              </label>
              <div className="flex items-center font-[Open_Sans]">
                <button className="text-black text-[16px] mr-4 flex items-center gap-[6px]">
                  <IoMdHeart className="w-[33px] h-[33px] text-[#530993]" />{" "}
                  Move to WishList
                </button>
                <button className="text-black flex items-center gap-[6px]">
                  <FaTrash className="w-[27px] h-[27px] text-[#530993]" />{" "}
                  Delete selected items
                </button>
              </div>
            </div>

            {wishlistItems.map((item) => (
              <WishlistItem key={item.id} item={item} />
            ))}

            <div className="flex justify-center my-8">
              <button className="border-[2px] border-purple-900 text-purple-900 w-[307px] h-[68px] font-inter px-6 py-2 rounded-md flex items-center justify-center gap-4 hover:shadow-[4px_4px_0px_0px_rgba(91,20,153,1)] transition-shadow duration-300 ease-in-out">
                <PiShoppingCartSimpleFill className="w-[40px] h-[40px]" />
                <span className="text-[24px]">View Cart</span>
              </button>
            </div>
          </div>
          <div className="border-t-2 border-purple-900 rounded-full"></div>

          {/* Recommended Items */}
          <div className="relative mb-4 ">
            <h2 className="text-xl  font-semibold font-poppins mt-[60px] mb-[96px] relative z-10 text-[24px] ">
              Items you may want to add to your Wishlist
            </h2>
            <div className="absolute inset-x-0 top-0  z-0"></div>
            <div className="relative z-10">
              <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-10 mb-8">
                {recommendedItems.map((item) => (
                  <div
                    key={item.id}
                    className="bg-white rounded-lg shadow-lg overflow-hidden w-[251px] h-[390px]"
                  >
                    <div className="relative w-[251px] h-[251px]">
                      <img
                        src={item.image}
                        alt={item.name}
                        className="w-full h-full object-cover"
                      />
                      <HeartButton /> {/* Use the HeartButton component here */}
                    </div>

                    <div className="p-4">
                      <h3 className="font-regular font-poppins text-[16px] mb-1">
                        {item.name}
                      </h3>
                      <div className="flex items-center mb-2">
                        {[...Array(5)].map((_, i) => (
                          <span
                            key={i}
                            className={`${
                              i < item.rating
                                ? "text-yellow-400"
                                : "text-gray-300"
                            }`}
                          >
                            ★
                          </span>
                        ))}
                        <p className="ml-[77.5px] font-[Open_Sans] text-[14px]">1000+ sold</p>
                      </div>
                      <div className="flex items-center gap-[86px]">
                        <p className="text-[14px] mb-2 font-[Open_Sans]">Free Shipping</p>
                        <p className=" font-semibold text-[24px] font-[Open_Sans] ">${item.price}</p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
              <div className="flex justify-center">
                <button className="bg-[#7C2CCC] flex items-center w-[161px] h-[45px] text-[21px] px-[36px] py-[12px] text-white shadow-md  rounded-[8px] hover:bg-[#420075]">
                  See More
                </button>
              </div>
            </div>
          </div>
        </main>
        <FooterProductDetails/>
      </div>
    </>
  );
};

export default WishlistPage;
