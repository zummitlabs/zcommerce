import React, { useEffect, useState } from 'react';

const TrendingCategories = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    const fetchCategories = async () => {
      const response = await fetch('https://fakestoreapi.com/products/categories');
      const categories = await response.json();

      const categoriesWithImages = await Promise.all(categories.map(async (category) => {
        const res = await fetch(`https://fakestoreapi.com/products/category/${category}`);
        const products = await res.json();
        return {
          category,
          image: products[0].image,
        };
      }));

      setCategories(categoriesWithImages);
    };

    fetchCategories();
  }, []);

  return (
    <div className="">
      <h2 className="text-2xl font-[600] font-poppins mb-2 mt-2 ml-4">Trending Categories For You</h2>
      <div className="flex justify-center space-x-44 py-[4px]"
         style={{
            backgroundImage: 'linear-gradient(#fff, #fff), linear-gradient(to right, #800080, #FFD700)',
            backgroundOrigin: 'border-box',
            backgroundClip: 'content-box, border-box',
          }}
      >
      
        {categories.map((cat) => (
          <div
            key={cat.category}
            className="border-4 border-transparent rounded-md p-4 text-center"
          >
            <img src={cat.image} alt={cat.category} className="h-40 w-40 object-fill mx-auto mb-2" />
            <h3 className="text-lg font-semibold">{cat.category.charAt(0).toUpperCase() + cat.category.slice(1)}</h3>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TrendingCategories;
