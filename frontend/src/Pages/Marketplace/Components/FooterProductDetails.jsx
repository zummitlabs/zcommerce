import React from 'react';
import qr from '../../../assets/images/qr.png';

function FooterProductDetails() {
  const linkClasses = "hover:underline cursor-pointer";
  
  const sections = [
    {
      title: "Company info",
      links: [
        { text: "About zCommerce", href: "/about-zcommerce" },
        { text: "Contact us", href: "/contact-us" },
        { text: "About Us", href: "/about-us" },
        { text: "Become a seller", href: "/become-a-seller" },
        { text: "Copyright protection", href: "/copyright-protection" },
        { text: "Product safety", href: "/product-safety" },
      ]
    },
    {
      title: "Customer service",
      links: [
        { text: "Return and refund policy", href: "/return-refund-policy" },
        { text: "Intellectual property policy", href: "/intellectual-property-policy" },
        { text: "Shipping info", href: "/shipping-info" },
        { text: "Report suspicious activity", href: "/report-suspicious-activity" },
      ]
    },
    {
      title: "Help",
      links: [
        { text: "Support Center & FAQ", href: "/support-center-faq" },
        { text: "zCommerce purchase protection", href: "/zcommerce-purchase-protection" },
        { text: "Sitemap", href: "/sitemap" },
        { text: "Partner with zCommerce", href: "/partner-with-zcommerce" },
      ]
    }
  ];

  return (
    <>
      <footer className="bg-custom-gradient text-white py-24 pb-32 px-28">
        <div className="container mx-auto grid grid-cols-1 md:grid-cols-4 gap-8 relative">
          {sections.map((section, index) => (
            <div key={index}>
              <h3 className="font-poppins font-semibold text-xl mb-4">{section.title}</h3>
              <ul className="font-open-sans text-sm space-y-2">
                {section.links.map((link, linkIndex) => (
                  <li key={linkIndex}>
                    <a href={link.href} className={linkClasses}>{link.text}</a>
                  </li>
                ))}
              </ul>
            </div>
          ))}
          <div className="bg-white text-black p-4 rounded-lg absolute right-0 max-w-56">
            <h3 className="font-poppins font-semibold text-lg mb-2 text-center">More deals and offers on our app!</h3>
            <img src={qr} alt="QR Code" className="w-32 h-32 mx-auto" />
            <p className="font-open-sans text-sm text-center mt-2">Scan the code to download</p>
          </div>
        </div>
      </footer>
      <div className="text-center w-full font-open-sans text-sm bg-white text-black py-2">
        Copyright 2024 ZCommerce. All rights reserved.
      </div>
    </>
  );
}

export default FooterProductDetails;