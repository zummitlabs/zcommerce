import { Link } from "react-router-dom"



function Footer() {

    return (
        <>

            <footer className="text-black h-80 py-10 " >
                <div className="container md:px-8 lg:px-16 text-black overflow-x-visible " >
                    <div className="grid grid-cols-1 lg:grid-cols-3 " >
                        <div className="ml-8" >
                            <div className="text-black" >

                                <h5 className="text-lg  font-open-sans font-semibold mb-4"> Company Information  </h5>
                                <ul className="text-black  font-open-sans  text-l ">
                                    <li className="mb-2">
                                        <Link to="/" className="hover:underline font-open-sans"> ZCommerce Shop </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans"> My account </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans "> About Us </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans "> Privacy Policy  </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans "> Terms and Conditions  </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans"> Cookie Policy  </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="ml-8">
                            <div className="text-black" >

                                <h5 className="text-lg font-semibold font-open-sans mb-4"> Customer service  </h5>
                                <ul className="text-black  font-open-sans  text-l ">
                                    <li className="mb-2">
                                        <Link to="/" className="hover:underline font-open-sans "> Shipping information </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans "> My favorites </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans"> Warranty and Return </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans"> Payment Methods  </Link>
                                    </li>
                                    <li className="mb-2 mt-3 ">
                                        <Link to="/ " className="hover:underline font-open-sans "> FAQ and Support  </Link>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div className="ml-8">
                            <div className="text-black" >
                                <h1 className="font-semibold text-2xl">Sign Up for weekly NewsLetter</h1>
                                <h3 className="mt-5" > Signup to newsletter to get coupons and discount promotion. </h3>
                                <div className="flex items-center justify-center mt-5 ">
                                    <input
                                        type="email"
                                        placeholder="Enter your email"
                                        className="p-2 border border-purple-600 rounded-l-md focus:outline-none "
                                    />
                                    <button className="p-2 bg-purple-800 text-white rounded-r-md hover:bg-purple-700 focus:outline-none">
                                        Subscribe
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </footer>
             



        </>
    )
}
export default Footer