/* eslint-disable react/prop-types */
import SimilarCard from "./SimilarCard";

const products = [
  {
    id: 1,
    name: "Gold Pearl Earrings",
    price: 2,
    sold: "1,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/1259/2515/9fceb62f5ca106f0f3322dca72e4f029?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=myaNJfqk3GAuYWy-~hTPKNVxGUfzsu8IWIERmFLaFz85rVzg1XGfakIKrwTh73yNgzV2DLIRaS8Z9p6W5bAWi8Nli~UNNQu-aIqz9ueMzxT3iZKUv4X-q1NKMKtvrEFZFfzSFII~LSMNAvwr5GG~GM~DxGj8-adpTAWhWdvgEvwGwCUOcxVkhgTYaXylNgjDfvUqkM2NZ~i7jR8NBwR3ZQl63xF0-VjLaCTzVOGybbE0zOVmCyb-cByv4ASccL32Za60q4s~w3qMTIIqh1kFwC9b6peRgo~-rWsRgDjlMEimhiKq9q6blXDPW79itMJxlHjwE9B6CwLepc8eW~nt5Q__",
    rating: 5,
  },
  {
    id: 2,
    name: "Set of 2 - Round Sunglasses",
    price: 2,
    sold: "1,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/5719/8c03/a7df260af68ba3f7e07591b78474fc79?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=qwUIlJf0w0kJbwx1tdB2MY4wlpgJcQDfGWnLFM0EMidhzYeIvar9rOD3a8YD6vAjyrFmbcSfpxh~M8u6le9D7KxrgdVk7E8k3j26sC0C-e5a-qFGykDH2ERdfH1hZ1AkuRnybhuSSy5sKpNdXWIA26Exbl0ofm~lSKVZAInFT5gTJyJQb7QCnxQEUeBoTA-ndk6CkDh9EWxH-pWHqErYYxcvt~fMSUR9r8~yuX5KVzLuBQ61gMGL9Pwmrz-yJ87Z7I4lQ5sbeqjNJZ9ww4aHn54QMZgqdYvlU1uY1V6-war5NoZMu00ALZ9F9EhxrkLehgJFgPnDy1lZM78ps7t8qQ__",
    rating: 5,
  },
  {
    id: 3,
    name: "White Plain T-Shirt",
    price: 2,
    sold: "3,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/14f3/9ef7/e02e35ac2fbb37bfc2027f90c64a0961?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=lDzk7ENiFpRanzqi89Lk--KksTvmDlvqJNeHIUB~heMz~vS2QgaEwIkmRl1AnzPrMhS1g2ZU4y~uiU5yfl0txQOqNZHHmxpQ-kSqmNfYJQAaN2gNFd8Lw6G7o2T8AQONXEYN9yJytYZqGWPhhgW67CCCe9hYAQ1e15~DarIsYCXYzQfRS7noFfHvJAtkWJWXSpxj4kiJgZIUr~gW-cUbF0ODtk8z6s9te~lRcMZM~D9kswQCHS80OehJFmRGONcoA~WqfzW3E0gR545RlUtEk~z01CRkG-VlbliC9nuMgH6We-qp1M9AJdDvPDYZqgT4JqeYVBEVzduEC73JZs0KjA__",
    rating: 5,
  },
  {
    id: 4,
    name: "Pearl Rose Pendant ",
    price: 2,
    sold: "1,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/27b4/8930/8cac140b35db794430d05ea97e1bcce3?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=O6boAVluC0HR4kDH4DkqLLx2-MjCQLffecCvxo5kzRXC6Om-dylSZOw47SPvofJoKvM0b750x2MwrZEvZ6gVNAGow6NAtC7qv-0~lXlL6ttLHnzr3t~9G0QOUvmE9zmEP~IuIYNvZgoeHRsZpl0qZejimSLEb4Yv0kn6sq-ZIaEHzCxbN4lJyRGtP~4B-2FjhYNYq7kb6Et0z3CoOXNlWN5lmCbw6A2HOM8SGsT-1Z02YXU0yPYqLK-85iSLbY3I51ncTPSy2KdVcMJL~~nX8MIy49Ou9U8wy2dr-51WZX4Fw63nqD77IIl4ZrmMwBUiTxqeoH2k-1a84vLuy57stg__",
    rating: 5,
  },
  {
    id: 5,
    name: "Pearl Necklace",
    price: 2,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/9670/7c88/3bd330748c8fae699dcc4dc1162018d5?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=cMbeqv9k57vbNt9Z2sDDPcoSGrsojADcYMtKXpD6iPEQmRCdI5I5fFjskYv6RKaiKvAUOgIeOMGq4VRekPC9C2GR5PoHrWN1xCYpcVFtAqiWlTkp08pSXssu7MOhfXNqfyhy4RJRiRsrysyDw55WjembEsbfEDAoLH3VntoXNB8MXp5HIs~Js5sI1sk0zyDazgcgnZ9PqXenR6vu-ohGKSASCmQWnv1zzRZxu5nq63bAaYkIl4N2JKL8SqxhaaJ1yU~Lz0A22ysPWb0Aq4pcV-AnRVP2m~ilb~-M8FJZiyGjOzKJ-qMm-bJAnw3zZ54vVNXz8eU-Irbdaqy5ciEBWQ__",
    rating: 5,
  },
  {
    id: 6,
    name: "White Denim Jacket",
    price: 999,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/8e4e/12b9/b6041edf0c2c1760498fb6ca70f6cd4e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vcpv6PZwdCIJDNJxgNkdKFEkr4WaPdMriLv08gFYhzZYjKRpXjshseRGe1mmCeExGSzxyBAjcWCqM3vQhD07zjYMf3ZjVE7rbtFInRLgcDiw6piJAA2szV20Zu4rUFaqRDDWS-UN9fHBrJaE9Shi41Uq9AB7Sbxk0SL1cSTHUMH1Pe0Q1jEp37QCdA-rFJ3IuIu8bu5-BezB~Pqv7PwFr9qVUJZjeRVNUcxsd80zv97EQ5e21mQM89bj1JN6nG6PnsxeFNGqvISEMd1n5iucMLVhrqAZtX~HI4z5N8qtZ9pD8zjrKMOYei~7zYJvAHB0TEtTfweL01s7ez7kHSegUQ__",
    rating: 5,
  },
  {
    id: 1,
    name: "Running Shoes",
    price: 1200,
    sold: "1,200+",
    image:
      "https://s3-alpha-sig.figma.com/img/746b/7534/68dbfc09dcf10d217bdd071ac8c6cd1e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vh~C917fbKayVj2VyoAkbLdOYyw3Q1MWSzReyCtj-d9UezpDyefOxaa5rmh51hNcvJ7cGoI9yo3vSwl6U2MG4IivMfzRejdRH6KStXONZ5Z2s8TiDZRc1~gf-JbF~O8xGvXqCNLLTIC40Ych-qo8GolTH~fEavCCwwjNvVLyHQiKvHBKaqcYQJ3pd1LoXCmRjOrALsKKxjxQcO5GtejV2Qcs5YuxzQI3LoBuUQu2wZ~zWzFzZhWjdlqVjejf2ar7T4g8k~N2q6i0bSj8OVrYf6~8llGv5G-ze4oJjPPY4-~iEWslQFhpGNT-BIH-X-X3N5GcYU6dMDRKiZ56Bm9Zsg__",
    rating: 5,
  },
  {
    id: 2,
    name: "Blue Stone Earrings",
    price: 600,
    sold: "1,200+",
    image:
      "https://s3-alpha-sig.figma.com/img/0826/cccb/d964549ca905a20996849ea69008b77b?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=K9Fk7zs0KBtYkzoJt0dVJH1uOtPhvQqPWD2m5-56N1s3r0kEynb7XZ79KN4VhRT9RL-E2y~zix0eyPylhfUFLiaQ2Utbx2uU0Ifg7zPjWIHYp98KKOSWqOHGVmsyAXsV7BO17k6wnkDtATIMPG2a33FSf1Ap44IjP39wqy28C0prPfZrZ4OhKMvrWdP7ak3xBf3zOv~OOKvFR5TNt4Wby5xYak-5nQNPaAwXEUk74-tIorc0VwHpEatMP4ASwdzyOnwXSbhDW7Qm1Na09yY-a6027HfLwcwoUySAf8UCSpTmpZkm1ttTXFnzdCYPFtS5vGkiXP-uCrbFJspppPHIwg__",
    rating: 5,
  },
  {
    id: 3,
    name: "Shampoo Travel Bottle",
    price: 400,
    sold: "3,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/f167/2a0a/f4b42e3e5fd81dbec901007a5962a5e3?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=m~driQDDzdki3XiSbJlk0mzJM0LRpId7qRaoj~oleV-YczXxKjuEwA2Fv222E8jt6~D02uaf55A9iP52Uu-~YsvnvChKOlf0j1aogMuvumNXsUBH15YjiPHK9iG3Qyi-1KjbYOvQ9NOtYatN7mxMDkS7LM2LlaDQKCY7j~VitgODUiIs6napwG2DSYMNdmcrl9b5EVyeGaoIZocR3Gv4BTpJsuSeK7W4N8XWJKu3N726ogLxcxdyTFkwsB0TP-oAkNHC-81zZHxH91GWXTM2o~AEj3oFBYUOzoQmw8JLQG6bHVvJQZr~xbOh9blAhqHNzD7ly~bw8ZRY39Sxt0L3KA__",
    rating: 5,
  },
  {
    id: 4,
    name: "Silver Pendant",
    price: 2500,
    sold: "1,500+",
    image:
      "https://s3-alpha-sig.figma.com/img/fbed/c331/1548976b3a3c18445525a1aaa497ebaa?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=XaeUjZR~1PsSZmszv1UsSIS950feLki4ao6M30gnrOxJlJgbWVEMbB~Ea0g3mi8tELk-b-L4pH-aTTNPDYkh3bvtLypoTvCiV~Dccb8MYPHJUhGtvzrjZ95b0~sNX1a7jHRKjg9sIG5iPwODDiW0FSQywrfVdeQkhApfAKjS-dlzNmlQgKDPIWgX3pF5gJaPaFJ6EMkROuyvNy-NsJgmQC7XSOOQP-N9G9rLIy315rJvKxntBaBzwxqGFtnGWLOJxIglqvJsSHPTaTv2HbVe29miPxcy3AWDa2Fqv9oBRTysHPmWnxkbUPgZw9cXNlyadBy578NdsxNwLzdqt9SfNA__",
    rating: 5,
  },
  {
    id: 5,
    name: "Black Bucket Hat",
    price: 499,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/a7ee/3597/efa5e5a07659752bfed6d79e0d5d904e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=WKYXUGcjU2e7R78oFF2-GQzh7S9DiNQHI1N19xULldMWYAaHA5bcQdTGkL93ErlFFYYFGV7GGu34d3U43B6RMQdK72xkcCRkrMMcu8-S6klkfQ3NlGkYKpI85bCLOiA5MrnLs77vqghUqo7-EykjD9jjD46ySJH21tg~ahnQJAW~NMIrhVQjl3s7f5jBndxp49ANoaOooaSPhWxwrS6fU0B0Hc3WhToqUi7BEHTg85nr0oaFYOwKnbbrT4OkBCnOte5OeCmp10ibru1DSvGf-n1pmHcREBUvhPTcpIwekv8aKoLBQ-BYa38uKDm544goMmyyJ6hriY5VVzHCN7CSxw__",
    rating: 5,
  },
  {
    id: 6,
    name: "White Denim Jacket",
    price: 999,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/8e4e/12b9/b6041edf0c2c1760498fb6ca70f6cd4e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vcpv6PZwdCIJDNJxgNkdKFEkr4WaPdMriLv08gFYhzZYjKRpXjshseRGe1mmCeExGSzxyBAjcWCqM3vQhD07zjYMf3ZjVE7rbtFInRLgcDiw6piJAA2szV20Zu4rUFaqRDDWS-UN9fHBrJaE9Shi41Uq9AB7Sbxk0SL1cSTHUMH1Pe0Q1jEp37QCdA-rFJ3IuIu8bu5-BezB~Pqv7PwFr9qVUJZjeRVNUcxsd80zv97EQ5e21mQM89bj1JN6nG6PnsxeFNGqvISEMd1n5iucMLVhrqAZtX~HI4z5N8qtZ9pD8zjrKMOYei~7zYJvAHB0TEtTfweL01s7ez7kHSegUQ__",
    rating: 5,
  },
  {
    id: 1,
    name: "Running Shoes",
    price: 1200,
    sold: "1,200+",
    image:
      "https://s3-alpha-sig.figma.com/img/746b/7534/68dbfc09dcf10d217bdd071ac8c6cd1e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vh~C917fbKayVj2VyoAkbLdOYyw3Q1MWSzReyCtj-d9UezpDyefOxaa5rmh51hNcvJ7cGoI9yo3vSwl6U2MG4IivMfzRejdRH6KStXONZ5Z2s8TiDZRc1~gf-JbF~O8xGvXqCNLLTIC40Ych-qo8GolTH~fEavCCwwjNvVLyHQiKvHBKaqcYQJ3pd1LoXCmRjOrALsKKxjxQcO5GtejV2Qcs5YuxzQI3LoBuUQu2wZ~zWzFzZhWjdlqVjejf2ar7T4g8k~N2q6i0bSj8OVrYf6~8llGv5G-ze4oJjPPY4-~iEWslQFhpGNT-BIH-X-X3N5GcYU6dMDRKiZ56Bm9Zsg__",
    rating: 5,
  },
  {
    id: 2,
    name: "Blue Stone Earrings",
    price: 600,
    sold: "1,200+",
    image:
      "https://s3-alpha-sig.figma.com/img/0826/cccb/d964549ca905a20996849ea69008b77b?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=K9Fk7zs0KBtYkzoJt0dVJH1uOtPhvQqPWD2m5-56N1s3r0kEynb7XZ79KN4VhRT9RL-E2y~zix0eyPylhfUFLiaQ2Utbx2uU0Ifg7zPjWIHYp98KKOSWqOHGVmsyAXsV7BO17k6wnkDtATIMPG2a33FSf1Ap44IjP39wqy28C0prPfZrZ4OhKMvrWdP7ak3xBf3zOv~OOKvFR5TNt4Wby5xYak-5nQNPaAwXEUk74-tIorc0VwHpEatMP4ASwdzyOnwXSbhDW7Qm1Na09yY-a6027HfLwcwoUySAf8UCSpTmpZkm1ttTXFnzdCYPFtS5vGkiXP-uCrbFJspppPHIwg__",
    rating: 5,
  },
  {
    id: 3,
    name: "Shampoo Travel Bottle",
    price: 400,
    sold: "3,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/f167/2a0a/f4b42e3e5fd81dbec901007a5962a5e3?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=m~driQDDzdki3XiSbJlk0mzJM0LRpId7qRaoj~oleV-YczXxKjuEwA2Fv222E8jt6~D02uaf55A9iP52Uu-~YsvnvChKOlf0j1aogMuvumNXsUBH15YjiPHK9iG3Qyi-1KjbYOvQ9NOtYatN7mxMDkS7LM2LlaDQKCY7j~VitgODUiIs6napwG2DSYMNdmcrl9b5EVyeGaoIZocR3Gv4BTpJsuSeK7W4N8XWJKu3N726ogLxcxdyTFkwsB0TP-oAkNHC-81zZHxH91GWXTM2o~AEj3oFBYUOzoQmw8JLQG6bHVvJQZr~xbOh9blAhqHNzD7ly~bw8ZRY39Sxt0L3KA__",
    rating: 5,
  },
  {
    id: 4,
    name: "Silver Pendant",
    price: 2500,
    sold: "1,500+",
    image:
      "https://s3-alpha-sig.figma.com/img/fbed/c331/1548976b3a3c18445525a1aaa497ebaa?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=XaeUjZR~1PsSZmszv1UsSIS950feLki4ao6M30gnrOxJlJgbWVEMbB~Ea0g3mi8tELk-b-L4pH-aTTNPDYkh3bvtLypoTvCiV~Dccb8MYPHJUhGtvzrjZ95b0~sNX1a7jHRKjg9sIG5iPwODDiW0FSQywrfVdeQkhApfAKjS-dlzNmlQgKDPIWgX3pF5gJaPaFJ6EMkROuyvNy-NsJgmQC7XSOOQP-N9G9rLIy315rJvKxntBaBzwxqGFtnGWLOJxIglqvJsSHPTaTv2HbVe29miPxcy3AWDa2Fqv9oBRTysHPmWnxkbUPgZw9cXNlyadBy578NdsxNwLzdqt9SfNA__",
    rating: 5,
  },
  {
    id: 5,
    name: "Black Bucket Hat",
    price: 499,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/a7ee/3597/efa5e5a07659752bfed6d79e0d5d904e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=WKYXUGcjU2e7R78oFF2-GQzh7S9DiNQHI1N19xULldMWYAaHA5bcQdTGkL93ErlFFYYFGV7GGu34d3U43B6RMQdK72xkcCRkrMMcu8-S6klkfQ3NlGkYKpI85bCLOiA5MrnLs77vqghUqo7-EykjD9jjD46ySJH21tg~ahnQJAW~NMIrhVQjl3s7f5jBndxp49ANoaOooaSPhWxwrS6fU0B0Hc3WhToqUi7BEHTg85nr0oaFYOwKnbbrT4OkBCnOte5OeCmp10ibru1DSvGf-n1pmHcREBUvhPTcpIwekv8aKoLBQ-BYa38uKDm544goMmyyJ6hriY5VVzHCN7CSxw__",
    rating: 5,
  },
  {
    id: 6,
    name: "White Denim Jacket",
    price: 999,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/8e4e/12b9/b6041edf0c2c1760498fb6ca70f6cd4e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vcpv6PZwdCIJDNJxgNkdKFEkr4WaPdMriLv08gFYhzZYjKRpXjshseRGe1mmCeExGSzxyBAjcWCqM3vQhD07zjYMf3ZjVE7rbtFInRLgcDiw6piJAA2szV20Zu4rUFaqRDDWS-UN9fHBrJaE9Shi41Uq9AB7Sbxk0SL1cSTHUMH1Pe0Q1jEp37QCdA-rFJ3IuIu8bu5-BezB~Pqv7PwFr9qVUJZjeRVNUcxsd80zv97EQ5e21mQM89bj1JN6nG6PnsxeFNGqvISEMd1n5iucMLVhrqAZtX~HI4z5N8qtZ9pD8zjrKMOYei~7zYJvAHB0TEtTfweL01s7ez7kHSegUQ__",
    rating: 5,
  },

  {
    id: 1,
    name: "Gold Pearl Earrings",
    price: 2,
    sold: "1,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/1259/2515/9fceb62f5ca106f0f3322dca72e4f029?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=myaNJfqk3GAuYWy-~hTPKNVxGUfzsu8IWIERmFLaFz85rVzg1XGfakIKrwTh73yNgzV2DLIRaS8Z9p6W5bAWi8Nli~UNNQu-aIqz9ueMzxT3iZKUv4X-q1NKMKtvrEFZFfzSFII~LSMNAvwr5GG~GM~DxGj8-adpTAWhWdvgEvwGwCUOcxVkhgTYaXylNgjDfvUqkM2NZ~i7jR8NBwR3ZQl63xF0-VjLaCTzVOGybbE0zOVmCyb-cByv4ASccL32Za60q4s~w3qMTIIqh1kFwC9b6peRgo~-rWsRgDjlMEimhiKq9q6blXDPW79itMJxlHjwE9B6CwLepc8eW~nt5Q__",
    rating: 5,
  },
  {
    id: 2,
    name: "Set of 2 - Round Sunglasses",
    price: 2,
    sold: "1,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/5719/8c03/a7df260af68ba3f7e07591b78474fc79?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=qwUIlJf0w0kJbwx1tdB2MY4wlpgJcQDfGWnLFM0EMidhzYeIvar9rOD3a8YD6vAjyrFmbcSfpxh~M8u6le9D7KxrgdVk7E8k3j26sC0C-e5a-qFGykDH2ERdfH1hZ1AkuRnybhuSSy5sKpNdXWIA26Exbl0ofm~lSKVZAInFT5gTJyJQb7QCnxQEUeBoTA-ndk6CkDh9EWxH-pWHqErYYxcvt~fMSUR9r8~yuX5KVzLuBQ61gMGL9Pwmrz-yJ87Z7I4lQ5sbeqjNJZ9ww4aHn54QMZgqdYvlU1uY1V6-war5NoZMu00ALZ9F9EhxrkLehgJFgPnDy1lZM78ps7t8qQ__",
    rating: 5,
  },
  {
    id: 3,
    name: "White Plain T-Shirt",
    price: 2,
    sold: "3,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/14f3/9ef7/e02e35ac2fbb37bfc2027f90c64a0961?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=lDzk7ENiFpRanzqi89Lk--KksTvmDlvqJNeHIUB~heMz~vS2QgaEwIkmRl1AnzPrMhS1g2ZU4y~uiU5yfl0txQOqNZHHmxpQ-kSqmNfYJQAaN2gNFd8Lw6G7o2T8AQONXEYN9yJytYZqGWPhhgW67CCCe9hYAQ1e15~DarIsYCXYzQfRS7noFfHvJAtkWJWXSpxj4kiJgZIUr~gW-cUbF0ODtk8z6s9te~lRcMZM~D9kswQCHS80OehJFmRGONcoA~WqfzW3E0gR545RlUtEk~z01CRkG-VlbliC9nuMgH6We-qp1M9AJdDvPDYZqgT4JqeYVBEVzduEC73JZs0KjA__",
    rating: 5,
  },
  {
    id: 4,
    name: "Pearl Rose Pendant ",
    price: 2,
    sold: "1,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/27b4/8930/8cac140b35db794430d05ea97e1bcce3?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=O6boAVluC0HR4kDH4DkqLLx2-MjCQLffecCvxo5kzRXC6Om-dylSZOw47SPvofJoKvM0b750x2MwrZEvZ6gVNAGow6NAtC7qv-0~lXlL6ttLHnzr3t~9G0QOUvmE9zmEP~IuIYNvZgoeHRsZpl0qZejimSLEb4Yv0kn6sq-ZIaEHzCxbN4lJyRGtP~4B-2FjhYNYq7kb6Et0z3CoOXNlWN5lmCbw6A2HOM8SGsT-1Z02YXU0yPYqLK-85iSLbY3I51ncTPSy2KdVcMJL~~nX8MIy49Ou9U8wy2dr-51WZX4Fw63nqD77IIl4ZrmMwBUiTxqeoH2k-1a84vLuy57stg__",
    rating: 5,
  },
  {
    id: 5,
    name: "Pearl Necklace",
    price: 2,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/9670/7c88/3bd330748c8fae699dcc4dc1162018d5?Expires=1722816000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=cMbeqv9k57vbNt9Z2sDDPcoSGrsojADcYMtKXpD6iPEQmRCdI5I5fFjskYv6RKaiKvAUOgIeOMGq4VRekPC9C2GR5PoHrWN1xCYpcVFtAqiWlTkp08pSXssu7MOhfXNqfyhy4RJRiRsrysyDw55WjembEsbfEDAoLH3VntoXNB8MXp5HIs~Js5sI1sk0zyDazgcgnZ9PqXenR6vu-ohGKSASCmQWnv1zzRZxu5nq63bAaYkIl4N2JKL8SqxhaaJ1yU~Lz0A22ysPWb0Aq4pcV-AnRVP2m~ilb~-M8FJZiyGjOzKJ-qMm-bJAnw3zZ54vVNXz8eU-Irbdaqy5ciEBWQ__",
    rating: 5,
  },
  {
    id: 6,
    name: "White Denim Jacket",
    price: 999,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/8e4e/12b9/b6041edf0c2c1760498fb6ca70f6cd4e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vcpv6PZwdCIJDNJxgNkdKFEkr4WaPdMriLv08gFYhzZYjKRpXjshseRGe1mmCeExGSzxyBAjcWCqM3vQhD07zjYMf3ZjVE7rbtFInRLgcDiw6piJAA2szV20Zu4rUFaqRDDWS-UN9fHBrJaE9Shi41Uq9AB7Sbxk0SL1cSTHUMH1Pe0Q1jEp37QCdA-rFJ3IuIu8bu5-BezB~Pqv7PwFr9qVUJZjeRVNUcxsd80zv97EQ5e21mQM89bj1JN6nG6PnsxeFNGqvISEMd1n5iucMLVhrqAZtX~HI4z5N8qtZ9pD8zjrKMOYei~7zYJvAHB0TEtTfweL01s7ez7kHSegUQ__",
    rating: 5,
  },
  {
    id: 1,
    name: "Running Shoes",
    price: 1200,
    sold: "1,200+",
    image:
      "https://s3-alpha-sig.figma.com/img/746b/7534/68dbfc09dcf10d217bdd071ac8c6cd1e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vh~C917fbKayVj2VyoAkbLdOYyw3Q1MWSzReyCtj-d9UezpDyefOxaa5rmh51hNcvJ7cGoI9yo3vSwl6U2MG4IivMfzRejdRH6KStXONZ5Z2s8TiDZRc1~gf-JbF~O8xGvXqCNLLTIC40Ych-qo8GolTH~fEavCCwwjNvVLyHQiKvHBKaqcYQJ3pd1LoXCmRjOrALsKKxjxQcO5GtejV2Qcs5YuxzQI3LoBuUQu2wZ~zWzFzZhWjdlqVjejf2ar7T4g8k~N2q6i0bSj8OVrYf6~8llGv5G-ze4oJjPPY4-~iEWslQFhpGNT-BIH-X-X3N5GcYU6dMDRKiZ56Bm9Zsg__",
    rating: 5,
  },
  {
    id: 2,
    name: "Blue Stone Earrings",
    price: 600,
    sold: "1,200+",
    image:
      "https://s3-alpha-sig.figma.com/img/0826/cccb/d964549ca905a20996849ea69008b77b?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=K9Fk7zs0KBtYkzoJt0dVJH1uOtPhvQqPWD2m5-56N1s3r0kEynb7XZ79KN4VhRT9RL-E2y~zix0eyPylhfUFLiaQ2Utbx2uU0Ifg7zPjWIHYp98KKOSWqOHGVmsyAXsV7BO17k6wnkDtATIMPG2a33FSf1Ap44IjP39wqy28C0prPfZrZ4OhKMvrWdP7ak3xBf3zOv~OOKvFR5TNt4Wby5xYak-5nQNPaAwXEUk74-tIorc0VwHpEatMP4ASwdzyOnwXSbhDW7Qm1Na09yY-a6027HfLwcwoUySAf8UCSpTmpZkm1ttTXFnzdCYPFtS5vGkiXP-uCrbFJspppPHIwg__",
    rating: 5,
  },
  {
    id: 3,
    name: "Shampoo Travel Bottle",
    price: 400,
    sold: "3,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/f167/2a0a/f4b42e3e5fd81dbec901007a5962a5e3?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=m~driQDDzdki3XiSbJlk0mzJM0LRpId7qRaoj~oleV-YczXxKjuEwA2Fv222E8jt6~D02uaf55A9iP52Uu-~YsvnvChKOlf0j1aogMuvumNXsUBH15YjiPHK9iG3Qyi-1KjbYOvQ9NOtYatN7mxMDkS7LM2LlaDQKCY7j~VitgODUiIs6napwG2DSYMNdmcrl9b5EVyeGaoIZocR3Gv4BTpJsuSeK7W4N8XWJKu3N726ogLxcxdyTFkwsB0TP-oAkNHC-81zZHxH91GWXTM2o~AEj3oFBYUOzoQmw8JLQG6bHVvJQZr~xbOh9blAhqHNzD7ly~bw8ZRY39Sxt0L3KA__",
    rating: 5,
  },
  {
    id: 4,
    name: "Silver Pendant",
    price: 2500,
    sold: "1,500+",
    image:
      "https://s3-alpha-sig.figma.com/img/fbed/c331/1548976b3a3c18445525a1aaa497ebaa?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=XaeUjZR~1PsSZmszv1UsSIS950feLki4ao6M30gnrOxJlJgbWVEMbB~Ea0g3mi8tELk-b-L4pH-aTTNPDYkh3bvtLypoTvCiV~Dccb8MYPHJUhGtvzrjZ95b0~sNX1a7jHRKjg9sIG5iPwODDiW0FSQywrfVdeQkhApfAKjS-dlzNmlQgKDPIWgX3pF5gJaPaFJ6EMkROuyvNy-NsJgmQC7XSOOQP-N9G9rLIy315rJvKxntBaBzwxqGFtnGWLOJxIglqvJsSHPTaTv2HbVe29miPxcy3AWDa2Fqv9oBRTysHPmWnxkbUPgZw9cXNlyadBy578NdsxNwLzdqt9SfNA__",
    rating: 5,
  },
  {
    id: 5,
    name: "Black Bucket Hat",
    price: 499,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/a7ee/3597/efa5e5a07659752bfed6d79e0d5d904e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=WKYXUGcjU2e7R78oFF2-GQzh7S9DiNQHI1N19xULldMWYAaHA5bcQdTGkL93ErlFFYYFGV7GGu34d3U43B6RMQdK72xkcCRkrMMcu8-S6klkfQ3NlGkYKpI85bCLOiA5MrnLs77vqghUqo7-EykjD9jjD46ySJH21tg~ahnQJAW~NMIrhVQjl3s7f5jBndxp49ANoaOooaSPhWxwrS6fU0B0Hc3WhToqUi7BEHTg85nr0oaFYOwKnbbrT4OkBCnOte5OeCmp10ibru1DSvGf-n1pmHcREBUvhPTcpIwekv8aKoLBQ-BYa38uKDm544goMmyyJ6hriY5VVzHCN7CSxw__",
    rating: 5,
  },
  {
    id: 6,
    name: "White Denim Jacket",
    price: 999,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/8e4e/12b9/b6041edf0c2c1760498fb6ca70f6cd4e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vcpv6PZwdCIJDNJxgNkdKFEkr4WaPdMriLv08gFYhzZYjKRpXjshseRGe1mmCeExGSzxyBAjcWCqM3vQhD07zjYMf3ZjVE7rbtFInRLgcDiw6piJAA2szV20Zu4rUFaqRDDWS-UN9fHBrJaE9Shi41Uq9AB7Sbxk0SL1cSTHUMH1Pe0Q1jEp37QCdA-rFJ3IuIu8bu5-BezB~Pqv7PwFr9qVUJZjeRVNUcxsd80zv97EQ5e21mQM89bj1JN6nG6PnsxeFNGqvISEMd1n5iucMLVhrqAZtX~HI4z5N8qtZ9pD8zjrKMOYei~7zYJvAHB0TEtTfweL01s7ez7kHSegUQ__",
    rating: 5,
  },
  {
    id: 1,
    name: "Running Shoes",
    price: 1200,
    sold: "1,200+",
    image:
      "https://s3-alpha-sig.figma.com/img/746b/7534/68dbfc09dcf10d217bdd071ac8c6cd1e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vh~C917fbKayVj2VyoAkbLdOYyw3Q1MWSzReyCtj-d9UezpDyefOxaa5rmh51hNcvJ7cGoI9yo3vSwl6U2MG4IivMfzRejdRH6KStXONZ5Z2s8TiDZRc1~gf-JbF~O8xGvXqCNLLTIC40Ych-qo8GolTH~fEavCCwwjNvVLyHQiKvHBKaqcYQJ3pd1LoXCmRjOrALsKKxjxQcO5GtejV2Qcs5YuxzQI3LoBuUQu2wZ~zWzFzZhWjdlqVjejf2ar7T4g8k~N2q6i0bSj8OVrYf6~8llGv5G-ze4oJjPPY4-~iEWslQFhpGNT-BIH-X-X3N5GcYU6dMDRKiZ56Bm9Zsg__",
    rating: 5,
  },
  {
    id: 2,
    name: "Blue Stone Earrings",
    price: 600,
    sold: "1,200+",
    image:
      "https://s3-alpha-sig.figma.com/img/0826/cccb/d964549ca905a20996849ea69008b77b?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=K9Fk7zs0KBtYkzoJt0dVJH1uOtPhvQqPWD2m5-56N1s3r0kEynb7XZ79KN4VhRT9RL-E2y~zix0eyPylhfUFLiaQ2Utbx2uU0Ifg7zPjWIHYp98KKOSWqOHGVmsyAXsV7BO17k6wnkDtATIMPG2a33FSf1Ap44IjP39wqy28C0prPfZrZ4OhKMvrWdP7ak3xBf3zOv~OOKvFR5TNt4Wby5xYak-5nQNPaAwXEUk74-tIorc0VwHpEatMP4ASwdzyOnwXSbhDW7Qm1Na09yY-a6027HfLwcwoUySAf8UCSpTmpZkm1ttTXFnzdCYPFtS5vGkiXP-uCrbFJspppPHIwg__",
    rating: 5,
  },
  {
    id: 3,
    name: "Shampoo Travel Bottle",
    price: 400,
    sold: "3,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/f167/2a0a/f4b42e3e5fd81dbec901007a5962a5e3?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=m~driQDDzdki3XiSbJlk0mzJM0LRpId7qRaoj~oleV-YczXxKjuEwA2Fv222E8jt6~D02uaf55A9iP52Uu-~YsvnvChKOlf0j1aogMuvumNXsUBH15YjiPHK9iG3Qyi-1KjbYOvQ9NOtYatN7mxMDkS7LM2LlaDQKCY7j~VitgODUiIs6napwG2DSYMNdmcrl9b5EVyeGaoIZocR3Gv4BTpJsuSeK7W4N8XWJKu3N726ogLxcxdyTFkwsB0TP-oAkNHC-81zZHxH91GWXTM2o~AEj3oFBYUOzoQmw8JLQG6bHVvJQZr~xbOh9blAhqHNzD7ly~bw8ZRY39Sxt0L3KA__",
    rating: 5,
  },
  {
    id: 4,
    name: "Silver Pendant",
    price: 2500,
    sold: "1,500+",
    image:
      "https://s3-alpha-sig.figma.com/img/fbed/c331/1548976b3a3c18445525a1aaa497ebaa?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=XaeUjZR~1PsSZmszv1UsSIS950feLki4ao6M30gnrOxJlJgbWVEMbB~Ea0g3mi8tELk-b-L4pH-aTTNPDYkh3bvtLypoTvCiV~Dccb8MYPHJUhGtvzrjZ95b0~sNX1a7jHRKjg9sIG5iPwODDiW0FSQywrfVdeQkhApfAKjS-dlzNmlQgKDPIWgX3pF5gJaPaFJ6EMkROuyvNy-NsJgmQC7XSOOQP-N9G9rLIy315rJvKxntBaBzwxqGFtnGWLOJxIglqvJsSHPTaTv2HbVe29miPxcy3AWDa2Fqv9oBRTysHPmWnxkbUPgZw9cXNlyadBy578NdsxNwLzdqt9SfNA__",
    rating: 5,
  },
  {
    id: 5,
    name: "Black Bucket Hat",
    price: 499,
    sold: "2,000+",
    image:
      "https://s3-alpha-sig.figma.com/img/a7ee/3597/efa5e5a07659752bfed6d79e0d5d904e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=WKYXUGcjU2e7R78oFF2-GQzh7S9DiNQHI1N19xULldMWYAaHA5bcQdTGkL93ErlFFYYFGV7GGu34d3U43B6RMQdK72xkcCRkrMMcu8-S6klkfQ3NlGkYKpI85bCLOiA5MrnLs77vqghUqo7-EykjD9jjD46ySJH21tg~ahnQJAW~NMIrhVQjl3s7f5jBndxp49ANoaOooaSPhWxwrS6fU0B0Hc3WhToqUi7BEHTg85nr0oaFYOwKnbbrT4OkBCnOte5OeCmp10ibru1DSvGf-n1pmHcREBUvhPTcpIwekv8aKoLBQ-BYa38uKDm544goMmyyJ6hriY5VVzHCN7CSxw__",
    rating: 5,
  },
];

const SimilarProduct = () => {
  return (
    <div className="container mx-auto p-[42px]">
      <h2 className="text-[24px] font-semibold mb-12">Similar Products</h2>
      <div className="grid grid-cols-2  sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-[26px] ">
        {products.map((product) => (
          <SimilarCard key={product.id} product={product} />
        ))}
      </div>
    </div>
  );
};

export default SimilarProduct;
