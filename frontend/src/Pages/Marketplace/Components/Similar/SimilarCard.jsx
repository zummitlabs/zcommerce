/* eslint-disable react/prop-types */

const SimilarCard = ({ product }) => {
  if(!product){
    return null;
  }
  return (
    <div className="bg-white w-[251px] h-[360px]  gap-x-[32px] rounded-[20px] shadow-md text-center">
      <img
        src={product.image}
        alt={product.name}
        className="w-full h-[251px] object-cover rounded-tr-[20px] rounded-tl-[20px]"
      />
      <div className="w-full p-2">
        <h2 className="text-[18px] text-start font-semibold ">
          {product.name}
        </h2>
        <div className="text-[#2A2A2A]   flex justify-between text-start ">
          {"★".repeat(product.rating)}
          {"☆".repeat(5 - product.rating)}
          <p className="text-[#030303] text-[14px] mb-1 ">
            {product.sold} sold
          </p>
        </div>
        <div className="flex justify-between">
          <p className="text-[14px] content-end  font-semibold  text-end">
            Free Shipping
          </p>
          <p className="text-[21px]  font-semibold  text-end">
            {product.price}$
          </p>
        </div>
      </div>
    </div>
  );
};

export default SimilarCard;
