import React from "react";
import {
  AiOutlineMenu,
  AiOutlineSearch,
  AiOutlineShoppingCart,
} from "react-icons/ai";
import logo from "../../../assets/images/logo.png";
import { useNavigate } from "react-router-dom";

const Navbar = () => {
  return (
    <div>
      <div className="bg-gradient-to-r from-purple-900 to-purple-700 text-white flex justify-between p-1">
        <div className="flex space-x-6">
          <a href="#about" className="font-thin hover:font-semibold">
            About Us
          </a>
          <a href="#selling-center" className="font-thin hover:font-semibold">
            Selling Center
          </a>
          <a href="#start-selling" className="font-thin hover:font-semibold">
            Start Selling
          </a>
        </div>
        <div className="flex space-x-6">
          <a href="#customer-care" className="font-thin hover:font-semibold">
            Customer Care
          </a>
          <a href="#payment" className="font-thin hover:font-semibold">
            Payment
          </a>
          <a href="#shipping" className="font-thin hover:font-semibold">
            Shipping
          </a>
          <a href="#language" className="font-thin hover:font-semibold">
            EN
          </a>
        </div>
      </div>
      <Header />
    </div>
  );
};

export default Navbar;

const Header = () => {
  const navigate = useNavigate();

  return (
    <div className="bg-white text-purple-700 flex justify-between items-center px-6 shadow-md">
      <div className="flex items-center space-x-8">
        <div className="flex space-x-0 items-center">
          <img src={logo} alt="Logo" className="h-14" />
          <h1 className="font-bold text-2xl font-poppins hidden md:block text-purple-950">
            Commerce
          </h1>
        </div>
        <div className="cursor-pointer flex items-center border-2 border-purple-700 rounded-md px-2 py-1 hover:shadow-3xl hover:transition-shadow duration-100">
          <AiOutlineMenu className="text-2xl text-purple-800" />
          <span className="ml-2 font-bold font-open-sans hidden md:block">
            Categories
          </span>
        </div>
      </div>
      <div className="flex items-center space-x-4">
        <div className="flex items-center border-2 border-purple-800 rounded-full px-3 py-1">
          <input
            type="text"
            placeholder="pearl necklace..."
            className="focus:outline-none p-1 text-gray-700 font-poppins w-28 lg:w-96"
          />
          <AiOutlineSearch className="text-2xl text-purple-900" />
        </div>
      </div>
      <div className="flex items-center space-x-4">
        <button className="font-bold font-poppins cursor-pointer border-2 border-purple-700 rounded-md px-3 py-1 hover:shadow-3xl hover:transition-shadow duration-100">
          Sign Up
        </button>
        <button
          className="cursor-pointer font-bold font-open-sans bg-purple-800 text-white rounded-md px-3 py-3 flex items-center hover:bg-gradient-to-b from-purple-800 to-purple-950 hover:transition-all duration-200"
          onClick={() => navigate("/cart")}
        >
          <AiOutlineShoppingCart className="text-2xl mr-2" />
          Cart
        </button>
      </div>
    </div>
  );
};
