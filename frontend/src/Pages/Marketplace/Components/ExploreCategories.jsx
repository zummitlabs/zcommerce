import React, { useRef, useState, useEffect } from 'react';

const categories = [
  "Men’s Apparel", "Women’s Apparel", "Laptops & Computers", "Mobiles & Gadgets", "E-learning",
  "Pet Care", "Sports", "Groceries", "Jewelry", "Home Appliances", "Hobbies & Stationery",
  "Makeup & Fragrances", "Toys & Games", "Health & Personal Care"
];

const ExploreCategories = () => {
  const containerRef = useRef(null);
  const [showLeftArrow, setShowLeftArrow] = useState(false);
  const [showRightArrow, setShowRightArrow] = useState(true);

  const handleScroll = (scrollOffset) => {
    if (containerRef.current) {
      const newPosition = containerRef.current.scrollLeft + scrollOffset;
      containerRef.current.scrollTo({
        left: newPosition,
        behavior: 'smooth'
      });
    }
  };

  const updateArrowsVisibility = () => {
    if (containerRef.current) {
      const { scrollLeft, scrollWidth, clientWidth } = containerRef.current;
      setShowLeftArrow(scrollLeft > 0);
      setShowRightArrow(scrollLeft + clientWidth < scrollWidth);
    }
  };

  useEffect(() => {
    updateArrowsVisibility();
  }, []);

  return (
    <div className="relative">
      <style jsx>{`
        .scrollbar-hide {
          -ms-overflow-style: none; /* IE and Edge */
          scrollbar-width: none; /* Firefox */
        }
        .scrollbar-hide::-webkit-scrollbar {
          display: none; 
        }
      `}</style>
      <div className="font-poppins font-[600] py-4 pl-5 text-[27px] ">
        EXPLORE CATEGORIES
      </div>
      <div className="bg-custom-gradient p-6 rounded-tl-8 relative">
        <div
          className="overflow-x-auto scrollbar-hide"
          ref={containerRef}
          onScroll={updateArrowsVisibility}
        >
          <div className="grid grid-rows-2 grid-flow-col gap-6 p-5">
            {categories.map((category, index) => (
              <button key={index} className="w-[272px] h-[64px]  bg-white  font-[600] font-open-sans text-[20px] rounded-[8px] hover:bg-purple-800 hover:text-white">
                {category}
              </button>
            ))}
          </div>
        </div>
        {showLeftArrow && (
          <div className="absolute top-1/2 left-0 transform -translate-y-1/2 flex items-center">
            <button className="mr-2 p-2 rounded-full w-[40px] bg-white shadow-md" onClick={() => handleScroll(-1400)}>
              <i className="fas fa-chevron-left text-purple-800 text-xl"></i>
            </button>
          </div>
        )}
        {showRightArrow && (
          <div className="absolute top-1/2 right-0 transform -translate-y-1/2 flex items-center">
            <button className="ml-2 p-2 rounded-full w-[40px] bg-white shadow-md" onClick={() => handleScroll(1400)}>
              <i className="fas fa-chevron-right text-purple-800 text-xl"></i>
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default ExploreCategories;
