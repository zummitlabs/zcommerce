import ProductCard from "./ProductCard";

const JustForSection = () => {
  const products = [
    {
      id: 1,
      name: "Running Shoes",
      price: 1200,
      sold: "1,200+",
      image:
        "https://s3-alpha-sig.figma.com/img/746b/7534/68dbfc09dcf10d217bdd071ac8c6cd1e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vh~C917fbKayVj2VyoAkbLdOYyw3Q1MWSzReyCtj-d9UezpDyefOxaa5rmh51hNcvJ7cGoI9yo3vSwl6U2MG4IivMfzRejdRH6KStXONZ5Z2s8TiDZRc1~gf-JbF~O8xGvXqCNLLTIC40Ych-qo8GolTH~fEavCCwwjNvVLyHQiKvHBKaqcYQJ3pd1LoXCmRjOrALsKKxjxQcO5GtejV2Qcs5YuxzQI3LoBuUQu2wZ~zWzFzZhWjdlqVjejf2ar7T4g8k~N2q6i0bSj8OVrYf6~8llGv5G-ze4oJjPPY4-~iEWslQFhpGNT-BIH-X-X3N5GcYU6dMDRKiZ56Bm9Zsg__",
      rating: 5,
    },
    {
      id: 2,
      name: "Blue Stone Earrings",
      price: 600,
      sold: "1,200+",
      image:
        "https://s3-alpha-sig.figma.com/img/0826/cccb/d964549ca905a20996849ea69008b77b?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=K9Fk7zs0KBtYkzoJt0dVJH1uOtPhvQqPWD2m5-56N1s3r0kEynb7XZ79KN4VhRT9RL-E2y~zix0eyPylhfUFLiaQ2Utbx2uU0Ifg7zPjWIHYp98KKOSWqOHGVmsyAXsV7BO17k6wnkDtATIMPG2a33FSf1Ap44IjP39wqy28C0prPfZrZ4OhKMvrWdP7ak3xBf3zOv~OOKvFR5TNt4Wby5xYak-5nQNPaAwXEUk74-tIorc0VwHpEatMP4ASwdzyOnwXSbhDW7Qm1Na09yY-a6027HfLwcwoUySAf8UCSpTmpZkm1ttTXFnzdCYPFtS5vGkiXP-uCrbFJspppPHIwg__",
      rating: 5,
    },
    {
      id: 3,
      name: "Shampoo Travel Bottle",
      price: 400,
      sold: "3,000+",
      image:
        "https://s3-alpha-sig.figma.com/img/f167/2a0a/f4b42e3e5fd81dbec901007a5962a5e3?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=m~driQDDzdki3XiSbJlk0mzJM0LRpId7qRaoj~oleV-YczXxKjuEwA2Fv222E8jt6~D02uaf55A9iP52Uu-~YsvnvChKOlf0j1aogMuvumNXsUBH15YjiPHK9iG3Qyi-1KjbYOvQ9NOtYatN7mxMDkS7LM2LlaDQKCY7j~VitgODUiIs6napwG2DSYMNdmcrl9b5EVyeGaoIZocR3Gv4BTpJsuSeK7W4N8XWJKu3N726ogLxcxdyTFkwsB0TP-oAkNHC-81zZHxH91GWXTM2o~AEj3oFBYUOzoQmw8JLQG6bHVvJQZr~xbOh9blAhqHNzD7ly~bw8ZRY39Sxt0L3KA__",
      rating: 5,
    },
    {
      id: 4,
      name: "Silver Pendant",
      price: 2500,
      sold: "1,500+",
      image:
        "https://s3-alpha-sig.figma.com/img/fbed/c331/1548976b3a3c18445525a1aaa497ebaa?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=XaeUjZR~1PsSZmszv1UsSIS950feLki4ao6M30gnrOxJlJgbWVEMbB~Ea0g3mi8tELk-b-L4pH-aTTNPDYkh3bvtLypoTvCiV~Dccb8MYPHJUhGtvzrjZ95b0~sNX1a7jHRKjg9sIG5iPwODDiW0FSQywrfVdeQkhApfAKjS-dlzNmlQgKDPIWgX3pF5gJaPaFJ6EMkROuyvNy-NsJgmQC7XSOOQP-N9G9rLIy315rJvKxntBaBzwxqGFtnGWLOJxIglqvJsSHPTaTv2HbVe29miPxcy3AWDa2Fqv9oBRTysHPmWnxkbUPgZw9cXNlyadBy578NdsxNwLzdqt9SfNA__",
      rating: 5,
    },
    {
      id: 5,
      name: "Black Bucket Hat",
      price: 499,
      sold: "2,000+",
      image:
        "https://s3-alpha-sig.figma.com/img/a7ee/3597/efa5e5a07659752bfed6d79e0d5d904e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=WKYXUGcjU2e7R78oFF2-GQzh7S9DiNQHI1N19xULldMWYAaHA5bcQdTGkL93ErlFFYYFGV7GGu34d3U43B6RMQdK72xkcCRkrMMcu8-S6klkfQ3NlGkYKpI85bCLOiA5MrnLs77vqghUqo7-EykjD9jjD46ySJH21tg~ahnQJAW~NMIrhVQjl3s7f5jBndxp49ANoaOooaSPhWxwrS6fU0B0Hc3WhToqUi7BEHTg85nr0oaFYOwKnbbrT4OkBCnOte5OeCmp10ibru1DSvGf-n1pmHcREBUvhPTcpIwekv8aKoLBQ-BYa38uKDm544goMmyyJ6hriY5VVzHCN7CSxw__",
      rating: 5,
    },
    {
      id: 6,
      name: "White Denim Jacket",
      price: 999,
      sold: "2,000+",
      image:
        "https://s3-alpha-sig.figma.com/img/8e4e/12b9/b6041edf0c2c1760498fb6ca70f6cd4e?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Vcpv6PZwdCIJDNJxgNkdKFEkr4WaPdMriLv08gFYhzZYjKRpXjshseRGe1mmCeExGSzxyBAjcWCqM3vQhD07zjYMf3ZjVE7rbtFInRLgcDiw6piJAA2szV20Zu4rUFaqRDDWS-UN9fHBrJaE9Shi41Uq9AB7Sbxk0SL1cSTHUMH1Pe0Q1jEp37QCdA-rFJ3IuIu8bu5-BezB~Pqv7PwFr9qVUJZjeRVNUcxsd80zv97EQ5e21mQM89bj1JN6nG6PnsxeFNGqvISEMd1n5iucMLVhrqAZtX~HI4z5N8qtZ9pD8zjrKMOYei~7zYJvAHB0TEtTfweL01s7ez7kHSegUQ__",
      rating: 5,
    },
  ];

  return (
    <div className="min-h-screen  w-full h-auto mb-[24px]  px-[41px] cursor-pointer">
      <div className=" w-full flex flex-col justify-center">
        <div className="w-auto   h-[64px] mt-2 content-center ">
          <span className="text-2xl ml-[10px] text-start font-bold align-middle">
            JUST FOR YOU
          </span>
        </div>
        <div className="flex justify-center gap-[50px] w-full  overflow-hidden">
          <div className="bg-white h-auto rounded-lg shadow-md mb-2">
            <div className="flex flex-col lg:flex-row items-center lg:justify-between">
              <div className="w-[548px] h-[634px] ">
                <img
                  className="object-cover w-full h-[634px] rounded"
                  src="https://s3-alpha-sig.figma.com/img/ef16/3352/9bfcf76775d6594adf00264b5eec8a53?Expires=1722211200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=FRvYLA4nbgI52sb3WGFoh~63r-y6KCvvbeKuJfwV9Y6x2OxowHmY6O3WxYktDf9s7WOPF6sZG5HMn0w4fGNYbcVAem0GVP0GnHRqSF4I8oY4hIIbzd8i-s9NH4yznxY-bCBlagbEJFCp5RHEErG8pTR0V-u6zAa7zXqnurmAeWxKWxKYVMmLlmWP0NZI0CBcG-7TdHdWHqLyFL7NsmWt15gYwnQBkDVV93XO3wvAyfvm0myUgtl7m5tFduJXigrKexC9iyxaci2whK6yWUfs2m-NNo3WpoqwjdtTFVtZAV6~NvOk2xddCkGduxYzczMAOOQlHRZYPLbKPA0f81yQew__"
                />
              </div>
            </div>
          </div>

          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
            {products.map((product) => (
              <ProductCard key={product.id} product={product} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default JustForSection;
