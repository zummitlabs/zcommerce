/* eslint-disable react/prop-types */

const ProductCard = ({ product }) => {
  return (
    <div className="bg-white w-[232px] h-[298px]  gap-x-[32px] rounded-lg shadow-md text-center">
      <img
        src={product.image}
        alt={product.name}
        className="w-full h-[189px] object-cover rounded-tr rounded-tl"
      />
      <div className="w-full p-2">
        <h2 className="text-[18px] text-start font-semibold ">
          {product.name}
        </h2>
        <div className="text-[#2A2A2A]   flex justify-between text-start ">
          {"★".repeat(product.rating)}
          {"☆".repeat(5 - product.rating)}
          <p className="text-[#030303] text-[16px] mb-1 ">
            {product.sold} sold
          </p>
        </div>

        <p className="text-[24px]  font-semibold  text-end">{product.price}₹</p>
      </div>
    </div>
  );
};

export default ProductCard;
