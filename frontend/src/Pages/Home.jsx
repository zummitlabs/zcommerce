/* eslint no-unused-vars: "off" */
import React, { useRef } from "react";
import Navbar from "../Components/Navbar";
import OpenStore from "../Components/OpenStore";
import HowItWorks from "../Components/HowItWorks";
import Buiseness from "../Components/Buiseness";
import CardSection from "../Components/Card/CardSection";
import WhyChooseUs from "../Components/WhyChooseUs";
import PricingPlan from "../Components/PricingPlans";
import Testimonial from "../Components/testimonial/Testimonal";
import Footer from "../Components/Footer";

function Home() {
  const whyChooseUsRef = useRef(null);
  const pricingPlanRef = useRef(null);

  const scrollToSection = (ref) => {
    ref.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <>
      <Navbar
        scrollToSection={scrollToSection}
        refs={{ whyChooseUsRef, pricingPlanRef }}
      />
      <OpenStore />
      <HowItWorks />
      <CardSection />
      <div ref={whyChooseUsRef}>
        <WhyChooseUs />
      </div>
      <div ref={pricingPlanRef}>
        <PricingPlan />
      </div>
      <Testimonial />
      <Buiseness />
      <Footer />
    </>
  );
}

export default Home;
