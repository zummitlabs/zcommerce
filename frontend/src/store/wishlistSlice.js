const initialState = {
    wishlistItems: [],
};

import { createSlice } from '@reduxjs/toolkit';

const wishlistSlice = createSlice({
    name: 'wishlist',
    initialState,
    reducers: {
        addToWishlist: (state, action) => {
            const item = action.payload;
            const existingItem = state.wishlistItems.find((x) => x.id === item.id);

            if (!existingItem) {
                state.wishlistItems.push(item);
            }
        },
        removeFromWishlist: (state, action) => {
            const itemId = action.payload;
            state.wishlistItems = state.wishlistItems.filter((item) => item.id !== itemId);
        },
        clearWishlist: (state) => {
            state.wishlistItems = [];
        },
    },
});

export const { addToWishlist, removeFromWishlist, clearWishlist } = wishlistSlice.actions;

export default wishlistSlice.reducer;