export function useAuthRegister() {
    const register = async (registerFormData , role) => {
        try {
            console.log(registerFormData , role , "from hook");
            return ;
            const response = await fetch(`http://localhost:4000/api/users/signup`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: registerFormData.registerFullName,
                    username: registerFormData.registerUsername,
                    email: registerFormData.registerEmail,
                    password: registerFormData.registerPassword,
                }),
            });

            const responseData = await response.json();

            if (response.ok) { // Checks if status is in the 200-299 range
                return { success: true, data: responseData };
            } else {
                console.error("Error creating user:", responseData.error || "Unknown error");
                return { success: false, error: responseData.error || "Unknown error" };
            }
        } catch (error) {
            console.error("Network error:", error.message);
            return { success: false, error: "Network error. Please try again later." };
        }
    };

    return { register };
}

export function useAuthLogin() {
    const login = async (loginFormData) => {
        try {
            const response = await fetch(`http://localhost:4000/api/users/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: loginFormData.email,
                    password: loginFormData.password,
                }),
            });

            const responseData = await response.json();

            if (response.ok) { // Checks if status is in the 200-299 range
                console.log("User logged in");
                return { success: true, data: responseData };
            } else {
                return { success: false, error: responseData.error || "Unknown error" };
            }
        } catch (error) {
            console.error("Network error:", error.message);
            return { success: false, error: "Network error. Please try again later." };
        }
    };

    return { login };
}

export function useAuthVerify() {
    const verify = async (token) => {
        try {
            const response = await fetch(`http://localhost:4000/api/users/verify`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    token,
                }),
            });

            const responseData = await response.json();

            if (response.ok) { // Checks if status is in the 200-299 range
                console.log("User verified");
                return { success: true, data: responseData };
            } else {
                console.error("Error verifying user:", responseData.error || "Unknown error");
                return { success: false, error: responseData.error || "Unknown error" };
            }
        } catch (error) {
            console.error("Network error:", error.message);
            return { success: false, error: "Network error. Please try again later." };
        }
    };

    return { verify };
}


export function usePasswordResetCode() {
    const passwordResetCode = async (email) => {
        try {
            const response = await fetch(`http://localhost:4000/api/users/forgetpassword`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email,
                }),
            });

            const responseData = await response.json();

            if (response.ok) { // Checks if status is in the 200-299 range
                console.log("Verification email sent");
                return { success: true, data: responseData };
            } else {
                console.error("Error sending verification email:", responseData.error || "Unknown error");
                return { success: false, error: responseData.error || "Unknown error" };
            }
        } catch (error) {
            console.error("Network error:", error.message);
            return { success: false, error: "Network error. Please try again later." };
        }
    };

    return { passwordResetCode };
}

export function usePasswordReset() {
    const passwordReset = async (email , password) => {
        try {
            const response = await fetch(`http://localhost:4000/api/users/resetpassword`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email:email,
                    password: password,
                }),
            });

            const responseData = await response.json();

            if (response.ok) { // Checks if status is in the 200-299 range
                console.log("Password reset");
                return { success: true, data: responseData };
            } else {
                console.error("Error resetting password:", responseData.error || "Unknown error");
                return { success: false, error: responseData.error || "Unknown error" };
            }
        } catch (error) {
            console.error("Network error:", error.message);
            return { success: false, error: "Network error. Please try again later." };
        }
    };

    return { passwordReset };
}


export function useAuthSellerRegister() {
    const sellerRegister = async (registerFormData , role) => {
        try {
            const response = await fetch(`http://localhost:4000/api/users/signup`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: registerFormData.firstName,
                    email: registerFormData.email,
                    password: registerFormData.password,
                    phone : registerFormData.phoneNumber,
                    role: role,
                }),
            });

            const responseData = await response.json();

            if (response.ok) { // Checks if status is in the 200-299 range
                return { success: true, data: responseData };
            } else {
                console.error("Error creating user:", responseData.error || "Unknown error");
                return { success: false, error: responseData.error || "Unknown error" };
            }
        } catch (error) {
            console.error("Network error:", error.message);
            return { success: false, error: "Network error. Please try again later." };
        }
    };

    return { sellerRegister };
}