/** @type {import('tailwindcss').Config} */
export default {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false,
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      gradientColorStops: theme => ({
        'purple-yellow': ['#800080', '#FFD700'],
      }),
      backgroundImage:{
        "custombg":"url('../assets/images/bg-pricing.svg')",
        "custom-gradient": 'conic-gradient(from 90deg at 59.72% 100%, #230045 0deg, #3B0076 185.95deg, #6C0D9F 204.07deg, #FCAF3E 360deg)',
      },
      fontFamily:{
        'open-sans':['Open Sans'],
        'poppins': ['Poppins', 'sans-serif'],
        'roboto': ['Roboto', 'sans-serif'],
        'bodoni':['Bodoni Moda SC'],
        'inter': ['Inter', 'sans-serif'],
      },
      boxShadow: {
        'custom-inset': 'inset 0 5px 2.1px 0 #9730fa, inset 4px 0 3px 0 #9730fa, inset -4px 0 3.1px 0 #9730fa',
          '3xl': '5px 5px 0px 0px rgba(107, 33, 168, 1)',
      },
      // backgroundImage: {
      //   'angular-gradient': 'conic-gradient(from 90deg at 80% 100%, #210043 29%, #3E0770 40%, #530C91 49%, #BA2CD1 63%, #E7897F 71%, #F8AB60 75%, #FFB954 80%, #FFB64A 85%, #FFA721 91%)',
      // },
      
      fontWeight: {
        'semi-bold': 600,
        'regular': 400,
      },
      backgroundImage: {
        'custom-gradient': 'linear-gradient(180deg, #7C2CCC 0%, #530993 100%)',
        'custom-gradient-register': 'linear-gradient(90deg, #440C7C 0%, #681BA4 22%, #AA24CC 100%)',

      },
    },
  },
  plugins: [
    
  ],
};
